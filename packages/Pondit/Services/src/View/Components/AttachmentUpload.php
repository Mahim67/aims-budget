<?php

namespace Pondit\Services\View\Components;

use Illuminate\View\Component;

class AttachmentUpload extends Component
{
    public  $class
           ,$label
           ,$name
           ,$id
           ,$otherAttr;

    public function __construct(
        $id = false,
        $class = false,
        $label =  false,
        $name = false,
        $otherAttr = false)
    {
        $this->id           = $id;
        $this->class        = $class;
        $this->label        = $label;
        $this->name         = $name;
        $this->otherAttr    = $otherAttr;
    }
    
    public function render()
    {
        return view('pondit-services::components.attachments_upload');
    }
}
