<?php

namespace Pondit\Services\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pondit\Services\Models\Attachment;

class AttachmentApiController extends Controller
{
    public function index()
    {
        $attachments = Attachment::all();
        return response()->json([
            'status' => 'ok',
            'data'   => $attachments
        ]);
    }
    public function store(Request $request)
    {
        try
        {            
            $file = $request->file;
            if(!is_null($file))
            {
                $filesize  = filesize($file);
                $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            }
            $filePath  =  "/storage/attachments/";

            $sequenceNumber = 1;
            $sequence = Attachment::orderBy('id', 'desc')->first();

            $sequenceNumber  = $sequence ? $sequence->sequence_number+1 : $sequenceNumber;
       
            $data  = new Attachment();
            $data->sequence_number      =   $sequenceNumber;
            $data->user_define_name     =   $request->user_define_name;
            $data->original_name        =   $file?$file->getClientOriginalName():null;
            $data->type                 =   $_FILES['file']['type'];
            $data->extension            =   $extension??null;
            $data->path                 =   $filePath;
            $data->size                 =   $filesize??null;
            $data->metadata             =   $request->metadata;
            if ($request->hasFile('file')) {
                $data['file'] = $this->uploadFile($request->file, $request->user_define_name);
            }
            $data->save();

            return response()->json([
                'status'  => 'ok',
                'message' => 'Created successfully!',
                'data'    => $data
            ]);
        } catch (QueryException $e) {
            return response()->json([
                'status'  => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }
    public function show($id)
    {
        $data = Attachment::where('id', $id)->first();
        return response()->json($data);
    }
    public function update(Request $request, $id)
    {
        try{
            $file      = $request->file;

            if(!is_null($file))
            {
                $filesize  = filesize($file);
                $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            }
            $filePath  =  "/storage/attachments/";
            $data      = Attachment::where('id', $id)->first();            
            
            $data->user_define_name     =   $request->user_define_name;
            $data->original_name        =   $file?$file->getClientOriginalName():null;
            $data->type                 =   $_FILES['file']['type'];
            $data->extension            =   $extension??null;
            $data->path                 =   $filePath;
            $data->size                 =   $filesize??null;
            $data->metadata             =   $request->metadata;
            if ($request->hasFile('file')) {
                $this->unlink($data->file);
                $data['file'] = $this->uploadFile($request->file, $request->user_define_name);
            }

            $data->save();
            
            $data->reArrangeSequence($request->sequence_number);
            return response()->json([
                'status'  => 'ok',
                'message' => 'Updated successfully!',
                'data'    => $data
            ]);
        } catch (QueryException $e) {
            return response()->json([
                'status'  => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function destroy($id)
    {
        try
        {
            $data = Attachment::where('id', $id)->first();
            $data->delete();
            $data->reArrangeSequenceOnDelete($data->sequence_number);
            return response()->json($data);
        } 
        catch (QueryException $th) {
            return response()->json($th->getMessage());
        }
    }
    public function trash()
    {
        $data = Attachment::onlyTrashed()->orderBy('id', 'desc')->get();
        return response()->json($data);
    }
    public function restore($id){
    	$data = Attachment::onlyTrashed()->where('id', $id)
                            ->first()
                            ->restore();
        return response()->json($data);
    }
    public function permanentDelete($id)
    {
        $data = Attachment::onlyTrashed()->where('id', $id)->first();
        $data->forceDelete();
        return response()->json(['status'=>'Entity has been Permanently Delete Successfully']);
    }
    
    private function uploadFile($file, $name)
    {
        $user_name = strtolower(str_replace([' ', ':'], '-', $name));

        $timestamp = str_replace([' ', ':'], '', Carbon::now()->toDateTimeString());
        $file_name = $timestamp .'-'.$user_name. '.' .$file->getClientOriginalExtension();

        $pathToUpload = $file->move((storage_path().'/app/public/attachments'), $file_name);

        return $file_name;
    }
    private function unlink($file)
    {
        $pathToUpload = storage_path().'/app/public/attachments';
        if ($file != '' && file_exists($pathToUpload. $file)) {
            @unlink($pathToUpload. $file);
        }
    }

    public function download($id)
    {
        try {
            $file_name = Attachment::where('id', $id)->select('file')->first();
            $path =  storage_path('app/public/attachments/').$file_name->file;
            // dd($path);
            return $path;
            // return response()->download($path);

        } catch (QueryException $e) {
            return response()->json([
                'status'  => 'error',
                'message' => $e->getMessage()
            ]);
        }
        
    }
}
