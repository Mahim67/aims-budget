<?php

namespace Pondit\Services\Models;

use Pondit\Services\Traits\RecordSequenceable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Attachment extends Model
{
    use HasFactory, SoftDeletes, RecordSequenceable;
    protected $dates    = ['deleted_at'];
    protected $table  =  "attachments";
    
    protected $fillable = ['id'
                            ,'sequence_number'
                            ,'original_name'
                            ,'user_define_name'
                            ,'type'
                            ,'path'
                            ,'metadata'
                            ,'size'
                        ];
}
