<?php

namespace Pondit\Services\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Pondit\Services\View\Components\AttachmentUpload;

class ServicesServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->loadRoutesFrom(__DIR__ . '/../Routes/web.php');
        $this->loadRoutesFrom(__DIR__ . '/../Routes/api.php');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        $this->publishes([
            __DIR__ . '/../../publishable/assets' => public_path('vendor/pondit-services/assets'),
        ], 'public');

        $this->loadViewsFrom(__DIR__ . '/../Resources/views', 'pondit-services');
        
        $this->loadComponents();
    }

    public function register(): void
    {
        // Register stuffs
    }

    private function loadComponents()
    {
        Blade::component('pondit-attach-upload', AttachmentUpload::class);
    }
}
