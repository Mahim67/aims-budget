<?php

use Illuminate\Support\Facades\Route;
use Pondit\Services\Http\Controllers\Api\AttachmentApiController;

Route::group(['prefix' => 'api/', 'middleware' => ['web']], function () {
 
    Route::group(['prefix' => 'attachments' ], function () {
        Route::get('/', [AttachmentApiController::class, 'index']);
        Route::get('/show/{id}', [AttachmentApiController::class, 'show']);
        Route::put('/update/{id}', [AttachmentApiController::class, 'update']);
        Route::post('/store', [AttachmentApiController::class, 'store']);
        Route::post('/{id}', [AttachmentApiController::class, 'destroy']);
        Route::get('/trash', [AttachmentApiController::class, 'trash']);
        Route::get ('/{id}/restore', [AttachmentApiController::class, 'restore']);
        Route::get ('/delete/{id}', [AttachmentApiController::class, 'permanentDelete']);
        // download
        Route::get('download/{id}', [AttachmentApiController::class, 'download']);
    });

});