<?php

use Illuminate\Support\Facades\Route;
use Pondit\Services\Http\Controllers\AttachmentController;


Route::group(['middleware' => ['web']], function () {
    
    Route::group(['prefix' => 'attachments', 'as' => 'attachments.'], function () {
        Route::get('/', [AttachmentController::class, 'index'])->name('index');
        Route::get('/create', [AttachmentController::class, 'create'])->name('create');
        Route::get('/show/{id}', [AttachmentController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [AttachmentController::class, 'edit'])->name('edit');
        Route::put('/update/{id}', [AttachmentController::class, 'update'])->name('update');
        Route::post('/', [AttachmentController::class, 'store'])->name('store');
        Route::post('/{id}', [AttachmentController::class, 'destroy'])->name('delete');
        Route::get('/trash', [AttachmentController::class, 'trash'])->name('trash');
        Route::get ('/{id}/restore', [AttachmentController::class, 'restore'])->name ('restore');
        Route::get ('/delete/{id}', [AttachmentController::class, 'permanentDelete'])->name ('permanentDelete');

        Route::get('download/{id}', [AttachmentController::class, 'download'])->name('download');
    });
});