<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachments', function (Blueprint $table) {
            $table->id();
            $table->string('original_name')->nullable();
            $table->string('user_define_name')->nullable();
            $table->unsignedInteger('sequence_number')->nullable();
            $table->string('type')->nullable();
            $table->string('extension')->nullable();
            $table->string('path')->nullable();
            $table->string('size')->nullable();
            $table->string('file')->nullable();
            $table->text('metadata')->nullable();
            
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attachments', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
