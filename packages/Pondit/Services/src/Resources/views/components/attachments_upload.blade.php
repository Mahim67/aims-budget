<div class='form-group row'>
    <label for="file" class="col-sm-2 col-form-label"> @lang($label)</label>
    <div class='col-sm-10'>
        <div id="attachment"></div>
        <input type="file" class="form-control {{$class}}" name="{{$name}}" id="{{$id}}"  {{$otherAttr}}>
        <div id="download"></div>
    </div>
</div>

@push('js')
<script>
    $(document).ready(function(){
     $('#upload_attachments_form').on('submit', function(event){
      event.preventDefault();
      $.ajax({
       url:"{{ url('api/attachments/store') }}",
       method:"POST",
       data:new FormData(this),
       dataType:'JSON',
       contentType: false,
       cache: false,
       processData: false,
       success:function(data)
       {
            console.log(data.data)
            $('#attachment').html(`<input type="text" value="`+data.data.id+`" name="id"/>`);
            if (data.data.extension = 'docx') {
                let dwnlod = `<a class="btn btn-circle btn-circle-sm bg-grey"
                        href="{{url('/attachments/download/`+data.data.id+`')}}">
                        <i class="fas fa-download"></i>
                        DOCX
                    </a>`
                $('#download').html(dwnlod);
            }
            else if(data.data.extension = 'xlsx'){
                let dwnlod = `<a class="btn btn-circle btn-circle-sm bg-grey"
                        href="{{url('/attachments/download/`+data.data.id+`')}}">
                        <i class="fas fa-download"></i>
                        XLXS
                    </a>`
                $('#download').html(dwnlod);
            }
            else{
                console.log("Other format")
            }

       }
      })
     });
    
    });
</script>
@endpush