@extends('pondit-limitless::layouts.master')

@push('css')
<style>
</style>
@endpush

@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<!-- Basic example -->
<x-pondit-card title="{{__('widgets::lang.attachments')}}">
    <table class="table datatable-colvis-basic">
        <thead>
            <tr>
                <th>{{ __("widgets::lang.ser_no") }}</th>
                <th>{{ __("widgets::lang.original_name") }}</th>
                <th>{{ __("widgets::lang.user_define_name") }}</th>
                <th>{{ __("widgets::lang.type") }}</th>
                <th>{{ __("widgets::lang.sequence_number") }}</th>
                <th class="text-center">{{ __("widgets::lang.action") }}</th>
            </tr>
        </thead>
        <tbody>
            <?php $sl = 1 ?>
            @foreach ($data as $datum)
            <tr>
                <td>{{ $sl++ }}</td>
                <td>{{ $datum->original_name }}</td>
                <td>{{ $datum->user_define_name }}</td>
                <td>{{ $datum->type }}</td>
                <td>{{ $datum->sequence_number }}</td>
                <td class="d-flex justify-content-center">
                    <span>
                        <a href="{{route('attachments.restore', $datum->id)}}"
                            class="btn bg-success btn-circle btn-circle-sm">
                            <i class=" fas fa-redo"></i>
                        </a>
                    </span>
                    <span>
                        <p class="btn bg-danger btn-circle btn-circle-sm show-alert"
                            onclick="delete_fun(this)" data-each_id = "{{ $datum->id }}">
                            <i class=" fas fa-trash"></i>
                        </p>
                    </span>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <x-slot name="cardFooter">
        <div class="text-muted"></div>
        <div class="text-muted">
            <x-pondit-act-i url="{{ route('attachments.index') }}"/>
            <x-pondit-act-c url="{{ route('attachments.create') }}"/>
            </a>
        </div>
        <span> </span>
    </x-slot>
</x-pondit-card>


@endsection

@push('js')

<script
    src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/plugins/tables/datatables/datatables.min.js">
</script>
<script  src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>


<script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/app.js"></script>
<script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/demo_pages/datatables_extension_colvis.js">
</script>

<script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/plugins/notifications/sweet_alert.min.js"></script>


<script>
   function delete_fun(input) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });
        swalInit.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if(result.value) {
                fetchDelete (input.dataset.each_id)
                swalInit.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                );
            }
            else if(result.dismiss === swal.DismissReason.cancel) {
                swalInit.fire(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                );
            }
        });
   } 
  
</script>

<script>
    function fetchDelete(param_id) {
        // console.log(param_id);
        fetch("/delete/"+param_id,
        {
            method: "GET",
            headers: {
                'Content-Type': 'application/json'
            }
        })
    }
</script>
@endpush