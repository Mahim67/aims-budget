@extends('pondit-limitless::layouts.master')

@section('content')

@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
{{ csrf_field() }}
<x-pondit-card title="{{__('widgets::lang.attachments')}}">
    {!! Form::model( $data,[
        'route' => ['attachments.update', $data->id],
        'method' => 'PUT',
        'enctype' => 'multipart/form-data'
        ]) !!}
        <div class='form-group row'>
            <label for="user_define_name" class="col-sm-2 col-form-label"> {{__('widgets::lang.user define name')}}</label>
            <div class='col-sm-10'>
                {!! Form::text('user_define_name', null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class='form-group row'>
            <label for="file" class="col-sm-2 col-form-label"> {{__('widgets::lang.file upload')}}</label>
            <div class='col-sm-10'>
                
                {!! Form::file('file', null, ['class'=>'form-control', 'required']) !!}
                @if ($data->file)
                <img src="/storage/attachments/{{ $data->file }}" width="150px" height="120px" class="pt-2">
                @else
                <p class="text-danger mt-2">Not found</p>
                @endif
            </div>
        </div>
        <div class='form-group row'>
            <label for="sequence_number" class="col-sm-2 col-form-label"> {{__('widgets::lang.sequence number')}}</label>
            <div class='col-sm-10'>
                {!! Form::text('sequence_number', null, ['class'=>'form-control']) !!}
            </div>
        </div>
        {{-- <div class='form-group row'>
            <label for="type" class="col-sm-2 col-form-label"> {{__('widgets::lang.type')}}</label>
            <div class='col-sm-10'>
                {!! Form::text('type', null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class='form-group row'>
            <label for="path" class="col-sm-2 col-form-label"> {{__('widgets::lang.path')}}</label>
            <div class='col-sm-10'>
                {!! Form::text('path', null, ['class'=>'form-control']) !!}
            </div>
        </div> --}}

        <div class='text-right mt-3'>
            <button type="submit" class="btn btn-sm bg-success float-right ml-1"><i class="fas fa-check"></i>
                Save</button>
            <a href="{{ route('attachments.index') }}" class="btn btn-sm bg-danger float-right"> <i
                    class="fas fa-times"></i> Cancel</a>
        </div>
        {!! Form::close() !!}
    <x-slot name="cardFooter">
        <div class="text-muted">

        </div>
        <div class="text-muted d-inline-flex">
            <x-pondit-act-i url="{{ route('attachments.index') }}" />
            <x-pondit-act-v url="{{ route('attachments.show', $data->id) }}" />
            <x-pondit-act-c url="{{ route('attachments.create') }}" />
            <x-pondit-act-d url="{{ route('attachments.delete', $data->id) }}" />
        </div>

        <span>

        </span>
    </x-slot>
</x-pondit-card>

@endsection



@push('js')
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

<script
    src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/plugins/tables/datatables/extensions/buttons.min.js">
</script>
<script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/plugins/forms/selects/select2.min.js">
</script>
<script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/app.js"></script>
@endpush