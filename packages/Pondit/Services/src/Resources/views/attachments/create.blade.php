@extends('pondit-limitless::layouts.master')

@section('content')

@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')

<x-pondit-card title="{{__('widgets::lang.attachments')}}">
    {!! Form::open([
    // 'route'=>['attachments.store'],
    'method' => 'POST',
    'id' => 'upload_attachments_form',
    'enctype' => 'multipart/form-data'
    ]) !!}
{{ csrf_field() }}
<div class="alert" id="message" style="display: none"></div>
    <div class='form-group row'>
        <label for="user_define_name" class="col-sm-2 col-form-label"> {{__('widgets::lang.user define name')}}</label>
        <div class='col-sm-10'>
            {!! Form::text('user_define_name', null, ['class'=>'form-control']) !!}
        </div>
    </div>
    {{-- <x-pondit-attach-up name="file" otherAttr="required" /> --}}
    
    <x-pondit-attach-upload name="file" otherAttr="required" label="{{__('File Upload')}}"/>

    {{-- <div class='form-group row'>
        <label for="file" class="col-sm-2 col-form-label"> {{__('widgets::lang.file upload')}}</label>
        <div class='col-sm-10'>
            {!! Form::file('file', null, ['class'=>'form-control', 'required']) !!}
        </div>
    </div> --}}
    <div class='form-group row'>
        <label for="sequence_number" class="col-sm-2 col-form-label"> {{__('widgets::lang.sequence number')}}</label>
        <div class='col-sm-10'>
            {!! Form::text('sequence_number', null, ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class='text-right mt-3'>
        <button type="submit" class="btn btn-sm bg-success float-right ml-1"><i class="fas fa-check"></i>
            Save</button>
        <a href="#" class="btn btn-sm bg-danger float-right"> <i class="fas fa-times"></i>
            Cancel</a>
    </div>
    {!! Form::close() !!}
    <x-slot name="cardFooter">
        <div class="text-muted"></div>
        <div class="text-muted">
            <x-pondit-act-i url="{{ route('attachments.index') }}" />
        </div>
        <span></span>
    </x-slot>
</x-pondit-card>

@endsection


@push('js')

{{-- <script>
    $(document).ready(function(){
        // let id = this.form
        var x = document.forms[0]['id'];
        console.log(x)
    });
</script> --}}
{{-- <script>
    $(document).ready(function(){
    
     $('#upload_attachments_form').on('submit', function(event){
      event.preventDefault();
      $.ajax({
       url:"{{ url('api/attachments/store') }}",
       method:"POST",
       data:new FormData(this),
       dataType:'JSON',
       contentType: false,
       cache: false,
       processData: false,
       success:function(data)
       {
            console.log(data)
            // $('#message').css('display', 'block');
            // $('#message').html(data.message);
            // $('#message').addClass(data.class_name);
            // $('#uploaded_image').html(data.uploaded_image);
       }
      })
     });
    
    });
</script> --}}

@endpush