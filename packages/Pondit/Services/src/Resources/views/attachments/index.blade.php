@extends('pondit-limitless::layouts.master')

@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<!-- Basic example -->
<x-pondit-card title="{{__('widgets::lang.attachments')}}">
    {{-- {{ csrf_token() }} --}}
    <x-pondit-datatable>
        <x-slot name="thead">
            <tr>
                <th>{{ __("widgets::lang.ser no") }}</th>
                <th>{{ __("widgets::lang.original name") }}</th>
                <th>{{ __("widgets::lang.user define name") }}</th>
                <th>{{ __("widgets::lang.type") }}</th>
                <th>{{ __("widgets::lang.sequence number") }}</th>
                <th class="text-center">{{ __("widgets::lang.action") }}</th>
            </tr>
        </x-slot>
        <?php $sl = 1 ?>
        @foreach ($data as $datum)
        <tr>
            <td>{{ $sl++ }}</td>
            <td>{{ $datum->original_name }}</td>
            <td>{{ $datum->user_define_name }}</td>
            <td>{{ $datum->type }}</td>
            <td>{{ $datum->sequence_number }}</td>

            <td class="d-flex justify-content-center">
                <span>
                    @if ($datum->extension == "docx" || $datum->extension == "xlsx" || $datum->extension == "csv")
                    {{-- <button class="btn btn-circle btn-circle-sm bg-grey download" id="{{$datum->id}}"
                    value="{{$datum->id}}">
                    <i class="fas fa-download"></i>
                    </button> --}}
                    <a class="btn btn-circle btn-circle-sm bg-grey"
                        href="{{route('attachments.download', $datum->id)}}">
                        <i class="fas fa-download"></i>
                    </a>
                    @else
                    <button type="button" class="btn btn-circle btn-circle-sm btn-primary download" data-toggle="modal" data-target="#exampleModalCenter" id="{{$datum->id}}">
                        p
                    </button>
                    @endif

                </span>
                <span>
                    <a href="{{route('attachments.show', $datum->id)}}" class="btn bg-success btn-circle btn-circle-sm">
                        <i class=" fas fa-eye"></i>
                    </a>
                </span>
                <span>
                    <a href="{{route('attachments.edit', $datum->id)}}" class="btn bg-primary btn-circle btn-circle-sm">
                        <i class=" fas fa-pen"></i>
                    </a>
                </span>
                <span>
                    {!! Form::open([
                    'route' => ['attachments.delete', $datum->id],
                    'method' => 'POST'
                    ]) !!}
                    <button class="btn bg-danger btn-circle btn-circle-sm" onclick="return confirm('Are You Sure!')"><i
                            class=" fas fa-trash"></i></button>
                    {!! Form::close() !!}
                </span>
            </td>
        </tr>
        @endforeach
    </x-pondit-datatable>

    <x-slot name="cardFooter">
        <div class="text-muted">{{count($data)}} </div>
        <div class="text-muted">
            <x-pondit-act-c url="{{ route('attachments.create') }}" />
            <x-pondit-act-t url="{{ route('attachments.trash') }}" />
        </div>
        <span>

        </span>
    </x-slot>
</x-pondit-card>


<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="model_body">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



@endsection

@push('js')
<script>
    $(document).ready(function()
        {
            $.ajax
            ({
                type: "GET",
                url: '/api/attachments',
                cache: false,
                success: function(data)
                {
                    console.log(data)
                }
            });
        })
    
    $('.download').click(function(e)
    {
        var id = e.currentTarget.id
        console.log(id);
        $.ajax
        ({
            type: "GET",
            url: '/api/attachments/show/'+id,
            cache: false,
            success: function(data)
            {
                console.log(data)
                if (data.extension == 'pdf') {
                    let pdf_view = `<embed src="/storage/attachments/`+data.file+`" width="100%" type="application/pdf" height="500px" />`;
                    $('#model_body').html(pdf_view)
                    $('.modal-title').html(data.user_define_name)
                }
                else {
                    let img_view = ` <img src="/storage/attachments/`+data.file+`" alt="" border=3 height=200 width=250 alt="">`;
                    $('#model_body').html(img_view)
                    $('.modal-title').html(data.user_define_name)
                }
            }
        });
    })
</script>
@endpush