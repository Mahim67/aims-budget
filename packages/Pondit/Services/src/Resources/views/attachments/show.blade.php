@extends('pondit-limitless::layouts.master')

@push('css')
<style>
</style>
@endpush

@section('content')

@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<!-- Basic example -->
<x-pondit-card title="{{__('widgets::lang.attachments')}}">
    <ul class="nav nav-sidebar mb-2">
        <li class="nav-item-header mt-0">Details</li>
        <li class="nav-item">
            <div class="d-flex nav-link">
                <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> {{__('widgets::lang.original name')}} :</div>
                <div class="mt-2 mt-sm-0 ml-3">{{$data->original_name}}</div>
            </div>
        </li>
        <li class="nav-item">
            <div class="d-flex nav-link">
                <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> {{__('widgets::lang.user define name')}} :</div>
                <div class="mt-2 mt-sm-0 ml-3">{{$data->user_define_name}}</div>
            </div>
        </li>
        <li class="nav-item">
            <div class="d-flex nav-link">
                <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> {{__('widgets::lang.sequence number')}} :</div>
                <div class="mt-2 mt-sm-0 ml-3">{{ $data->sequence_number }}</div>
            </div>
        </li>
        <li class="nav-item">
            <div class="d-flex nav-link">
                <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> {{__('widgets::lang.type')}} :</div>
                <div class="mt-2 mt-sm-0 ml-3">{{ $data->type }}</div>
            </div>
        </li>
        <li class="nav-item">
            <div class="d-flex nav-link">
                <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> {{__('widgets::lang.type')}} :</div>
                <div class="mt-2 mt-sm-0 ml-3">{{ $data->extension }}</div>
            </div>
        </li>
        <li class="nav-item">
            <div class="d-flex nav-link">
                <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> {{__('widgets::lang.path')}} :</div>
                <div class="mt-2 mt-sm-0 ml-3">{{ $data->path }}</div>
            </div>
        </li>
        <li class="nav-item">
            <div class="d-flex nav-link">
                <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> {{__('widgets::lang.file')}} :</div>
                <div class="mt-2 mt-sm-0 ml-3">
                    <img src="/storage/attachments/{{$data->file}}" alt="" border=3 height=200 width=250 alt="">
                </div>
            </div>
        </li>
    </ul>
    <x-slot name="cardFooter">
        <div class="text-muted"></div>
        <div class="text-muted d-flex justify-content-between ">
            <x-pondit-act-i url="{{ route('attachments.index') }}" />
            <x-pondit-act-c url="{{ route('attachments.create') }}" />
            <x-pondit-act-e url="{{ route('attachments.edit', $data->id) }}" />
            <x-pondit-act-d url="{{ route('attachments.delete', $data->id) }}" />
        </div>
        <span></span>
    </x-slot>
</x-pondit-card>

@endsection

@push('js')

<script
    src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/plugins/tables/datatables/extensions/buttons.min.js">
</script>
<script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/plugins/forms/selects/select2.min.js">
</script>
<script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/app.js"></script>

@endpush