var postBtn_tech_spec = document.querySelector(".tech_spec_button");
var sendHttpRequest = (method, url, data) => {
	return fetch(url, {
		method: method,
		body: JSON.stringify(data),
		headers: {
			"Content-Type": "application/json",
			Authorization: "Bearer " + auth.getToken(),
		},
	}).then((response) => {
		if (response.status >= 400) {
			return response.json().then((errResData) => {
				var error = new Error("Something went wrong!");
				error.data = errResData;
				throw error;
			});
		}
		return response.json();
	});
};
function sendTechSpecData() {
	var specification = document.getElementById("specification_tech_spec_input");
	var procurement_id = document.getElementById("procurement_id").value;
	if (specification.value.length == 0 || procurement_id.length == 0) {
		alert("Specification field is empty");
		return true;
	}
	
	sendHttpRequest(
		"POST",
		API_BASE_URL +
			`procurements/${procurement_id}/tech-specs/store`,
		{
			specification: specification.value,
			option: "freetext",
		}
	)
		.then((responseData) => {
			console.log(
				specification.nextSibling.querySelector(".note-editing-area")
					.childNodes[2]
			);
			displayOutputfor_tech_spec(
				responseData.data.specification,
				responseData.data.sequence_number,
				responseData.data.id
			);
			specification.nextSibling.querySelector(
				".note-editing-area"
			).childNodes[2].innerHTML = "";
			
		})
		.catch((err) => {
			console.log(err, err.data);
		});
}

var displayOutputfor_tech_spec = (specification_data, id, pkey) => {
	var final_output = document.getElementById("final_output_tech_spec");
	var card_body = document.createElement("div");
    card_body.setAttribute("class", "card card-body");
    card_body.setAttribute('id',pkey)
	var div = document.createElement("div");
	// div.addEventListener("click", editable_function(this),);
	div.setAttribute("id", "editable_class_tech_spec");
	div.setAttribute("ondblclick", "editable_tech_spec_function(this)");
	div.innerHTML = "<b>" + id + "</b>" + "." + specification_data;

	var div_second = document.createElement("div");
	div_second.setAttribute("id", "editable_class_tech_spec_textarea");
	div_second.setAttribute("class", "display_none_class");
	var textarea = document.createElement("textarea");
	textarea.setAttribute("id", "editable_class_tech_spec_textarea_input");
	textarea.setAttribute("class", "summernote");
	textarea.innerHTML = specification_data;
	var input = document.createElement("input");
	input.setAttribute("type", "hidden");
	input.setAttribute("id", "gen_tech_spec_id");
	input.setAttribute("value", pkey);
	var p = document.createElement("p");
	p.setAttribute("id", "specification_update_tech_spec");
	p.setAttribute("onclick", "update_techspec(this)");
	var u_i = document.createElement("i");
	u_i.setAttribute("class", "fas fa-check px-1 py-1 bg-success");
	p.appendChild(u_i);

	var div_clerboth = document.createElement("div");
	div_clerboth.style.clear = "both";
	var div_after_clear_both = document.createElement("div");
	var div_float_right = document.createElement("div");
	div_float_right.setAttribute("class", "float-right");
	var table = document.createElement("table");
	var tr = document.createElement("tr");
	var td = document.createElement("td");
	var td_p = document.createElement("p");
	td_p.setAttribute("class", "delete_tech_spec");
	td_p.setAttribute("onclick", "delete_techspec_func(this)");
	var p_i = document.createElement("i");
	p_i.setAttribute("class", "fas fa-trash px-1 py-1 bg-danger");
	td_p.appendChild(p_i);
	td_p.appendChild(p);
	tr.appendChild(td_p);
	table.appendChild(tr);
	div_float_right.appendChild(table);
	div_after_clear_both.appendChild(div_float_right);

	div_second.append(textarea, input, p);

	card_body.append(div, div_second, div_after_clear_both);
	var br = document.createElement("br");

	final_output.append(card_body);
	// $('#final_output_term_condition').sortable();
	// $(".summernote").summernote();
};
postBtn_tech_spec.addEventListener("click", sendTechSpecData);


//

function editable_tech_spec_function(input) {
   
	$(".row-sortable-technical-spec").sortable("disable");
	$(".summernote").summernote();
	var editable_class_tech_spec_list = document.querySelectorAll(
		"#editable_class_tech_spec"
	);
	var editable_class_tech_spec_textarea = document.querySelectorAll(
		"#editable_class_tech_spec_textarea"
	);
    var index = 0;
    //  alert("in");
	for (index; index < editable_class_tech_spec_list.length; index++) {
		if (editable_class_tech_spec_list[index] == input) {
			editable_class_tech_spec_list[index].classList.add(
				"display_none_class"
			);
			editable_class_tech_spec_textarea[index].classList.remove(
				"display_none_class"
			);
		} else {
			editable_class_tech_spec_list[index].classList.remove(
				"display_none_class"
			);
			// editable_class_term_condition.classList[index].classList.remove("display_none_class");
			editable_class_tech_spec_textarea[index].classList.add(
				"display_none_class"
			);
		}
	}
}

////


function displayTechSpeconOutput(data_output) {
	var final_output = document.getElementById("final_output_tech_spec");
	while (final_output.firstChild) {
		final_output.removeChild(final_output.firstChild);
	}
	data_output.forEach((element) => {
		var card_body = document.createElement("div");
        card_body.setAttribute("class", "card card-body");
        card_body.setAttribute('id',element.id)
		var div = document.createElement("div");
		// div.addEventListener("click", editable_function(this),);
		div.setAttribute("id", "editable_class_tech_spec");
		div.setAttribute("ondblclick", "editable_tech_spec_function(this)");
		div.innerHTML =
			"<b>" + element.sequence_number + "</b>" + "." + element.specification;

		var div_second = document.createElement("div");
		div_second.setAttribute("id", "editable_class_tech_spec_textarea");
		div_second.setAttribute("class", "display_none_class");
		var textarea = document.createElement("textarea");
		textarea.setAttribute("id", "editable_class_tech_spec_textarea_input");
		textarea.setAttribute("class", "summernote");
		textarea.innerHTML = element.specification;
		var input = document.createElement("input");
		input.setAttribute("type", "hidden");
		input.setAttribute("id", "gen_tech_spec_id");
		input.setAttribute("value", element.id);
		var p = document.createElement("p");
		p.setAttribute("id", "specification_update_tech_spec");
		p.setAttribute("onclick", "update_techspec(this)");
		var u_i = document.createElement("i");
		u_i.setAttribute("class", "fas fa-check px-1 py-1 bg-success");
		p.appendChild(u_i);
		// p.innerHTML = "update";

		var div_clerboth = document.createElement("div");
		div_clerboth.style.clear = "both";
		var div_after_clear_both = document.createElement("div");
		var div_float_right = document.createElement("div");
		div_float_right.setAttribute("class", "float-right");
		var table = document.createElement("table");
		var tr = document.createElement("tr");
		var td = document.createElement("td");
		var td_p = document.createElement("p");
		td_p.setAttribute("class", "delete_tech_spec");
		td_p.setAttribute("onclick", "delete_techspec_func(this)");
		var p_i = document.createElement("i");
		p_i.setAttribute("class", "fas fa-trash px-1 py-1 bg-danger");
		td_p.appendChild(p_i);
		td_p.appendChild(p);
		tr.appendChild(td_p);
		table.appendChild(tr);
		div_float_right.appendChild(table);
		div_after_clear_both.appendChild(div_float_right);

		div_second.append(textarea, input, p);

		card_body.append(div, div_second, div_clerboth, div_after_clear_both);
		var br = document.createElement("br");

		final_output.append(card_body);
	});
}

function get_tech_spec_data() {
    var procurement_id = document.getElementById("procurement_id").value;
	sendHttpRequest(
		"POST",
		API_BASE_URL + `procurements/${procurement_id}/tech-specs`
	)
        .then((responseData) => {
            // console.log(responseData.data);
			displayTechSpeconOutput(
				responseData.data.tech_specs
			);
		})
		.catch((err) => {
			console.log(err, err.data);
		});
}
function make_tech_sortable() {
	return $(".row-sortable-technical-spec").sortable("enable");
}


$(document).ready(function () {
	var pcm_id = $("#procurement_id_rearrange").val();
	$(".row-sortable-technical-spec").sortable({
		update: function (event, ui) {
			var each_row_tech_spec = ui.item[0];
			// console.log(each_row_tech_spec);
			sendHttpRequest(
				"POST",
				API_BASE_URL +
					`procurements/${pcm_id}/tech-specs/${each_row_tech_spec.id}/rearrance-sequence`,
				{
					sequence_number: ui.item.index() + 1,
				}
			)
				.then((responseData) => {
					console.log(responseData.data);
				})
				.catch((err) => {
					console.log(err, err.data);
				});
		},
	});
});


function update_techspec(input) {
	$(".row-sortable-technical-spec").sortable();
	$(".summernote").summernote("disable");
	var procurement_id = document.querySelector("#procurement_id").value;
	var specification_updates = document.querySelectorAll(
		"#specification_update_tech_spec"
	);
	var gen_tech_spec_ids = document.querySelectorAll(
		"#gen_tech_spec_id"
	);
	var parent_editable_class_tech_spec_textarea = document.querySelectorAll(
		"#editable_class_tech_spec_textarea"
	);
	var editable_class_tech_spec_textarea_inputs = document.querySelectorAll(
		"#editable_class_tech_spec_textarea_input"
	);
	var index = 0;
	for (index; index < specification_updates.length; index++) {
		if (specification_updates[index] == input) {
			let each_parent = parent_editable_class_tech_spec_textarea[index];
			let gen_tech_spec_val = editable_class_tech_spec_textarea_inputs[index].value;
            let gen_tech_spec_id = gen_tech_spec_ids[index].value;
			sendHttpRequest(
				"POST",
				API_BASE_URL +
					`procurements/${procurement_id}/tech-specs/${gen_tech_spec_id}/update`,
				{
					specification: gen_tech_spec_val,
					option: "freetext",
				}
			)
				.then((responseData) => {
					// console.log(responseData.data);
					each_parent.classList.add("display_none_class");
					get_tech_spec_data();
					make_tech_sortable();
				
				
					// office_id.value = "";
				})
				.catch((err) => {
					console.log(err, err.data);
				});
			break;
		}
	}
	// $('#final_output_term_condition').sortable();
}

///

function delete_techspec_func(input) {
	var procurement_id = document.querySelector("#procurement_id").value;
	var gen_tech_spec_ids = document.querySelectorAll(
		"#gen_tech_spec_id"
	);
	var delete_tech_specs = document.querySelectorAll(
		".delete_tech_spec"
	);
	var index = 0;
	for (index; index < delete_tech_specs.length; index++) {
		if (delete_tech_specs[index] == input) {
            let gen_tech_spec_id = gen_tech_spec_ids[index].value;
			sendHttpRequest(
				"POST",
				API_BASE_URL +
					`procurements/${procurement_id}/tech-specs/${gen_tech_spec_id}/delete`
			)
				.then((responseData) => {
					console.log(responseData.data);
					get_tech_spec_data();
				})
				.catch((err) => {
					console.log(err, err.data);
				});
			break;
		}
	}
	// $('#final_output_term_condition').sortable();
}