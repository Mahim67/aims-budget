

///

// var universe_indexing = document.querySelector('.term_condition_index_data').dataset.term_condition_index;
var postBtn_term_condition = document.querySelector(".add_field_button");
var sendHttpRequest = (method, url, data) => {
	return fetch(url, {
		method: method,
		body: JSON.stringify(data),
		headers: {
			"Content-Type": "application/json",
			Authorization: "Bearer " + auth.getToken(),
		},
	}).then((response) => {
		if (response.status >= 400) {
			return response.json().then((errResData) => {
				var error = new Error("Something went wrong!");
				error.data = errResData;
				throw error;
			});
		}
		return response.json();
	});
};
function sendGenTermData() {
	var specification = document.getElementById("specification_term_condition");
	var note_editable = document.querySelector(".note-editable");
	var procurement_id = document.getElementById("procurement_id").value;
	if (specification.value.length == 0 || procurement_id.length == 0) {
		alert("Specification field is empty");
		return true;
	}
	console.log(
		API_BASE_URL +
			`procurements/${procurement_id}/general-terms-and-conditions/store`
	);
	sendHttpRequest(
		"POST",
		API_BASE_URL +
			`procurements/${procurement_id}/general-terms-and-conditions/store`,
		{
			specification: specification.value,
			option: "freetext",
		}
	)
		.then((responseData) => {
			console.log(
				specification.nextSibling.querySelector(".note-editing-area")
					.childNodes[2]
			);
			displayOutput(
				responseData.data.specification,
				responseData.data.sequence_number,
				responseData.data.id
			);
			specification.nextSibling.querySelector(
				".note-editing-area"
			).childNodes[2].innerHTML = "";
			// note_editable.firstChild.innerHTML = "";
			universe_indexing++;
			// office_id.value = "";
		})
		.catch((err) => {
			console.log(err, err.data);
		});
}

var displayOutput = (specification_data, id, pkey) => {
	var final_output = document.getElementById("final_output_term_condition");
	var card_body = document.createElement("div");
	card_body.setAttribute("class", "card card-body");
	card_body.setAttribute('id',pkey)
	var div = document.createElement("div");
	// div.addEventListener("click", editable_function(this),);
	div.setAttribute("id", "editable_class_term_condition");
	div.setAttribute("ondblclick", "editable_function(this)");
	div.innerHTML = "<b>" + id + "</b>" + "." + specification_data;

	var div_second = document.createElement("div");
	div_second.setAttribute("id", "editable_class_term_condition_textarea");
	div_second.setAttribute("class", "display_none_class");
	var textarea = document.createElement("textarea");
	textarea.setAttribute("id", "editable_class_term_condition_textarea_input");
	textarea.setAttribute("class", "summernote");
	textarea.innerHTML = specification_data;
	var input = document.createElement("input");
	input.setAttribute("type", "hidden");
	input.setAttribute("id", "gen_term_condition_id");
	input.setAttribute("value", pkey);
	var p = document.createElement("p");
	p.setAttribute("id", "specification_update");
	p.setAttribute("onclick", "update_general_terms_and_condition(this)");
	var u_i = document.createElement("i");
	u_i.setAttribute("class", "fas fa-check px-1 py-1 bg-success");
	p.appendChild(u_i);

	var div_clerboth = document.createElement("div");
	div_clerboth.style.clear = "both";
	var div_after_clear_both = document.createElement("div");
	var div_float_right = document.createElement("div");
	div_float_right.setAttribute("class", "float-right");
	var table = document.createElement("table");
	var tr = document.createElement("tr");
	var td = document.createElement("td");
	var td_p = document.createElement("p");
	td_p.setAttribute("class", "delete_general_term_conditions");
	td_p.setAttribute("onclick", "delete_general_term_condition_func(this)");
	var p_i = document.createElement("i");
	p_i.setAttribute("class", "fas fa-trash px-1 py-1 bg-danger");
	td_p.appendChild(p_i);
	td_p.appendChild(p);
	tr.appendChild(td_p);
	table.appendChild(tr);
	div_float_right.appendChild(table);
	div_after_clear_both.appendChild(div_float_right);

	div_second.append(textarea, input, p);

	card_body.append(div, div_second, div_after_clear_both);
	var br = document.createElement("br");

	final_output.append(card_body);
	// $('#final_output_term_condition').sortable();
	// $(".summernote").summernote();
};
postBtn_term_condition.addEventListener("click", sendGenTermData);

///

function get_term_condition_data() {
	var procurement_id = document.getElementById("procurement_id").value;
	sendHttpRequest(
		"POST",
		API_BASE_URL + `procurements/${procurement_id}/general-terms-and-conditions`
	)
		.then((responseData) => {
			displayTermandConditionOutput(
				responseData.data.general_terms_and_conditions
			);
		})
		.catch((err) => {
			console.log(err, err.data);
		});
}

/////

function displayTermandConditionOutput(data_output) {
	var final_output = document.getElementById("final_output_term_condition");
	while (final_output.firstChild) {
		final_output.removeChild(final_output.firstChild);
	}
	data_output.forEach((element) => {
		var card_body = document.createElement("div");
		card_body.setAttribute("class", "card card-body");
		var div = document.createElement("div");
		 card_body.setAttribute('id',element.id)
		// div.addEventListener("click", editable_function(this),);
		div.setAttribute("id", "editable_class_term_condition");
		div.setAttribute("ondblclick", "editable_function(this)");
		div.innerHTML =
			"<b>" + element.sequence_number + "</b>" + "." + element.specification;

		var div_second = document.createElement("div");
		div_second.setAttribute("id", "editable_class_term_condition_textarea");
		div_second.setAttribute("class", "display_none_class");
		var textarea = document.createElement("textarea");
		textarea.setAttribute("id", "editable_class_term_condition_textarea_input");
		textarea.setAttribute("class", "summernote");
		textarea.innerHTML = element.specification;
		var input = document.createElement("input");
		input.setAttribute("type", "hidden");
		input.setAttribute("id", "gen_term_condition_id");
		input.setAttribute("value", element.id);
		var p = document.createElement("p");
		p.setAttribute("id", "specification_update");
		p.setAttribute("onclick", "update_general_terms_and_condition(this)");
		var u_i = document.createElement("i");
		u_i.setAttribute("class", "fas fa-check px-1 py-1 bg-success");
		p.appendChild(u_i);
		// p.innerHTML = "update";

		var div_clerboth = document.createElement("div");
		div_clerboth.style.clear = "both";
		var div_after_clear_both = document.createElement("div");
		var div_float_right = document.createElement("div");
		div_float_right.setAttribute("class", "float-right");
		var table = document.createElement("table");
		var tr = document.createElement("tr");
		var td = document.createElement("td");
		var td_p = document.createElement("p");
		td_p.setAttribute("class", "delete_general_term_conditions");
		td_p.setAttribute("onclick", "delete_general_term_condition_func(this)");
		var p_i = document.createElement("i");
		p_i.setAttribute("class", "fas fa-trash px-1 py-1 bg-danger");
		td_p.appendChild(p_i);
		td_p.appendChild(p);
		tr.appendChild(td_p);
		table.appendChild(tr);
		div_float_right.appendChild(table);
		div_after_clear_both.appendChild(div_float_right);

		div_second.append(textarea, input, p);

		card_body.append(div, div_second, div_clerboth, div_after_clear_both);
		var br = document.createElement("br");

		final_output.append(card_body);
	});
}

////

$(document).ready(function () {
	var procurement_id = document.querySelector("#procurement_id").value;
	$(".row-sortable").sortable({
		update: function (event, ui) {
			var each_row_term_condition = ui.item[0];
			console.log(each_row_term_condition)
			console.log(ui.item.index() + 1);
			// api/procurements/procurement_id/general-terms-and-conditions/terms_and_condition_id/rearrance-sequence
			sendHttpRequest(
				"POST",
				API_BASE_URL +
					`procurements/${procurement_id}/general-terms-and-conditions/${each_row_term_condition.id}/rearrance-sequence`,
				{
					sequence_number: ui.item.index() + 1,
				}
			)
				.then((responseData) => {
					console.log(responseData.data);
				})
				.catch((err) => {
					console.log(err, err.data);
				});
		},
	});
});
function make_sortable() {
	return $(".row-sortable").sortable("enable");
}

////

function editable_function(input) {
	$(".row-sortable").sortable("disable");
	$(".summernote").summernote();
	var editable_class_term_condition_list = document.querySelectorAll(
		"#editable_class_term_condition"
	);
	var editable_class_term_condition_textarea = document.querySelectorAll(
		"#editable_class_term_condition_textarea"
	);
	var index = 0;
	for (index; index < editable_class_term_condition_list.length; index++) {
		if (editable_class_term_condition_list[index] == input) {
			editable_class_term_condition_list[index].classList.add(
				"display_none_class"
			);
			editable_class_term_condition_textarea[index].classList.remove(
				"display_none_class"
			);
		} else {
			editable_class_term_condition_list[index].classList.remove(
				"display_none_class"
			);
			// editable_class_term_condition.classList[index].classList.remove("display_none_class");
			editable_class_term_condition_textarea[index].classList.add(
				"display_none_class"
			);
		}
	}
}

////

function update_general_terms_and_condition(input) {
	$(".row-sortable").sortable();
	$(".summernote").summernote("disable");
	var procurement_id = document.querySelector("#procurement_id").value;
	var specification_updates = document.querySelectorAll(
		"#specification_update"
	);
	var gen_term_condition_ids = document.querySelectorAll(
		"#gen_term_condition_id"
	);
	var parent_editable_class_term_condition_textareas = document.querySelectorAll(
		"#editable_class_term_condition_textarea"
	);
	var editable_class_term_condition_textareas = document.querySelectorAll(
		"#editable_class_term_condition_textarea_input"
	);
	var index = 0;
	for (index; index < specification_updates.length; index++) {
		if (specification_updates[index] == input) {
			let each_parent = parent_editable_class_term_condition_textareas[index];
			let gen_term_val = editable_class_term_condition_textareas[index].value;
			let gen_term_id = gen_term_condition_ids[index].value;
			sendHttpRequest(
				"POST",
				API_BASE_URL +
					`procurements/${procurement_id}/general-terms-and-conditions/${gen_term_id}/update`,
				{
					specification: gen_term_val,
					option: "freetext",
				}
			)
				.then((responseData) => {
					console.log(responseData.data);
					each_parent.classList.add("display_none_class");
					get_term_condition_data();
					make_sortable();
					note_editable.innerHTML = "";
				
					// office_id.value = "";
				})
				.catch((err) => {
					console.log(err, err.data);
				});
			break;
		}
	}
	// $('#final_output_term_condition').sortable();
}

///

function delete_general_term_condition_func(input) {
	var procurement_id = document.querySelector("#procurement_id").value;
	var gen_term_condition_ids = document.querySelectorAll(
		"#gen_term_condition_id"
	);
	var delete_general_term_conditions = document.querySelectorAll(
		".delete_general_term_conditions"
	);
	var index = 0;
	for (index; index < delete_general_term_conditions.length; index++) {
		if (delete_general_term_conditions[index] == input) {
			let gen_term_id = gen_term_condition_ids[index].value;
			sendHttpRequest(
				"POST",
				API_BASE_URL +
					`procurements/${procurement_id}/general-terms-and-conditions/${gen_term_id}/delete`
			)
				.then((responseData) => {
					console.log(responseData.data);
					get_term_condition_data();
				})
				.catch((err) => {
					console.log(err, err.data);
				});
			break;
		}
	}
	// $('#final_output_term_condition').sortable();
}
