/* ------------------------------------------------------------------------------
 *
 *  # Fancytree
 *
 *  Demo JS code for extra_trees.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------

var Fancytree = function() {


    //
    // Setup module components
    //

    // Uniform
    var _componentFancytree = function() {
        if (!$().fancytree) {
            console.warn('Warning - fancytree_all.min.js is not loaded.');
            return;
        }

        // Editable nodes
        $('.tree-editable').fancytree({
            extensions: ['edit', 'dnd'],
            source: {
                url: '/vendor/pondit/themes/limitless/global_assets/demo_data/fancytree/fancytree.json'
            },
            edit: {
                adjustWidthOfs: 0,
                inputCss: {minWidth: '0'},
                triggerStart: ['f2', 'dblclick', 'shift+click', 'mac+enter'],
                save: function(event, data) {
                    alert('save ' + data.input.val()); // Save data.input.val() or return false to keep editor open
                }
            },
            dnd: {
                autoExpandMS: 400,
                focusOnClick: true,
                preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
                preventRecursiveMoves: true, // Prevent dropping nodes on own descendants
                dragStart: function(node, data) {
                    return true;
                },
                dragEnter: function(node, data) {
                    return true;
                },
                dragDrop: function(node, data) {

                    // This function MUST be defined to enable dropping of items on the tree.
                    data.otherNode.moveTo(node, data.hitMode);
                }
            }
        });
        

       
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function() {
            _componentFancytree();
        }
    }
}();


// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
    Fancytree.init();
});


