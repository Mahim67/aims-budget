<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('pagetitle') BAF - Airforce Inventory Management System</title>
    <link rel="shortcut icon" href="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/images/logo-sm.png">

    <link href="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/css/googlefonts.css" rel="stylesheet"
        type="text/css">

    <link href="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/css/icons/fontawesome/styles.min.css"
        rel="stylesheet" type="text/css">
    <link href="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/css/icons/material/styles.min.css"
        rel="stylesheet" type="text/css">
    <link href="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/css/icons/icomoon/styles.min.css"
        rel="stylesheet" type="text/css">
    <link href="{{ asset('') }}vendor/pondit/themes/limitless/assets/css/bootstrap.css" rel="stylesheet"
        type="text/css">
    <link href="{{ asset('') }}vendor/pondit/themes/limitless/assets/css/bootstrap_limitless.css" rel="stylesheet"
        type="text/css">
    <link href="{{ asset('') }}vendor/pondit/themes/limitless/assets/css/layout.min.css" rel="stylesheet"
        type="text/css">
    <link href="{{ asset('') }}vendor/pondit/themes/limitless/assets/css/layout.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('') }}vendor/pondit/themes/limitless/assets/css/components.css" rel="stylesheet"
        type="text/css">
    <link href="{{ asset('') }}vendor/pondit/themes/limitless/assets/css/colors.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('') }}vendor/pondit/themes/limitless/assets/css/custom.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('') }}vendor/pondit/themes/limitless/assets/css/style.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('') }}vendor/pondit/themes/limitless/assets/css/components.min.css" rel="stylesheet"
        type="text/css">


    @stack('css')
    <!-- /global stylesheets -->
    <script src="{{ asset('vendor/pondit/themes/limitless/global_assets/js/axios.min.js') }}"></script>

</head>

<body>

    <!-- Main navbar -->
    <x-pondit-limitless-header
        logoPath="{{ asset('vendor/pondit/themes/limitless/global_assets/images/BAF Logo Light.svg') }}">
        <x-slot name="logoutOption">
            <a href="#" onclick="alert('Not Implemented');" class="navbar-nav-link">
                <i class="icon-switch2"></i>
                <span class="d-md-none ml-2">Logout</span>
            </a>
        </x-slot>
    </x-pondit-limitless-header>
    <!-- /main navbar -->


    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->

        {{-- @include('pondit-limitless::partials.sidebar') --}}

        <x-pondit-limitless-sidebar>

        <x-pondit-limitless-nav-item menuName="Office Demand" route="{{route('prepcm-demand-details.index')}}" navItemClass="active" />

            <x-pondit-limitless-nav-group navGroupName="Office Demand">
                <x-slot name="navItem">
                    
                    <x-pondit-limitless-nav-item menuName="Item Wise" route="#" />
                    <x-pondit-limitless-nav-item menuName="Range Wise" route="{{route('prepcm-demand-details.range-wise-demand')}}" />
                    <x-pondit-limitless-nav-item menuName="Budget Wise" route="#" />
                </x-slot>
            </x-pondit-limitless-nav-group>
            <x-pondit-limitless-nav-group navGroupName="Base Demand">
                <x-slot name="navItem">
                    <x-pondit-limitless-nav-item menuName="Item Wise" route="#" />
                    <x-pondit-limitless-nav-item menuName="Office Wise" route="#" />
                    <x-pondit-limitless-nav-item menuName="Range Wise" route="{{route('prepcm-demand-details.range-wise-demand')}}" />
                    <x-pondit-limitless-nav-item menuName="Budget Wise" route="#" />
                </x-slot>
            </x-pondit-limitless-nav-group>

            <x-pondit-limitless-nav-group navGroupName="All Demand">
                <x-slot name="navItem">
                    <x-pondit-limitless-nav-item menuName="Item Wise" route="#" />
                    <x-pondit-limitless-nav-item menuName="Base Wise" route="#" />
                    <x-pondit-limitless-nav-item menuName="Range Wise" route="#" />
                    <x-pondit-limitless-nav-item menuName="Budget Wise" route="#" />
                </x-slot>
            </x-pondit-limitless-nav-group>
            <x-pondit-limitless-nav-group navGroupName="Budget">
                <x-slot name="navItem">
                    <x-pondit-limitless-nav-item menuName="Budget" route="{{route('budget-api.index')}}" />
                    <x-pondit-limitless-nav-item menuName="Budget Code" route="{{ route('budgetcode-api.index') }}" />
                    <x-pondit-limitless-nav-item menuName="Budget Details" route="{{ route('budget-details-api.index') }}" />
                </x-slot>
            </x-pondit-limitless-nav-group>

        </x-pondit-limitless-sidebar>

        <!-- /main sidebar -->

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            {{-- @include('pondit-limitless::partials.pageheader') --}}
            <x-pondit-limitless-pageheader />
            <!-- /page header -->

            <!-- Content area -->
            <div class="content">
                @yield('content')
            </div>
            <!-- /content area -->

            <!-- Footer -->
            <x-pondit-limitless-footer />
            <!-- /footer -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

    <!-- Core JS files -->
    <script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/app.js"></script>

    <script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/main/jquery.min.js"></script>
    <script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/plugins/loaders/blockui.min.js">
    </script>
    <script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/plugins/ui/ripple.min.js"></script>
    <!-- /core JS files -->
    <script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/demo_pages/components_modals.js">
    </script>
    <script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/plugins/notifications/bootbox.min.js">
    </script>

    <script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/plugins/forms/selects/select2.min.js">
    </script>
    <script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/demo_pages/form_select2.js"></script>

    @stack('js')



</body>

</html>