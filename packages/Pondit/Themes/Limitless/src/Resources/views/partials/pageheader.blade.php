<div class="page-header page-header-light">

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        
        @include('pondit-limitless::elements.breadcrumb')

        <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                @include('pondit-limitless::elements.search')
            </div>
        </div>
    </div>
</div>