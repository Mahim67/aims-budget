<!-- Main navbar -->
<div class="navbar navbar-expand-md navbar-dark bg-success navbar-static">
	<div class="navbar-brand">
		<a href="{{url('/')}}" class="d-inline-block">
			<img src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/images/BAF Logo Light.svg" height="100Morning, Victoria!
				" alt="">
		</a>
	</div>

	<div class="d-md-none">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
			<i class="icon-tree5"></i>
		</button>
		<button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
			<i class="icon-paragraph-justify3"></i>
		</button>
	</div>

	<div class="collapse navbar-collapse" id="navbar-mobile">
		<ul class="navbar-nav">
			<li class="nav-item">
				<a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
					<i class="icon-paragraph-justify3"></i>
				</a>
			</li>
		</ul>

		<ul class="navbar-nav ml-md-auto">
			<li class="nav-item">
				<a href="#" onclick="alert('Not Implemented');" class="navbar-nav-link">
					<i class="icon-switch2"></i>
					<span class="d-md-none ml-2">Logout</span>
				</a>
			</li>
		</ul>
	</div>
</div>

@push('js')

@endpush
<!-- /main navbar -->