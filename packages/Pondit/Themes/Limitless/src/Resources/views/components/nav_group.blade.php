<li class="nav-item nav-item-submenu bg-success">
    <a href="#" class="nav-link text-light">
        <i class="fas fa-database"></i>
        <span>{{ $navGroupName }}</span>
    </a>
    <ul class="nav nav-group-sub bg-primary" data-submenu-title="Menu levels">
       {{ $navItem }}
    </ul>
</li>