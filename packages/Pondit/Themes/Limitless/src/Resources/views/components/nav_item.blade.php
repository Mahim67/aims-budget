<li class="nav-item">
<a href="{{ $route }}" class="nav-link text-light {{ $navItemClass }}">
        <i class="{{ $icon }}"></i>
        <span>
            {{ $menuName }}
        </span>
    </a>
</li>