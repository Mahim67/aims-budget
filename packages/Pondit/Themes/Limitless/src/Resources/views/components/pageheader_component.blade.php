<div class="page-header page-header-light">

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <x-pondit-limitless-breadcrumb />
        <div class="header-elements d-none">
            <div class="breadcrumb justify-content-center">
                <x-pondit-limitless-bread-search />
            </div>
        </div>
    </div>
</div>