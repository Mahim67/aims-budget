<div class="breadcrumb-elements-item dropdown p-0">
    <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
        <i class="fas fa-bell"></i>
        <span class="badge badge-primary badge-pill">2</span>
    </a>
    <div class="dropdown-menu dropdown-menu-right">
        <a href="" class="dropdown-item">
            <i class="fas fa-envelope"></i>
        </a>
    </div>
</div>