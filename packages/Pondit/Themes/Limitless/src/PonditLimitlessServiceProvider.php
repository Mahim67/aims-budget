<?php

namespace Pondit\Themes\Limitless;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class PonditLimitlessServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->loadTranslationsFrom(__DIR__.'/../../Resources/lang', 'pondit-limitless');        
        $this->publishes([
            __DIR__ . '/../publishable/assets' => public_path('vendor/pondit/themes/limitless/assets'),

            __DIR__ . '/../publishable/global_assets' => public_path('vendor/pondit/themes/limitless/global_assets'),
            
        ], 'public');

        $this->loadViewsFrom(__DIR__ . '/Resources/views', 'pondit-limitless');
        
        $this->loadComponents();
    }

    private function loadComponents()
    {
        Blade::component('pondit-limitless-header', View\Components\HeaderComponent::class);
        Blade::component('pondit-limitless-sidebar', View\Components\SidebarComponent::class);
        Blade::component('pondit-limitless-nav-item', View\Components\NavItem::class);
        Blade::component('pondit-limitless-nav-group', View\Components\NavGroup::class);
        Blade::component('pondit-limitless-footer', View\Components\FooterComponent::class);
        Blade::component('pondit-limitless-breadcrumb', View\Components\BreadcrumbComponent::class);
        Blade::component('pondit-limitless-bread-search', View\Components\BreadSearchComponent::class);
        Blade::component('pondit-limitless-pageheader', View\Components\PageHeaderComponent::class);
    }
}
