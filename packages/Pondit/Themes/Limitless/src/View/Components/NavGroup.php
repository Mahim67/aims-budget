<?php

namespace Pondit\Themes\Limitless\View\Components;

use Illuminate\View\Component;
use Pondit\Themes\Limitless\View\Components\HeaderComponent;

class NavGroup extends Component
{
    public  $navGroupName
           ,$navItem;

    public function __construct(
        $navGroupName   = false,
        $navItem        = false
        )
    {
        $this->navGroupName  =  $navGroupName;
        $this->navItem       =  $navItem;
    }
    
    public function render()
    {
        return view('pondit-limitless::components.nav_group');
    }
}
