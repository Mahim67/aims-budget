<?php

namespace Pondit\Themes\Limitless\View\Components;

use Illuminate\View\Component;
use Pondit\Themes\Limitless\View\Components\HeaderComponent;

class BreadSearchComponent extends Component
{
    // public  $href
    //        ,$icon
    //        ,$menuName;

    // public function __construct(
    //     $logoutOption   = false,
    //     $icon           = "fas fa-arrows-alt",
    //     $menuName       = false
    //     )
    // {
    //     $this->logoutOption  =  $logoutOption;
    //     $this->menuName      =  $menuName;
    //     $this->icon          = $icon;
    // }
    
    public function render()
    {
        return view('pondit-limitless::components.breadcrumb_search');
    }
}
