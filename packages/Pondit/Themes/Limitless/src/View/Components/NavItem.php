<?php

namespace Pondit\Themes\Limitless\View\Components;

use Illuminate\View\Component;
use Pondit\Themes\Limitless\View\Components\HeaderComponent;

class NavItem extends Component
{
    public  $route
           ,$icon
           ,$navItemClass
           ,$menuName;

    public function __construct(
        $route          = false,
        $icon           = "fas fa-arrows-alt",
        $menuName       = false,
        $navItemClass   = false
        )
    {
        $this->route         =  $route;
        $this->navItemClass  =  $navItemClass;
        $this->menuName      =  $menuName;
        $this->icon          = $icon;
    }
    
    public function render()
    {
        return view('pondit-limitless::components.nav_item');
    }
}
