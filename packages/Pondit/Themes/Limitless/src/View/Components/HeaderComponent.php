<?php

namespace Pondit\Themes\Limitless\View\Components;

use Illuminate\View\Component;
use Pondit\Themes\Limitless\View\Components\HeaderComponent;

class HeaderComponent extends Component
{
    public  $logoutOption
           ,$logoPath;

    public function __construct(
        $logoutOption = false,
        $logoPath = false
        )
    {
        $this->logoutOption  =  $logoutOption;
        $this->logoPath      = $logoPath;
    }
    
    public function render()
    {
        return view('pondit-limitless::components.header_component');
    }
}
