<?php

namespace Pondit\Themes\Limitless\View\Components;

use Illuminate\View\Component;
use Pondit\Themes\Limitless\View\Components\HeaderComponent;

class SidebarComponent extends Component
{
    public  $dashboardTitle;

    public function __construct(
        $dashboardTitle = "Dashboard"
        )
    {
        $this->dashboardTitle  =  $dashboardTitle;
    }
    
    public function render()
    {
        return view('pondit-limitless::components.sidebar_component');
    }
}
