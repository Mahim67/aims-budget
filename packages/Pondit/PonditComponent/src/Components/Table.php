<?php

namespace Pondit\PonditComponent\Components;

use Illuminate\View\Component;

class Table extends Component
{
    public $tableClass
           ,$theadClass
           ,$thead
           ,$tbodyClass
           ,$tfooter
           ,$tfootClass;

    public function __construct(
        $tableClass = 'table-bordered', $theadClass='', $thead='',
        $tbodyClass='', $tfooter='', $tfootClass='')
    {
        $this->tableClass = $tableClass;
        $this->theadClass = $theadClass;
        $this->thead      = $thead;
        $this->tbodyClass = $tbodyClass;
        $this->tfooter    = $tfooter;
        $this->tfootClass = $tfootClass;
    }
    
    public function render()
    {
        return view('widgets::pondit.limitless.table');
    }
}
