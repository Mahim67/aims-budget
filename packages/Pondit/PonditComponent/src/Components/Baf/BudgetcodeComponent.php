<?php

namespace Pondit\PonditComponent\Components\Baf;

use Illuminate\View\Component;
use Pondit\Baf\Budget\MasterData\Models\Budgetcode;

class BudgetcodeComponent extends Component
{
    public  $class
           ,$label
           ,$name
           ,$id
           ,$selected
           ,$otherAttr;

    public function __construct(
        $id         =  false,
        $label      =  "widgets::lang.budgetcode",
        $name       =  false,
        $class      =  false,
        $selected   =  false,
        $otherAttr  =  false
    )
    {
        $this->id           = $id;
        $this->class        = $class;
        $this->label        = $label;
        $this->name         = $name;
        $this->selected     = $selected;
        $this->otherAttr    = $otherAttr;
    }
    
    public function render()
    {
        $budgetcodes = Budgetcode::pluck('new_code', 'id')->toArray();
        return view('widgets::baf.budgetcode', compact('budgetcodes'));
    }
}
