<?php

namespace Pondit\PonditComponent\Components\Baf;

use Illuminate\View\Component;

class ItemList extends Component
{
    public  $class
           ,$label
           ,$name
           ,$id
           ,$selected
           ,$otherAttr;

    public $itemLists = ['0'=>'Singer 24inch', '1'=>'LG', '3'=>'BMW'];

    public function __construct(
        $id         =  false,
        $label      =  "widgets::lang.itemList",
        $name       =  false,
        $class      =  false,
        $selected   =  false,
        $otherAttr  =  false
    )
    {
        $this->id           = $id;
        $this->class        = $class;
        $this->label        = $label;
        $this->name         = $name;
        $this->selected     = $selected;
        $this->otherAttr    = $otherAttr;
    }
    
    public function render()
    {
        $lists = $this->itemLists;
        return view('widgets::baf.itemlist', compact('lists'));
    }
}
