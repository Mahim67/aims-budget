<?php

namespace Pondit\PonditComponent\Components;

use Illuminate\View\Component;

class Card extends Component
{
    public $topClass
           ,$bg
           ,$headClass
           ,$titleClass
           ,$slot
           ,$title
           ,$footerClass
           ,$cardFooter;

    public function __construct(
        $bg = 'success', $title=false, $topClass=false, $headClass=false,
        $titleClass=false, $footerClass=false)
    {
        $this->topClass = $topClass;
        $this->bg = $bg;
        $this->headClass = $headClass;
        $this->titleClass = $titleClass;
        $this->title = $title;
        $this->footerClass = $footerClass;
    }
    
    public function render()
    {
        return view('widgets::pondit.limitless.card');
    }
}
