<?php

namespace Pondit\PonditComponent\Components;

use Illuminate\View\Component;

class DataTable extends Component
{
    public $tableClass
           ,$theadClass
           ,$thead
           ,$tbodyClass
           ,$tfooter
           ,$tfootClass;

    public function __construct(
        $tableClass='datatable-colvis-basic',
        $theadClass = false, $tbodyClass = false, $tfootClass = false
    )
    {
        $this->tableClass = $tableClass;
        $this->theadClass = $theadClass;
        $this->tbodyClass = $tbodyClass;
        $this->tfootClass = $tfootClass;
    }
    
    public function render()
    {
        return view('widgets::pondit.limitless.datatable');
    }
}
