<?php

namespace Pondit\PonditComponent\Components;

use Illuminate\View\Component;

class Multiselect extends Component
{
    public $topClass
           ,$id
           ,$name
           ,$data
           ,$label
           ,$labelClass
           ,$selectClass
           ,$selectDivClass
           ,$selectPlaceholder;

    public function __construct(
        $topClass = false, $name = false, $data , 
        $id = false, $label = false, $labelClass = false,
        $selectClass = false, $selectDivClass = false, $selectPlaceholder = false )
    {
        $this->id                = $id;
        $this->name              = $name;
        $this->data              = $data;
        $this->label             = $label;
        $this->topClass          = $topClass;
        $this->labelClass        = $labelClass;
        $this->selectClass       = $selectClass;
        $this->selectDivClass    = $selectDivClass;
        $this->selectPlaceholder = $selectPlaceholder;
    }
    
    public function render()
    {
        return view('widgets::pondit.limitless.multiselect2');
    }
}
