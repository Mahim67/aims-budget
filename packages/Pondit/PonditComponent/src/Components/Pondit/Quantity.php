<?php

namespace Pondit\PonditComponent\Components\Pondit;

use Illuminate\View\Component;

class Quantity extends Component
{
    public  $class
           ,$label
           ,$value
           ,$name
           ,$id
           ,$qtyUnitName
           ,$qtyUnitId
           ,$qtyUnitClass
           ,$otherAttr;

    public function __construct(
        $id = "qty",
        $class = false,
        $label =  "widgets::lang.qty",
        $name = false,
        $value = '0',
        $qtyUnitName = false,
        $qtyUnitId = false,
        $qtyUnitClass = false,
        $otherAttr = false)
    {
        $this->id             = $id;
        $this->class          = $class;
        $this->label          = $label;
        $this->name           = $name;
        $this->value          = $value;
        $this->qtyUnitName    = $qtyUnitName;
        $this->qtyUnitId      = $qtyUnitId;
        $this->qtyUnitClass   = $qtyUnitClass;
        $this->otherAttr      = $otherAttr;
    }
    
    public function render()
    {
        return view('widgets::pondit.quantity');
    }
}
