<?php

namespace Pondit\PonditComponent\Components\Pondit;

use Illuminate\View\Component;

class TotalCost extends Component
{
    public  $name
           ,$id
           ,$qty
           ,$estimatedPrice;

    public function __construct(
        $name       = false,
        $id         = "total_cost",
        $qty    = false,
        $estimatedPrice   = false
        )
    {
        $this->estimatedPrice   = $estimatedPrice;
        $this->qty    = $qty;
        $this->name       = $name;
        $this->id         = $id;
    }
    
    public function render()
    {
        return view('widgets::pondit.total_cost');
    }
}
