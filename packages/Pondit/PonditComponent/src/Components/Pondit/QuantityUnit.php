<?php

namespace Pondit\PonditComponent\Components\Pondit;

use Illuminate\View\Component;

class QuantityUnit extends Component
{
    public  $qtyUnitClass
           ,$qtyUnitName
           ,$qtyUnitId
           ,$otherAttr;

    public function __construct(
        $qtyUnitName = false,
        $qtyUnitClass = false,
        $qtyUnitId = false,
        $otherAttr = false)
    {
        $this->qtyUnitName    = $qtyUnitName;
        $this->qtyUnitId      = $qtyUnitId;
        $this->qtyUnitClass   = $qtyUnitClass;
        $this->otherAttr      = $otherAttr;
    }
    
    public function render()
    {
        return view('widgets::pondit.quantity_unit');
    }
}
