<?php

namespace Pondit\PonditComponent\Components;

use Illuminate\View\Component;

class ActionPreviewFile extends Component
{
    public $url
           ,$id
           ,$class
           ,$icon
           ,$color
           ,$tooltip
           ,$title;

    public function __construct(
        $url = '#', $icon = 'file-powerpoint',
        $tooltip = "widgets::lang.preview",
        $class = false, $id = false,
        $title = false, $color = 'indigo'
    )
    {
        $this->id = $id;
        $this->url = $url;
        $this->icon = $icon;
        $this->color = $color;
        $this->class = $class;
        $this->title = $title;
        $this->tooltip = $tooltip;
    }
    
    public function render()
    {
        return view('widgets::baf.actions_preview_file');
    }
}
