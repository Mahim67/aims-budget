<?php

namespace Pondit\PonditComponent\Components;

use Illuminate\View\Component;

class ActionLink extends Component
{
    public $url
           ,$id
           ,$bg
           ,$class
           ,$icon
           ,$tooltip
           ,$title;

    public function __construct(
        $url = '#', $icon = false, $tooltip = false, 
        $id = false, $bg = false, $class = false, $title = false )
    {
        $this->url      = $url;
        $this->id       = $id;
        $this->bg       = $bg;
        $this->class    = $class;
        $this->icon     = $icon;
        $this->tooltip  = $tooltip;
        $this->title    = $title;
    }
    
    public function render()
    {
        return view('widgets::pondit.limitless.action_link');
    }
}
