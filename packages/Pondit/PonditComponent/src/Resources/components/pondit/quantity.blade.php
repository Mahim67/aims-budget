<div class="form-group">
    <label for="{{$id}}" class="col-form-label"> @lang($label)</label>
    <div class="d-flex">
        <input type="number" class="form-control {{$class}}" name="{{$name}}" id="{{$id}}" {{$otherAttr}} value="{{$value}}">
        <x-pondit-qty-unit class="w-50 ml-2" qtyUnitName={{$qtyUnitName}}/>
    </div>
</div>
