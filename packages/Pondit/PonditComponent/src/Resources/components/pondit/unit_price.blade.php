<div class="">
    <label for="{{$id}}" class="col-form-label">{{$label}}</label>
    <div class="d-flex">
        <input type="number" name="{{$name}}" id="{{$id}}" class="form-control {{$class}}" {{$otherAttr}} value="{{$value}}">
    <x-pondit-currency currencyName="{{$currencyName}}" class="w-50 ml-3"/>
    </div>
</div>