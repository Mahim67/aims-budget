<div class="row">
    <div class="col-md-5">
        {{ $estimatedPrice }}
    </div>
    <div class="col-md-5">
        {{ $qty }}
    </div>
    <div class="col-md-2">
        <label for="">Total Cost</label>
        <input type="text" name="{{$name}}" id="{{$id}}" class="form-control" readonly>
    </div>
</div>

@push('js')
<script>
    $(document).ready(function() {

$('input[id="qty"]').keyup(function() {

    let price = $('input[id="estimated_price"]').val();
    let qty = $('input[id="qty"]').val();
    
    let total = parseInt(price) * parseInt(qty);
    $('input[id="total_cost"]').val(total);
    console.log(total)

});
$('input[id="estimated_price"]').keyup(function() {

    let qty = $('input[id="qty"]').val();
    let price = $('input[id="estimated_price"]').val();

    let total = parseInt(price) * parseInt(qty);
    $('input[id="total_cost"]').val(total);
    console.log(total)

});

});
</script>
@endpush