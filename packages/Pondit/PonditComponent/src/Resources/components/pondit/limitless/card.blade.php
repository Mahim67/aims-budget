<div class="card {{ $topClass }}">
    <div class="card-header card-rounded-top header-elements-inline bg-{{ $bg }} {{ $headClass}}">
        <h5 class="card-title {{ $titleClass }}">{{ $title }}</h5>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
                <a class="list-icons-item" data-action="remove"></a>
            </div>
        </div>
    </div>
    <div class="card-body">
        {{ $slot }}
    </div>
    <div class="card-footer d-flex justify-content-between align-items-center {{ $footerClass }}">
        {{ $cardFooter }}
    </div>
</div>
