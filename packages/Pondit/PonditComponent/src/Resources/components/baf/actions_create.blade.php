<a href="{{ $url }}" id="{{ $id }}" class="{{ $class }} btn btn-outline bg-brown btn-icon text-brown btn-sm border-brown border-2 rounded-round legitRipple mr-1"  data-popup="tooltip" title= @lang($tooltip) data-original-title= @lang($tooltip) >
    <i class="fas fa-{{ $icon }}"></i>
    {{ $title }}
</a>
