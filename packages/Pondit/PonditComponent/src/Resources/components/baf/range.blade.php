<div class="form-group">
    <label for="{{$id}}">@lang($label)</label>
    <select name="{{$name}}" id="{{$id}}" class="form-control select-search {{$class}}" {{$otherAttr}}>
        <option value="">-- Select Range --</option>
        @foreach ($ranges as $key=>$item)
        @if ($key != $selected)
        <option value="{{$key}}">{{$item}}</option>
        @else
        <option value="{{$key}}" selected='selected'>{{$item}}</option>
        @endif
        @endforeach
    </select>
</div>