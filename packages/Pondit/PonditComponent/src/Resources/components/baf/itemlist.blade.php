<div class="form-group row">
    <label for="{{$id}}">@lang($label)</label>
    <select name="{{$name}}" id="{{$id}}" class="form-control select-search {{$class}}" {{$otherAttr}}>
        <option value="">-- Select Item List --</option>
        @foreach ($lists as $key=>$item)
        @if ($key != $selected)
        <option value="{{$key}}">{{$item}}</option>
        @else
        <option value="{{$key}}" selected='selected'>{{$item}}</option>
        @endif
        @endforeach
    </select>
</div>
