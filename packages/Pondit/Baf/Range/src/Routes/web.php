<?php

use Illuminate\Support\Facades\Route;
use Pondit\Baf\Range\Http\Controllers\RangeController;


Route::group(['middleware' => ['web']], function () {
    
    Route::group(['prefix' => 'ranges', 'as' => 'range.'], function () {
        Route::get('/', [RangeController::class, 'index'])->name('index');
        Route::get('/create', [RangeController::class, 'create'])->name('create');
        Route::get('/show/{id}', [RangeController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [RangeController::class, 'edit'])->name('edit');
        Route::post('/', [RangeController::class, 'store'])->name('store');
        Route::put('/update/{id}', [RangeController::class, 'update'])->name('update');
        Route::post('/{id}', [RangeController::class, 'destroy'])->name('delete');
    });
});