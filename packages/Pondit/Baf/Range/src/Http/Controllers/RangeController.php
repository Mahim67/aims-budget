<?php

namespace Pondit\Baf\Range\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pondit\Baf\Range\Models\Range;


class RangeController extends Controller
{
    public function index()
    {
        $data = Range::all();
        return view('ranges::ranges.index', \compact('data'));
    }
    public function create()
    {
        return view('ranges::ranges.create');
    }
    public function store(Request $request, Range $Range)
    {
        // dd($request->all());
        try
        {            
            $sequenceNumber = 1;
            $sequence = Range::orderBy('id', 'desc')->first();

            $sequenceNumber  = $sequence ? $sequence->sequence_number+1 : $sequenceNumber;
            // dd($sequenceNumber);        

            $data  = new Range();
            $data->sequence_number      = $sequenceNumber;
            $data->title          = $request->title;
            $data->description          = $request->description;
            // dd($data);
            $data->save();
            return redirect()
                    ->route('range.index')
                    ->withMessage('Entity has been created successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                    ->route('range.create')
                    ->withErrors($th->getMessage());
        }
    }
    public function show($id)
    {
        $data = Range::findOrFail($id);
        return \view('ranges::ranges.show', \compact('data'));
    }
    public function edit(Range $Range, $id)
    {
        $data = Range::findOrFail($id);
        return \view('ranges::ranges.edit', \compact('data'));
    }
    public function update(Request $request, Range $Range, $id)
    {
        try{
            // dd($request->all());
            $data = Range::findOrFail($id);
            // dd($data);
            
            $data->title          = $request->title;
            $data->description          = $request->description;
            // dd($data);
            $data->save();

            $data->reArrangeSequence($request->sequence_number);

            return redirect()
                ->route('range.index')
                ->withMessage('Entity has been updated successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                        ->back()
                        ->withErrors($th->getMessage());
        }
    }

    public function destroy($id)
    {
        $data = Range::findOrFail($id);
        // dd($data);
        $data->delete();
        $data->reArrangeSequenceOnDelete($data->sequence_number);

        return redirect()
                    ->route('range.index')
                    ->withMessage('Entity has been deleted successfully!');
    }
    
    
}