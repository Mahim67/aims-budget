<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOvProcurementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ov_procurements', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('procurement_id')->nullable();
            $table->string('title')->nullable();

            $table->string('fin_year')->nullable();
            $table->string('initiating_dte')->nullable();
            $table->string('initiating_group')->nullable();
            $table->string('initiated_at')->nullable();
            $table->string('techspec_approved_at')->nullable();
            $table->string('procurement_type')->nullable();

            $table->string('description')->nullable();
            $table->string('qty')->nullable();
            $table->string('unit')->nullable();
            $table->string('projected_value')->nullable();
            $table->string('budget_code')->nullable();

            $table->string('tender_no')->nullable();
            $table->string('tender_floated_at')->nullable();

            $table->string('submitted_offers')->nullable();
            $table->string('submitted_at')->nullable();

            $table->string('tech_vett_passed_offers')->nullable();
            $table->string('fin_vett_passed_offers')->nullable();
            $table->string('chosen_offer')->nullable();
            $table->string('quoted_value')->nullable();
            $table->string('acceptance_send_at')->nullable();
            $table->string('supplier_confirmed_at')->nullable();
            $table->string('contracted_at')->nullable();
            $table->string('contracted_value')->nullable();
            $table->string('rejected_at')->nullable();
            
            $table->string('created_by', 64)->nullable();
            $table->string('updated_by', 64)->nullable();
            $table->string('deleted_by', 64)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        Schema::dropIfExists('ov_procurements', function (Blueprint $table) {

            $table->dropSoftDeletes();

        });
    }
}
