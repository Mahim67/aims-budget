<?php

namespace Pondit\Baf\Report\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pondit\Baf\Report\Models\OvProcurement;


class OvProcurementController extends Controller
{
    public function index()
    {
        $data = OvProcurement::all();
        return view('baf-reports::ov-procurements.index', compact('data'));
    }
    public function create()
    {
        return view('baf-reports::ov-procurements.create');
    }
    public function store(Request $request)
    {
        // dd($request->all());
        try
        {         
            $data  = new OvProcurement();

            $data->title           = $request->title;
            $data->procurement_id  = $request->procurement_id;

            $data->save();
            return redirect()
                    ->route('ov-procurements.index')
                    ->withMessage('Entity has been created successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                    ->route('ov-procurements.create')
                    ->withErrors($th->getMessage());
        }
    }
    public function show($id)
    {
        $data = OvProcurement::findOrFail($id);
        return \view('baf-reports::ov-procurements.show', compact('data'));
    }
    public function edit($id)
    {
        $data = OvProcurement::findOrFail($id);
        return view('baf-reports::ov-procurements.edit', compact('data'));
    }
    public function update(Request $request, $id)
    {
        try{
            // dd($request->all());
            $data = OvProcurement::findOrFail($id);
            $data->title           = $request->title;
            $data->procurement_id  = $request->procurement_id;
            $data->save();

            return redirect()
                ->route('ov-procurements.index')
                ->withMessage('Entity has been updated successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                        ->back()
                        ->withErrors($th->getMessage());
        }
    }
    public function destroy($id)
    {
        $data = OvProcurement::findOrFail($id);
        $data->delete();
        return redirect()
                    ->route('ov-procurements.index')
                    ->withMessage('Entity has been deleted successfully!');
    }
    

    public function reportOne()
    {
        return view('baf-reports::report.report1');
    }
    public function reportTwo()
    {
        return view('baf-reports::report.report2');
    }
    public function reportThree()
    {
        return view('baf-reports::report.report3');
    }
    public function reportFour()
    {
        return view('baf-reports::report.report4');
    }
    public function reportFive()
    {
        return view('baf-reports::report.report5');
    }
    public function reportSix()
    {
        return view('baf-reports::report.report6');
    }
    
}