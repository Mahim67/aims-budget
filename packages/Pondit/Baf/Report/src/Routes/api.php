<?php

use Illuminate\Support\Facades\Route;
use Pondit\Baf\Range\Http\Controllers\Api\RangeApiController;


Route::group(['prefix' => 'api', 'middleware' => ['web']], function () {
    
    Route::group(['prefix' => 'ranges', 'as' => 'range.'], function () {

        Route::get('/range', [RangeApiController::class, 'range']);

        Route::get('/', [RangeApiController::class, 'index']);
        Route::post('/store', [RangeApiController::class, 'store']);
        Route::get('/show/{id}', [RangeApiController::class, 'show']);
        Route::put('/update/{id}', [RangeApiController::class, 'update']);
        Route::post('/delete/{id}', [RangeApiController::class, 'destroy']);
    });
});