<?php

use Illuminate\Support\Facades\Route;
use Pondit\Baf\Report\Http\Controllers\OvProcurementController;


Route::group(['middleware' => ['web']], function () {
    
    Route::group(['prefix' => 'ov-procurements', 'as' => 'ov-procurements.'], function () {
        Route::get('/', [OvProcurementController::class, 'index'])->name('index');
        Route::get('/create', [OvProcurementController::class, 'create'])->name('create');
        Route::get('/show/{id}', [OvProcurementController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [OvProcurementController::class, 'edit'])->name('edit');
        Route::post('/', [OvProcurementController::class, 'store'])->name('store');
        Route::put('/update/{id}', [OvProcurementController::class, 'update'])->name('update');
        Route::post('/{id}', [OvProcurementController::class, 'destroy'])->name('delete');
        /* 
        * Reports Route
        */
        Route::get('/report-one', [OvProcurementController::class, 'reportOne'])->name('report-one');
        Route::get('/report-two', [OvProcurementController::class, 'reportTwo'])->name('report-two');
        Route::get('/report-three', [OvProcurementController::class, 'reportThree'])->name('report-three');
        Route::get('/report-four', [OvProcurementController::class, 'reportFour'])->name('report-four');
        Route::get('/report-five', [OvProcurementController::class, 'reportFive'])->name('report-five');
        Route::get('/report-six', [OvProcurementController::class, 'reportSix'])->name('report-six');
    });
});