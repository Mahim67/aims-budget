<?php

namespace Pondit\Baf\Report\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OvProcurement extends Model
{
    use SoftDeletes;
    
    protected $dates    = ['deleted_at'];

    protected $table    = 'ov_procurements';

    protected $fillable = ['id'
                            ,'procurement_id'
                            ,'title'
                        ];
}
