@extends('pondit-limitless::layouts.master')
@push('css')
<style>
    table, th, td {
/* border: 1px solid black; */
border-collapse: collapse;
}
.heading p{
text-decoration: underline;
/* font-size: 20px; */
font-weight: bold;
}
h1,p{
text-decoration: underline;
}
.d-flex{
 display: flex;
}
.justify-center{
 justify-content:center;
}
</style>
@endpush
@section('content')
<x-pondit-card title="Report">

    <div class="heading d-flex justify-center">
        <div>
            <h1>Procurement Progress Sheet</h1>
        </div>
    </div>
    <div class="text-right">
        <div>
            <p>DATE: {{date('d-m-Y')}}</p>
            <p>DTE SUP(POL GROUP)</p>
        </div>
    </div>
    <x-pondit-datatable>
        <x-slot name="thead">
            <tr>
                <td>Ser No</td>
                <td>Indt no with Dt</td>
                <td>Tender No</td>
                <td>Description</td>
                <td>Tech vetting wef</td>
                <td>Received tech vetting wef</td>
                <td>Time taken for tech</td>
                <td>Dt of final acceptance</td>
                <td>Time taken for suppliers</td>
                <td>Time taken to send</td>
                <td>Time taken in SFC(DP)</td>
                <td>Time taken to send</td>
                <td>Time taken to signing</td>
                <td>RMKS</td>
            </tr>
        </x-slot>
        <tr>
            <td>1</td>
            <td>06.03.2600.030.45001.91.173 dt 09 Oct 19</td>
            <td>06.03.2600.030 dt 09 Oct 19</td>
            <td>10 X Areo Lubricants</td>
            <td>09 Oct 19</td>
            <td>09 Oct 19</td>
            <td>19 Days</td>
            <td>09 Oct 19</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Acceptance given on 09 Oct 19</td>
          </tr>
          <tr>
            <td>1</td>
            <td>06.03.2600.030.45001.91.173 dt 09 Oct 19</td>
            <td>06.03.2600.030 dt 09 Oct 19</td>
            <td>10 X Areo Lubricants</td>
            <td>09 Oct 19</td>
            <td>09 Oct 19</td>
            <td>19 Days</td>
            <td>09 Oct 19</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Acceptance given on 09 Oct 19</td>
          </tr>
    </x-pondit-datatable>





    {{-- <table>
        <thead>
            <tr>
                <td>Ser No</td>
                <td>Indt no with Dt</td>
                <td>Tender No</td>
                <td>Description</td>
                <td>Tech vetting wef</td>
                <td>Received tech vetting wef</td>
                <td>Time taken for tech</td>
                <td>Dt of final acceptance</td>
                <td>Time taken for suppliers</td>
                <td>Time taken to send</td>
                <td>Time taken in SFC(DP)</td>
                <td>Time taken to send</td>
                <td>Time taken to signing</td>
                <td>RMKS</td>
            </tr>
        </thead>
        <tbody>
            <tr>
              <td>1</td>
              <td>06.03.2600.030.45001.91.173 dt 09 Oct 19</td>
              <td>06.03.2600.030 dt 09 Oct 19</td>
              <td>10 X Areo Lubricants</td>
              <td>09 Oct 19</td>
              <td>09 Oct 19</td>
              <td>19 Days</td>
              <td>09 Oct 19</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td>Acceptance given on 09 Oct 19</td>
            </tr>
            <tr>
                <td>1</td>
                <td>06.03.2600.030.45001.91.173 dt 09 Oct 19</td>
                <td>06.03.2600.030 dt 09 Oct 19</td>
                <td>10 X Areo Lubricants</td>
                <td>09 Oct 19</td>
                <td>09 Oct 19</td>
                <td>19 Days</td>
                <td>09 Oct 19</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>Acceptance given on 09 Oct 19</td>
              </tr>
        </tbody>
    </table> --}}

</x-pondit-card>

@endsection