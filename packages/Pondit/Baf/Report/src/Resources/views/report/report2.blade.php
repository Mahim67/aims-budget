@extends('pondit-limitless::layouts.master')
@push('css')
<style>
  table,
  th,
  td {
    /* border: 1px solid black; */
    border-collapse: collapse;
    width: 100%;
    white-space: nowrap;
  }

  h1 {
    text-align: center;
    text-decoration: underline;
  }

  p {
    text-align: right;
    text-decoration: underline;
    
  }
</style>
@endpush
@section('content')
<x-pondit-card title="Report">

  <h1>Summary of Indent Dte Sup(SPS Clothing GP) FY:2020-2021</h1>
  <p>Date: {{date('d-m-Y')}}</p>
  <p> SPS Clothing</p>

  <x-pondit-datatable>
    <x-slot name="thead">
      <tr>
        <th scope="col" colspan="2" rowspan="2">Ser No</th>
        <th scope="col" colspan="2">DTE ENGG</th>
        <th scope="col" colspan="2">DTE C & E</th>
        <th scope="col" colspan="2">DTE A & W</th>
        <th scope="col" colspan="2">DTE SUP</th>
        <th scope="col" colspan="2">DTE MED</th>
        <th scope="col" colspan="2">TOTAL</th>
        <th scope="col">G/TOTAL</th>
      </tr>
      <tr>
        <th scope="col">FC</th>
        <th scope="col">LC</th>
        <th scope="col">FC</th>
        <th scope="col">LC</th>
        <th scope="col">FC</th>
        <th scope="col">LC</th>
        <th scope="col">FC</th>
        <th scope="col">LC</th>
        <th scope="col">FC</th>
        <th scope="col">LC</th>
        <th scope="col">FC</th>
        <th scope="col">LC</th>
        <th scope="col"></th>
      </tr>
    </x-slot>
    <tr>
      <td>1</td>
      <td>Total Indent</td>
      <td>37</td>
      <td>1</td>
      <td>19</td>
      <td>2</td>
      <td>7</td>
      <td></td>
      <td>17</td>
      <td>17</td>
      <td></td>
      <td></td>
      <td>82</td>
      <td>20</td>
      <td>102</td>

    </tr>
    <tr>
      <td>2</td>
      <td>Indent Returned/Unaction by DGDP Cancelled after or before offer & other reason/indent dropped</td>
      <td>37</td>
      <td>1</td>
      <td>19</td>
      <td>2</td>
      <td>7</td>
      <td></td>
      <td>17</td>
      <td>17</td>
      <td></td>
      <td></td>
      <td>82</td>
      <td>20</td>
      <td>102</td>

    </tr>
    <tr>
      <td>3</td>
      <td>Under clearification before offer</td>
      <td>37</td>
      <td>1</td>
      <td>19</td>
      <td>2</td>
      <td>7</td>
      <td></td>
      <td>17</td>
      <td>17</td>
      <td></td>
      <td></td>
      <td>82</td>
      <td>20</td>
      <td>102</td>

    </tr>
    <tr>
      <td>4</td>
      <td>Executable Indent</td>
      <td>37</td>
      <td>1</td>
      <td>19</td>
      <td>2</td>
      <td>7</td>
      <td></td>
      <td>17</td>
      <td>17</td>
      <td></td>
      <td></td>
      <td>82</td>
      <td>20</td>
      <td>102</td>
    </tr>
    <tr>
      <td>5</td>
      <td>Offer received</td>
      <td>37</td>
      <td>1</td>
      <td>19</td>
      <td>2</td>
      <td>7</td>
      <td></td>
      <td>17</td>
      <td>17</td>
      <td></td>
      <td></td>
      <td>82</td>
      <td>20</td>
      <td>102</td>
    </tr>
    <tr>
      <td>6</td>
      <td>Offer under tech vetting</td>
      <td>37</td>
      <td>1</td>
      <td>19</td>
      <td>2</td>
      <td>7</td>
      <td></td>
      <td>17</td>
      <td>17</td>
      <td></td>
      <td></td>
      <td>82</td>
      <td>20</td>
      <td>102</td>
    </tr>
    <tr>
      <td>7</td>
      <td>Under clarification/pending after offer</td>
      <td>37</td>
      <td>1</td>
      <td>19</td>
      <td>2</td>
      <td>7</td>
      <td></td>
      <td>17</td>
      <td>17</td>
      <td></td>
      <td></td>
      <td>82</td>
      <td>20</td>
      <td>102</td>
    </tr>
    <tr>
      <td>8</td>
      <td>Under prep at Sup Dte for CFA's app</td>
      <td>37</td>
      <td>1</td>
      <td>19</td>
      <td>2</td>
      <td>7</td>
      <td></td>
      <td>17</td>
      <td>17</td>
      <td></td>
      <td></td>
      <td>82</td>
      <td>20</td>
      <td>102</td>
    </tr>
    <tr>
      <td>9</td>
      <td>Under CFA's app</td>
      <td>37</td>
      <td>1</td>
      <td>19</td>
      <td>2</td>
      <td>7</td>
      <td></td>
      <td>17</td>
      <td>17</td>
      <td></td>
      <td></td>
      <td>82</td>
      <td>20</td>
      <td>102</td>
    </tr>
    <tr>
      <td>10</td>
      <td>Under acceptance at Sup Dte</td>
      <td>37</td>
      <td>1</td>
      <td>19</td>
      <td>2</td>
      <td>7</td>
      <td></td>
      <td>17</td>
      <td>17</td>
      <td></td>
      <td></td>
      <td>82</td>
      <td>20</td>
      <td>102</td>
    </tr>
    <tr>
      <td>11</td>
      <td>Acceptance given to DGDP</td>
      <td>37</td>
      <td>1</td>
      <td>19</td>
      <td>2</td>
      <td>7</td>
      <td></td>
      <td>17</td>
      <td>17</td>
      <td></td>
      <td></td>
      <td>82</td>
      <td>20</td>
      <td>102</td>
    </tr>
    <tr>
      <td>12</td>
      <td>Tech acceptance at DGDP</td>
      <td>37</td>
      <td>1</td>
      <td>19</td>
      <td>2</td>
      <td>7</td>
      <td></td>
      <td>17</td>
      <td>17</td>
      <td></td>
      <td></td>
      <td>82</td>
      <td>20</td>
      <td>102</td>
    </tr>
    <tr>
      <td>13</td>
      <td>Under prep of ESC at Sup Dte</td>
      <td>37</td>
      <td>1</td>
      <td>19</td>
      <td>2</td>
      <td>7</td>
      <td></td>
      <td>17</td>
      <td>17</td>
      <td></td>
      <td></td>
      <td>82</td>
      <td>20</td>
      <td>102</td>
    </tr>
    <tr>
      <td>14</td>
      <td>Offer Under Fin Vetting</td>
      <td>37</td>
      <td>1</td>
      <td>19</td>
      <td>2</td>
      <td>7</td>
      <td></td>
      <td>17</td>
      <td>17</td>
      <td></td>
      <td></td>
      <td>82</td>
      <td>20</td>
      <td>102</td>
    </tr>
    <tr>
      <td>15</td>
      <td>Awaiting offer</td>
      <td>37</td>
      <td>1</td>
      <td>19</td>
      <td>2</td>
      <td>7</td>
      <td></td>
      <td>17</td>
      <td>17</td>
      <td></td>
      <td></td>
      <td>82</td>
      <td>20</td>
      <td>102</td>
    </tr>
    <tr>
      <td>16</td>
      <td>Contract Signed</td>
      <td>37</td>
      <td>1</td>
      <td>19</td>
      <td>2</td>
      <td>7</td>
      <td></td>
      <td>17</td>
      <td>17</td>
      <td></td>
      <td></td>
      <td>82</td>
      <td>20</td>
      <td>102</td>
    </tr>
  </x-pondit-datatable>



  {{-- <table>
    <thead>
      <tr>
        <th scope="col" colspan="2" rowspan="2">Ser No</th>
        <th scope="col" colspan="2">DTE ENGG</th>
        <th scope="col" colspan="2">DTE C & E</th>
        <th scope="col" colspan="2">DTE A & W</th>
        <th scope="col" colspan="2">DTE SUP</th>
        <th scope="col" colspan="2">DTE MED</th>
        <th scope="col" colspan="2">TOTAL</th>
        <th scope="col">G/TOTAL</th>
      </tr>
      <tr>
        <th scope="col">FC</th>
        <th scope="col">LC</th>
        <th scope="col">FC</th>
        <th scope="col">LC</th>
        <th scope="col">FC</th>
        <th scope="col">LC</th>
        <th scope="col">FC</th>
        <th scope="col">LC</th>
        <th scope="col">FC</th>
        <th scope="col">LC</th>
        <th scope="col">FC</th>
        <th scope="col">LC</th>
        <th scope="col"></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
        <td>Total Indent</td>
        <td>37</td>
        <td>1</td>
        <td>19</td>
        <td>2</td>
        <td>7</td>
        <td></td>
        <td>17</td>
        <td>17</td>
        <td></td>
        <td></td>
        <td>82</td>
        <td>20</td>
        <td>102</td>

      </tr>
      <tr>
        <td>2</td>
        <td>Indent Returned/Unaction by DGDP Cancelled after or before offer & other reason/indent dropped</td>
        <td>37</td>
        <td>1</td>
        <td>19</td>
        <td>2</td>
        <td>7</td>
        <td></td>
        <td>17</td>
        <td>17</td>
        <td></td>
        <td></td>
        <td>82</td>
        <td>20</td>
        <td>102</td>

      </tr>
      <tr>
        <td>3</td>
        <td>Under clearification before offer</td>
        <td>37</td>
        <td>1</td>
        <td>19</td>
        <td>2</td>
        <td>7</td>
        <td></td>
        <td>17</td>
        <td>17</td>
        <td></td>
        <td></td>
        <td>82</td>
        <td>20</td>
        <td>102</td>

      </tr>
      <tr>
        <td>4</td>
        <td>executable Indent</td>
        <td>37</td>
        <td>1</td>
        <td>19</td>
        <td>2</td>
        <td>7</td>
        <td></td>
        <td>17</td>
        <td>17</td>
        <td></td>
        <td></td>
        <td>82</td>
        <td>20</td>
        <td>102</td>
      </tr>
      <tr>
        <td>5</td>
        <td>Offer received</td>
        <td>37</td>
        <td>1</td>
        <td>19</td>
        <td>2</td>
        <td>7</td>
        <td></td>
        <td>17</td>
        <td>17</td>
        <td></td>
        <td></td>
        <td>82</td>
        <td>20</td>
        <td>102</td>
      </tr>
      <tr>
        <td>6</td>
        <td>Offer under tech vetting</td>
        <td>37</td>
        <td>1</td>
        <td>19</td>
        <td>2</td>
        <td>7</td>
        <td></td>
        <td>17</td>
        <td>17</td>
        <td></td>
        <td></td>
        <td>82</td>
        <td>20</td>
        <td>102</td>
      </tr>
      <tr>
        <td>7</td>
        <td>Under clarification/pending after offer</td>
        <td>37</td>
        <td>1</td>
        <td>19</td>
        <td>2</td>
        <td>7</td>
        <td></td>
        <td>17</td>
        <td>17</td>
        <td></td>
        <td></td>
        <td>82</td>
        <td>20</td>
        <td>102</td>
      </tr>
      <tr>
        <td>8</td>
        <td>Under prep at Sup Dte for CFA's app</td>
        <td>37</td>
        <td>1</td>
        <td>19</td>
        <td>2</td>
        <td>7</td>
        <td></td>
        <td>17</td>
        <td>17</td>
        <td></td>
        <td></td>
        <td>82</td>
        <td>20</td>
        <td>102</td>
      </tr>
      <tr>
        <td>9</td>
        <td>Under CFA's app</td>
        <td>37</td>
        <td>1</td>
        <td>19</td>
        <td>2</td>
        <td>7</td>
        <td></td>
        <td>17</td>
        <td>17</td>
        <td></td>
        <td></td>
        <td>82</td>
        <td>20</td>
        <td>102</td>
      </tr>
      <tr>
        <td>10</td>
        <td>Under acceptance at Sup Dte</td>
        <td>37</td>
        <td>1</td>
        <td>19</td>
        <td>2</td>
        <td>7</td>
        <td></td>
        <td>17</td>
        <td>17</td>
        <td></td>
        <td></td>
        <td>82</td>
        <td>20</td>
        <td>102</td>
      </tr>
      <tr>
        <td>11</td>
        <td>Acceptance given to DGDP</td>
        <td>37</td>
        <td>1</td>
        <td>19</td>
        <td>2</td>
        <td>7</td>
        <td></td>
        <td>17</td>
        <td>17</td>
        <td></td>
        <td></td>
        <td>82</td>
        <td>20</td>
        <td>102</td>
      </tr>
      <tr>
        <td>12</td>
        <td>Tech acceptance at DGDP</td>
        <td>37</td>
        <td>1</td>
        <td>19</td>
        <td>2</td>
        <td>7</td>
        <td></td>
        <td>17</td>
        <td>17</td>
        <td></td>
        <td></td>
        <td>82</td>
        <td>20</td>
        <td>102</td>
      </tr>
      <tr>
        <td>13</td>
        <td>Under prep of ESC at Sup Dte</td>
        <td>37</td>
        <td>1</td>
        <td>19</td>
        <td>2</td>
        <td>7</td>
        <td></td>
        <td>17</td>
        <td>17</td>
        <td></td>
        <td></td>
        <td>82</td>
        <td>20</td>
        <td>102</td>
      </tr>
      <tr>
        <td>14</td>
        <td>Offer Under Fin Vetting</td>
        <td>37</td>
        <td>1</td>
        <td>19</td>
        <td>2</td>
        <td>7</td>
        <td></td>
        <td>17</td>
        <td>17</td>
        <td></td>
        <td></td>
        <td>82</td>
        <td>20</td>
        <td>102</td>
      </tr>
      <tr>
        <td>15</td>
        <td>Awaiting offer</td>
        <td>37</td>
        <td>1</td>
        <td>19</td>
        <td>2</td>
        <td>7</td>
        <td></td>
        <td>17</td>
        <td>17</td>
        <td></td>
        <td></td>
        <td>82</td>
        <td>20</td>
        <td>102</td>
      </tr>
      <tr>
        <td>16</td>
        <td>Contract Signed</td>
        <td>37</td>
        <td>1</td>
        <td>19</td>
        <td>2</td>
        <td>7</td>
        <td></td>
        <td>17</td>
        <td>17</td>
        <td></td>
        <td></td>
        <td>82</td>
        <td>20</td>
        <td>102</td>
      </tr>
    </tbody>
  </table> --}}

</x-pondit-card>

@endsection