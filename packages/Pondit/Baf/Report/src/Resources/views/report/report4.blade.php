@extends('pondit-limitless::layouts.master')
@push('css')
<style>
    table,
    th,
    td {
        /* border: 1px solid black; */
        border-collapse: collapse;
    }

    p , h1{
        text-decoration: underline;
    }

    .d-flex {
        display: flex;
    }

    .justify-end {
        justify-content: flex-end;
    }
</style>
@endpush
@section('content')
<x-pondit-card title="Report">
    <h1 class="text-center">Indent Cancelled</h1>

    <div class="d-flex  justify-end">
        <div>
            <p>Date:{{ date('d-m-Y')}}</p>
            <p>Clothing Gp</p>
        </div>
    </div>
    <x-pondit-datatable>
        <x-slot name="thead">
            <tr>
                <td>SL No</td>
                <td>Indt no with Dt</td>
                <td>Tender No</td>
                <td>Description</td>
                <td>Qty</td>
                <td>FC/LC</td>
                <td>Bdgt code</td>
                <td>Indt/Proj Value</td>
                <td>Offer rcvd on</td>
                <td>Offer U/Vett</td>
                <td>Rcvd A/Vett</td>
                <td>Esc Dt</td>
                <td>Tech Accpt Dt</td>
                <td>Processed for CAF's App</td>
                <td>Rcvd A/CAF</td>
                <td>Accpt dt</td>
                <td>Accppt/Cont Value</td>
                <td>Cont dt</td>
                <td>Remark</td>
            </tr>
        </x-slot>
        <tr>
            <td>1</td>
            <td>CloU32 17.02.20</td>
            <td>037/19 </td>
            <td title="Wrist Watch Aircrew">  {{ \Illuminate\Support\Str::limit('Wrist Watch Aircrew', 20, $end='...') }}
            </td>
            <td>100 Ea</td>
            <td>FC</td>
            <td></td>
            <td>USD23,650.00</td>
            <td>31.03.20</td>
            <td>31.03.20</td>
            <td>31.03.20</td>
            <td>31.03.20</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Under Tech Vetting wef31.03.20</td>
        </tr>
    </x-pondit-datatable>


    {{-- <table>
        <thead>
            <tr>
                <td>SL No</td>
                <td>Indt no with Dt</td>
                <td>Tender No</td>
                <td>Description</td>
                <td>Qty</td>
                <td>FC/LC</td>
                <td>Bdgt code</td>
                <td>Indt/Proj Value</td>
                <td>Offer rcvd on</td>
                <td>Offer U/Vett</td>
                <td>Rcvd A/Vett</td>
                <td>Esc Dt</td>
                <td>Tech Accpt Dt</td>
                <td>Processed for CAF's App</td>
                <td>Rcvd A/CAF</td>
                <td>Accpt dt</td>
                <td>Accppt/Cont Value</td>
                <td>Cont dt</td>
                <td>Remark</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td>CloU32 17.02.20</td>
                <td>037/19 </td>
                <td>Wrist Watch Aircrew</td>
                <td>100 Ea</td>
                <td>FC</td>
                <td></td>
                <td>USD23,650.00</td>
                <td>31.03.20</td>
                <td>31.03.20</td>
                <td>31.03.20</td>
                <td>31.03.20</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>Under Tech Vetting wef31.03.20</td>
            </tr>
            <tr>
                <td>1</td>
                <td>CloU32 17.02.20</td>
                <td>037/19 </td>
                <td>Helmet Flg(For MI Series Hel)</td>
                <td>100 Ea</td>
                <td>FC</td>
                <td></td>
                <td>USD23,650.00</td>
                <td>31.03.20</td>
                <td>31.03.20</td>
                <td>31.03.20</td>
                <td>31.03.20</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>Under Tech Vetting wef31.03.20</td>
            </tr>
        </tbody>
    </table> --}}

</x-pondit-card>

@endsection