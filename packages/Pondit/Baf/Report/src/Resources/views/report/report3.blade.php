@extends('pondit-limitless::layouts.master')
@push('css')
<style>
    table, th, td {
/* border: 1px solid black; */
border-collapse: collapse;
}
p, h1{
text-decoration: underline;
}
.d-flex{
   display: flex;
}
.justify-between{
   justify-content: space-between;
}
.align-self-end{
   align-self:flex-end
}
  </style>
@endpush
@section('content')
<x-pondit-card title="Report">
    <h1 class="text-center">Under Tech Vetting</h1>
    <div class="d-flex justify-between">
        <p class="align-self-end"></p>
        <div>
            <p>Date: {{date('d-m-Y')}}</p>
            <p>DTE SUP, SPS</p>
        </div>
    </div>
    <x-pondit-datatable>
        <x-slot name="thead">
            <tr>
                <td>SL No</td>
                <td>Indt no with Dt</td>
                <td>Tender No</td>
                <td>Description</td>
                <td>Qty</td>
                <td>FC/LC</td>
                <td>Bdgt code</td>
                <td>Indt/Proj Value</td>
                <td>Offer rcvd on</td>
                <td>Offer U/Vett</td>
                <td>Rcvd A/Vett</td>
                <td>Esc Dt</td>
                <td>Tech Accpt Dt</td>
                <td>Processed for CAF's App</td>
                <td>Rcvd A/CAF</td>
                <td>Accpt dt</td>
                <td>Accppt/Cont Value</td>
                <td>Cont dt</td>
                <td>Remark</td>
            </tr>
        </x-slot>
        <tr>
            <td>1</td>
            <td>CloU32 17.02.20</td>
            <td></td>
            <td title="Emergency Medical Service(EMS) Kit with Medical Floor   Platform(For Bell-212 and M117 Series Helicopter)">  {{ \Illuminate\Support\Str::limit('Emergency Medical Service(EMS) Kit with Medical Floor   Platform(For Bell-212 and M117 Series Helicopter)', 20, $end='...') }} </td>
            <td>02 Set</td>
            <td>FC</td>
            <td>40,00,00,000</td>
            <td>30.03.20</td>
            <td>31.03.20</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Under Tech Vetting wef31.03.20</td>
        </tr>
    </x-pondit-datatable>

    {{-- <table>
        <thead>
            <tr>
                <td>SL No</td>
                <td>Indt no with Dt</td>
                <td>Tender No</td>
                <td>Description</td>
                <td>Qty</td>
                <td>FC/LC</td>
                <td>Bdgt code</td>
                <td>Indt/Proj Value</td>
                <td>Offer rcvd on</td>
                <td>Offer U/Vett</td>
                <td>Rcvd A/Vett</td>
                <td>Esc Dt</td>
                <td>Tech Accpt Dt</td>
                <td>Processed for CAF's App</td>
                <td>Rcvd A/CAF</td>
                <td>Accpt dt</td>
                <td>Accppt/Cont Value</td>
                <td>Cont dt</td>
                <td>Remark</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td>CloU32 17.02.20</td>
                <td>Emergency Medical Service(EMS) Kit with Medical Floor 
                    Platform(For Bell-212 and M117 Series Helicopter) </td>
                <td>02 Set</td>
                <td>FC</td>
                <td></td>
                <td>40,00,00,000</td>
                <td>30.03.20</td>
                <td>31.03.20</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>Under Tech Vetting wef31.03.20</td>
            </tr>
        </tbody>
    </table>  --}}
  

</x-pondit-card>

@endsection