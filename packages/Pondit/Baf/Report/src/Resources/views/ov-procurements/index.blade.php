@extends('pondit-limitless::layouts.master')

@section('content')

<x-pondit-card title="Report">
    <x-pondit-datatable>
        <x-slot name="thead">
            <tr>
                <th>{{__('SL')}}</th>
                <th>{{__('reports')}}</th>
                <th>{{__('procurement')}}</th>
                <th>{{__('actions')}}</th>
            </tr>
        </x-slot>
        @foreach ($data as $key=>$datam)
        <tr>
            <td>{{ ++$key }}</td>
            <td>{{ $datam->title }}</td>
            <td>{{ $datam->procurement_id }}</td>
            <td class="d-flex">
                <x-pondit-act-link url="{{route('ov-procurements.show', $datam->id)}}" icon="eye" bg="success" />
                <x-pondit-act-link url="{{route('ov-procurements.edit', $datam->id)}}" icon="pen" bg="primary" />
                <x-pondit-act-link-d url="{{route('ov-procurements.delete', $datam->id)}}" icon="trash" />
            </td>
        </tr>
        @endforeach
    </x-pondit-datatable>
    <x-slot name="cardFooter">
        <div></div>
        <div>
            <x-pondit-act-c url="{{route('ov-procurements.create')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection