@extends('pondit-limitless::layouts.master')
@section('content')

<x-pondit-card title="Report">
    <x-pondit-form action="{{route('ov-procurements.store')}}">
        <div class='form-group row'>
            <label class="col-sm-2 col-form-label">Procurement</label>
            <div class='col-sm-10'>
                <input type="text" name="procurement_id" class="form-control">
            </div>
        </div>
        <div class='form-group row'>
            <label name="title" class="col-sm-2 col-form-label">Title</label>
            <div class='col-sm-10'>
                <input type="text" name="title" class="form-control" />
            </div>
        </div>
        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>
    <x-slot name="cardFooter">
        <div></div>
        <div>
            <x-pondit-act-i url="{{route('ov-procurements.index')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection