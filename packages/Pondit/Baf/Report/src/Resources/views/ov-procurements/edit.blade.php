@extends('pondit-limitless::layouts.master')
@section('content')
<x-pondit-card title="Report">

    <x-pondit-form action="{{ route('ov-procurements.update', $data->id) }}">
        @method('PUT')
        <div class='form-group row'>
            <label name="procurement_id" class="col-sm-2 col-form-label">{{__('Procurement')}}</label>
            <div class='col-sm-10'>
                <input type="text" name="Procurement" value="{{old('procurement_id') ?? $data->procurement_id }}" class="form-control"/>
            </div>
        </div>
        <div class='form-group row'>
        <label name="title" class="col-sm-2 col-form-label">{{__('title')}}</label>
            <div class='col-sm-10'>
            <input type="text" name="title" value="{{old('title') ?? $data->title }}" class="form-control"/>
            </div>
        </div>
        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>

    <x-slot name="cardFooter">
        <div></div>
        <div class="d-flex">
            <x-pondit-act-i url="{{route('ov-procurements.index')}}" tooltip="{{__('list')}}"/>
            <x-pondit-act-c url="{{route('ov-procurements.create')}}" tooltip="{{__('create')}}"/>
            <x-pondit-act-v url="{{route('ov-procurements.show', $data->id)}}" tooltip="{{__('show')}}"/>
            <x-pondit-act-d url="{{route('ov-procurements.delete', $data->id)}}" tooltip="{{__('remove')}}"/>
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection