<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterdataBudgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('masterdata_budgets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fin_year')->nullable();
            $table->string('total_amount', 64)->nullable();
            $table->integer('no_of_supplimentary_budget')->nullable();
            $table->string('created_by', 64)->nullable();
            $table->string('updated_by', 64)->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        Schema::dropIfExists('masterdata_budgetcodes', function (Blueprint $table) {

            $table->dropSoftDeletes();

        });
    }
}