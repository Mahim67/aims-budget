<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterdataBudgetDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('masterdata_budget_details', function (Blueprint $table) {
            $table->id();
            $table->integer('budget_id');
            $table->integer('budgetcode_id')->nullable();
            $table->string('amount',64)->nullable();
            $table->integer('is_initial')->nullable();
            $table->integer('supplimentary_budget_no')->nullable();
            $table->string('created_by', 64)->nullable();
            $table->string('updated_by', 64)->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        Schema::dropIfExists('masterdata_budgetcodes', function (Blueprint $table) {

            $table->dropSoftDeletes();

        });
    }
}