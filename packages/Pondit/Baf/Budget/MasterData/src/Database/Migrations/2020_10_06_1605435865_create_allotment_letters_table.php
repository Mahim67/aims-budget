<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAllotmentLettersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('allotment_letters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('introduced_fin_year')->nullable();
            $table->string('alloted_letter', 64)->nullable();
            $table->string('alloted_file', 64)->nullable();
            $table->unsignedInteger('sequence_number')->nullable();
            $table->string('created_by', 64)->nullable();
            $table->string('updated_by', 64)->nullable();
            $table->string('deleted_by', 64)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        Schema::dropIfExists('allotment_letters', function (Blueprint $table) {

            $table->dropSoftDeletes();

        });
    }
}