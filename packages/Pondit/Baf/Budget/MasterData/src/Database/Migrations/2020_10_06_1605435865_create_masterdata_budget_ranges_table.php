<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterdataBudgetRangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('masterdata_budget_ranges', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('budget_id')->nullable();
            $table->integer('range_id')->nullable();
            $table->string('budgetcode', 64)->nullable();
            $table->string('created_by', 64)->nullable();
            $table->string('updated_by', 64)->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        Schema::dropIfExists('masterdata_budgetcodes', function (Blueprint $table) {

            $table->dropSoftDeletes();

        });
    }
}