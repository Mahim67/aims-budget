<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterdataBudgetcodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('masterdata_budgetcodes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('range_id')->nullable();
            $table->string('old_code', 64)->nullable();
            $table->string('new_code')->nullable()->unique();
            $table->string('budget_head', 64)->nullable();
            $table->string('activity_type')->nullable();
            $table->text('description')->nullable();
            $table->unsignedInteger('sequence_number')->nullable();
            $table->string('created_by', 64)->nullable();
            $table->string('updated_by', 64)->nullable();
            $table->string('deleted_by', 64)->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        Schema::dropIfExists('masterdata_budgetcodes', function (Blueprint $table) {

            $table->dropSoftDeletes();

        });
    }
}