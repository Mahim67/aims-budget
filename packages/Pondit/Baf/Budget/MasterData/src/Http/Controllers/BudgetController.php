<?php

namespace Pondit\Baf\Budget\MasterData\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pondit\Baf\Budget\MasterData\Models\Budgetcode;

class BudgetController extends Controller
{
    
    public function index()
    {
        $data = Budgetcode::all();
        return view('budget::budgets.index', \compact('data'));
    }
    public function create()
    {
        return view('budget::budgets.create');
    }
    public function store(Request $request, Budgetcode $budgetcode)
    {
        // dd($request->all());
        try
        {
            
            $sequenceNumber = 1;
            $sequence = Budgetcode::orderBy('id', 'desc')->first();

            $sequenceNumber  = $sequence ? $sequence->sequence_number+1 : $sequenceNumber;
            // dd($sequenceNumber);        

            $data  = new Budgetcode();
            $data->sequence_number      = $sequenceNumber;
            $data->description          = $request->description;
            // dd($data);
            $data->save();
            return redirect()
                    ->route('budget.index')
                    ->withMessage('Entity has been created successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                    ->route('budget.create')
                    ->withErrors($th->getMessage());
        }
    }
    public function show($id)
    {
        $data = Budgetcode::findOrFail($id);
        return \view('budget::budgets.show', \compact('data'));
    }
    public function edit(Budgetcode $budgetcode, $id)
    {
        $data = Budgetcode::findOrFail($id);
        return \view('budget::budgets.edit', \compact('data'));
    }
    public function update(Request $request, Budgetcode $budgetcode, $id)
    {
        try{
            // dd($request->all());
            $data = Budgetcode::findOrFail($id);
            // dd($data);
            // Field updating goes here
            
            $data->description          = $request->description;
            // dd($data);
            $data->save();

            $data->reArrangeSequence($request->sequence_number);

            return redirect()
                ->route('budget.index')
                ->withMessage('Entity has been updated successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                        ->route('budget.edit')
                        ->withErrors($th->getMessage());
        }
    }

    public function destroy($id)
    {
        $data = Budgetcode::findOrFail($id);
        // dd($data);
        $data->delete();
        $data->reArrangeSequenceOnDelete($data->sequence_number);

        return redirect()
                    ->route('budget.index')
                    ->withMessage('Entity has been deleted successfully!');
    }
    
    
}