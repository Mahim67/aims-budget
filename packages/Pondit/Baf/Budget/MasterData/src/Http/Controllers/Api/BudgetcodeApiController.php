<?php

namespace Pondit\Baf\Budget\MasterData\Http\Controllers\Api;

use Exception;
use Illuminate\Http\Request;

use Pondit\Baf\Range\Models\Range;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Pondit\Baf\Budget\MasterData\Models\Budgetcode;
use Pondit\Baf\Budget\MasterData\Http\Helpers\BudgetcodeSlug;

class BudgetcodeApiController extends Controller
{
    public function budgetcode ()
    {
        $ranges = Range::all();
        return view('budget::budgetcodes.api.index',compact('ranges'));
    }

    public function index()
    {
        $data = Budgetcode::all();
        
        return response()->json($data);
    }

    public function store(Request $request)
    {
        // dd($request->all());
        try
        {
            $validatedData = $request->validate([
                'new_code' => 'unique:masterdata_budgetcodes',
            ]); 


            // $slug  = new BudgetcodeSlug();
            
            $sequenceNumber = 1;
            $sequence = Budgetcode::orderBy('id', 'desc')->first();

            $sequenceNumber  = $sequence ? $sequence->sequence_number+1 : $sequenceNumber;
            // dd($sequenceNumber);        

            $data = [
                'sequence_number'           => $sequenceNumber ,
                'old_code'                  => $request->old_code ,
                'new_code'                  => $request->new_code ,
                'activity_type'             => $request->activity_type ,
                'description'               => $request->description ,
            ];

            $range = $request->range;

            $budget = Budgetcode::create($data);
            $budget->range()->attach($range);

            if(!$budget)
                throw new Exception("Unable To Create", 1);
                
            return response()->json($budget);
        }
        catch (QueryException $th)
        {
            return response()->json($th->getMessage());
        }
    }

    public function show($id)
    {
        $data = Budgetcode::with('range')->findOrFail($id);

        return response()->json($data);
    }

    public function edit($id)
    {
        $data = Budgetcode::findOrFail($id);

        return response()->json($data);
    }

    public function update(Request $request, Budgetcode $budgetcode, $id)
    {
        try{
            // dd($request->all());
            $data = Budgetcode::where('id', $id)->first();
            // dd($data);
            $slug  = new BudgetcodeSlug();
            // Field updating goes here
            
            $data->introduced_fin_year  = $request->introduced_fin_year;
            $data->old_code             = $request->old_code;
            $data->new_code             = $request->new_code;
            if ($data->new_code != $request->new_code) {
                $data->new_code = $slug->createSlug($request->new_code, $id);
            }
            $data->activity_type        = $request->activity_type;
            $data->budget_head          = $request->budget_head;
            $data->description          = $request->description;
            // dd($data);
            $data->reArrangeSequence($request->sequence_number);

            $data->save();

            return response()->json($data);
        }
        catch (QueryException $th)
        {
            return response()->json($th->getMessage());
        }
       
    }

    public function destroy($id)
    {
       try {
            $data = Budgetcode::findOrFail($id);
            $range = $data->range;
            $data->range()->detach($range);
            $data->delete();
            return response()->json($data);

       } catch (QueryException $th) {
           return response()->json($th->getMessage());
       }

    }

    public function trash(){
        $data = Budgetcode::onlyTrashed()->orderBy('created_at', 'desc')->get();
        
    	return response()->json($data);
    }

    public function restore($id){
    	Budgetcode::onlyTrashed()->where('id', $id)
                                    ->first()
                                    ->restore();
        return response()->json($this->prepareResponse(true, 'Entity has been restored successfully!'));
                            
    }

    public function permanentDelete($id){
       
        $data = Budgetcode::onlyTrashed()->where('id', $id)->first();

        $data->forceDelete();
           return response()->json(['status'=>'Entity has been Permanently Delete Successfully']);

    }

}
