<?php

namespace Pondit\Baf\Budget\MasterData\Http\Controllers\Api;

use Exception;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Pondit\Baf\Budget\MasterData\Models\Budget;
use Pondit\Baf\Budget\MasterData\Http\Helpers\BudgetcodeSlug;

class BudgetApiController extends Controller
{
    public function budget ()
    {
        return view('budget::budgets.api.index');
    }

    public function index()
    {
        $data = Budget::all();
        
        return response()->json($data);
    }

    public function store(Request $request)
    {
        try
        {
            $data = [
                'fin_year'                      => $request->fin_year,
                'total_amount'                  => $request->total_amount,
                'no_of_supplimentary_budget'    => 0,
            ];

            $budget = Budget::create($data);
             
            if(!$budget)
                throw new Exception("Unable To Insart", 1);
                
            return response()->json($budget);
        }
        catch (QueryException $th)
        {
            return response()->json($th->getMessage());
        }
    }

    public function show($id)
    {
        $data = Budget::findOrFail($id);

        return response()->json($data);
    }

    public function edit($id)
    {
        $data = Budget::findOrFail($id);

        return response()->json($data);
    }

    public function update(Request $request, $id)
    {
        try{
 
            $data = [
                'fin_year'                      => $request->fin_year,
                'total_amount'                  => $request->total_amount,
                'no_of_supplimentary_budget'    => 0,
            ];

            $budget = Budget::where('id' , $id)->update($data);

            if(!$budget)
                throw new Exception("Unable TO Update", 1);
                
            return response()->json($budget);
        }
        catch (QueryException $th)
        {
            return response()->json($th->getMessage());
        }
       
    }

    public function destroy($id)
    {
       try {
            $data = Budget::findOrFail($id);
            
            if(!$data)
                throw new Exception("Data Not Found", 404);
                
            $data->delete();
            return response()->json($data);

       } catch (QueryException $th) {
           return response()->json($th->getMessage());
       }

    }

}
