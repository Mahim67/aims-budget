<?php

namespace Pondit\Baf\Budget\MasterData\Http\Controllers\Api;

use Exception;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Monolog\Handler\ElasticaHandler;
use Pondit\Baf\Budget\MasterData\Models\Budget;
use Pondit\Baf\Budget\MasterData\Http\Helpers\BudgetcodeSlug;
use Pondit\Baf\Budget\MasterData\Models\Budgetcode;
use Pondit\Baf\Budget\MasterData\Models\BudgetDetails;

class BudgetDetailsController extends Controller
{
    public function budgetDetails ()
    {
        $budgets = Budget::all()->pluck('fin_year','id')->toArray();
        $budget_codes = Budgetcode::all()->pluck('new_code','id')->toArray();
        return view('budget::budget-details.api.index',compact('budgets','budget_codes'));
    }

    public function index()
    {
        $data = BudgetDetails::selectRaw('*,masterdata_budget_details.id as id')
                                ->leftJoin('masterdata_budgets', 'masterdata_budgets.id', '=', 'masterdata_budget_details.budget_id')
                                ->join('masterdata_budgetcodes', 'masterdata_budgetcodes.id', '=', 'masterdata_budget_details.budgetcode_id')
                                ->get();
         
        return response()->json($data);
    }

    public function store(Request $request)
    {
        try
        {
            if($request->is_initial == true){
                $is_initial = 1;
            }else{
                $is_initial = 0;
            }

            $supplimentary_budget_no = 1;
            $hasSupplimantry = BudgetDetails::where('budgetcode_id',$request->budgetcode_id)->latest()->first();

            $supplimentary_budget_no = $hasSupplimantry ? $hasSupplimantry->supplimentary_budget_no+1 : $supplimentary_budget_no;


            $data = [
                'budget_id'                      => $request->budget_id,
                'budgetcode_id'                  => $request->budgetcode_id,
                'amount'                         => $request->amount,      
                'is_initial'                     => $is_initial,      
                'supplimentary_budget_no'        => $supplimentary_budget_no,      
            ];


            $budget = BudgetDetails::create($data);
             
            if(!$budget)
                throw new Exception("Unable To Insart", 1);
                
            return response()->json($budget);
        }
        catch (QueryException $th)
        {
            return response()->json($th->getMessage());
        }
    }

    public function show($id)
    {
        $data = BudgetDetails::findOrFail($id);

        return response()->json($data);
    }

    public function edit($id)
    {
        $data = BudgetDetails::findOrFail($id);

        return response()->json($data);
    }

    public function update(Request $request, $id)
    {
        try{
 
            $data = [
                'fin_year'                      => $request->fin_year,
                'total_amount'                  => $request->total_amount,
                'no_of_supplimentary_budget'    => 0,
            ];

            $budget = BudgetDetails::where('id' , $id)->update($data);

            if(!$budget)
                throw new Exception("Unable TO Update", 1);
                
            return response()->json($budget);
        }
        catch (QueryException $th)
        {
            return response()->json($th->getMessage());
        }
       
    }

    public function destroy($id)
    {
       try {
            $data = BudgetDetails::findOrFail($id);
            
            if(!$data)
                throw new Exception("Data Not Found", 404);
                
            $data->delete();
            return response()->json($data);

       } catch (QueryException $th) {
           return response()->json($th->getMessage());
       }

    }

}
