<?php

namespace Pondit\Baf\Budget\MasterData\Http\Controllers;

use File;
use Image;
use Excel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pondit\Baf\Budget\MasterData\Models\AllotmentLetter;
use Pondit\Baf\Budget\MasterData\Imports\BudgetcodeImport;

class AllotmentLetterController extends Controller
{
    public function store(Request $request)
    {
        // dd($request->alloted_letter);
        try
        {
            
            $sequenceNumber = 1;
            $sequence = AllotmentLetter::orderBy('id', 'desc')->first();

            $sequenceNumber  = $sequence ? $sequence->sequence_number+1 : $sequenceNumber;
            // dd($sequenceNumber);        
            $data  = new AllotmentLetter();
            if ($request->hasFile('alloted_letter')) {
                $data['alloted_letter'] = $this->uploadFile($request->alloted_letter, $request->introduced_fin_year);
            }
            if ($request->hasFile('alloted_file')) {
                $data['alloted_file'] = $this->uploadFile($request->alloted_file, $request->introduced_fin_year);
            }
          
            $data->introduced_fin_year      = $request->introduced_fin_year;
            $data->sequence_number      = $sequenceNumber;
            // dd($data);
            $data->save();
            
            // dd($request->alloted_file);
            $import = Excel::import(new BudgetcodeImport, $request->alloted_file);
            dd($import);

            return redirect()
                    ->route('budgetcode.index')
                    ->withMessage('Entity has been created successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                    ->route('budgetcode.upload')
                    ->withErrors($th->getMessage());
        }
    }
    
    private function uploadFile($file, $name)
    {
        // dd($file->getClientOriginalName());
        $timestamp = str_replace([' ', ':'], '_', Carbon::now()->toDateString());
        $file_name = $timestamp .'_'.'('.$name. ')_' .$file->getClientOriginalName();
        if (! file_exists(public_path()."/uploads/budget")) {
            File::makeDirectory(public_path()."/uploads/budget");
        }
        $pathToUpload = $file->move( (public_path()."/uploads/budget/".$name), $file_name);
        return $file_name;
    }

    private function unlink($file)
    {
        $pathToUpload = storage_path().'/app/public/workflow-templates/';
        if ($file != '' && file_exists($pathToUpload. $file)) {
            @unlink($pathToUpload. $file);
        }
    }
}