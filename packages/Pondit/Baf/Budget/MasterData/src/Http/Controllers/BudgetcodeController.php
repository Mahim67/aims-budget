<?php

namespace Pondit\Baf\Budget\MasterData\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pondit\Baf\Budget\MasterData\Models\Budgetcode;
use Pondit\Baf\Budget\MasterData\Http\Helpers\BudgetcodeSlug;


class BudgetcodeController extends Controller
{
    public function upload()
    {
        return view('budget::budgetcodes.upload');
    }

    public function index()
    {
        $data = Budgetcode::all();
        return view('budget::budgetcodes.index', \compact('data'));
    }
    public function create()
    {
        return view('budget::budgetcodes.create');
    }
    public function store(Request $request, Budgetcode $budgetcode)
    {
        // dd($request->all());
        try
        {
            $slug  = new BudgetcodeSlug();
            
            $sequenceNumber = 1;
            $sequence = Budgetcode::orderBy('id', 'desc')->first();

            $sequenceNumber  = $sequence ? $sequence->sequence_number+1 : $sequenceNumber;
            // dd($sequenceNumber);        

            $data  = new Budgetcode();
            $data->sequence_number      = $sequenceNumber;
            $data->introduced_fin_year  = $request->introduced_fin_year;
            $data->old_code             = $request->old_code;
            $data->new_code             = $slug->createSlug($request->new_code);
            $data->activity_type        = $request->activity_type;
            $data->budget_head          = $request->budget_head;
            $data->description          = $request->description;
            // dd($data);
            $data->save();
            return redirect()
                    ->route('budgetcode.index')
                    ->withMessage('Entity has been created successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                    ->route('budgetcode.create')
                    ->withErrors($th->getMessage());
        }
    }
    public function show($id)
    {
        $data = Budgetcode::where('new_code', $id)->first();
        return \view('budget::budgetcodes.show', \compact('data'));
    }
    public function edit(Budgetcode $budgetcode, $id)
    {
        $data = Budgetcode::where('new_code', $id)->first();
        return \view('budget::budgetcodes.edit', \compact('data'));
    }
    public function update(Request $request, Budgetcode $budgetcode, $id)
    {
        try{
            // dd($request->all());
            $data = Budgetcode::where('new_code', $id)->first();
            // dd($data);
            $slug  = new BudgetcodeSlug();
            // Field updating goes here
            
            $data->introduced_fin_year  = $request->introduced_fin_year;
            $data->old_code             = $request->old_code;
            $data->new_code             = $request->new_code;
            if ($data->new_code != $request->new_code) {
                $data->new_code = $slug->createSlug($request->new_code, $id);
            }
            $data->activity_type        = $request->activity_type;
            $data->budget_head          = $request->budget_head;
            $data->description          = $request->description;
            // dd($data);
            $data->save();

            $data->reArrangeSequence($request->sequence_number);

            return redirect()
                ->route('budgetcode.index')
                ->withMessage('Entity has been updated successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                        ->route('budgetcode.edit', $id)
                        ->withErrors($th->getMessage());
        }
    }

    public function destroy($id)
    {
        $data = Budgetcode::where('new_code', $id)->first();
        // dd($data);
        $data->delete();
        $data->reArrangeSequenceOnDelete($data->sequence_number);

        return redirect()
                    ->route('budgetcode.index')
                    ->withMessage('Entity has been deleted successfully!');
    }
    
    
}