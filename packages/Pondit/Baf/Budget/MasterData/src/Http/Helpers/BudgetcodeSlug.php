<?php

namespace Pondit\Baf\Budget\MasterData\Http\Helpers;

use Illuminate\Support\Str;
use Illuminate\Database\QueryException;
use Pondit\Baf\Budget\MasterData\Models\Budgetcode;

class BudgetcodeSlug
{
    
    public function createSlug($title, $id = 0)
    {
        try {
            // Normalize the title
            $slug = Str::slug($title);
            // Get any that could possibly be related.
            // This cuts the queries down by doing it once.
            $allSlugs = $this->getRelatedSlugs($slug, $id);
            // If we haven't used it before then we are all good.
            if (! $allSlugs->contains('new_code', $slug)){
                return $slug;
            }
            // Just append numbers like a savage until we find not used.
            for ($i = 1; $i <= 10; $i++) {
                $newSlug = $slug.'-'.$i;
                if (! $allSlugs->contains('new_code', $newSlug)) {
                    return $newSlug;
                }
            }
        } catch (QueryException $th) {

            return redirect()->withErrors($th->getMessage());
        }
        
    }

    protected function getRelatedSlugs($slug, $id = 0)
    {
        return Budgetcode::select('new_code')->where('new_code', 'like', $slug.'%')
            ->where('id', '<>', $id)
            ->get();
    }

}