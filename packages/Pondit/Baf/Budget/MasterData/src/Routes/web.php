<?php

use Illuminate\Support\Facades\Route;
use Pondit\Baf\Budget\MasterData\Http\Controllers\BudgetController;
use Pondit\Baf\Budget\MasterData\Http\Controllers\BudgetcodeController;
use Pondit\Baf\Budget\MasterData\Http\Controllers\AllotmentLetterController;


Route::group(['prefix' => 'masterdata', 'middleware' => ['web']], function () {
    
    Route::group(['prefix' => 'budgetcodes', 'as' => 'budgetcode.'], function () {
        Route::get('/', [BudgetcodeController::class, 'index'])->name('index');
        Route::get('/create', [BudgetcodeController::class, 'create'])->name('create');
        Route::get('/show/{id}', [BudgetcodeController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [BudgetcodeController::class, 'edit'])->name('edit');
        Route::post('/', [BudgetcodeController::class, 'store'])->name('store');
        Route::put('/update/{id}', [BudgetcodeController::class, 'update'])->name('update');
        Route::post('/{id}', [BudgetcodeController::class, 'destroy'])->name('delete');

        Route::get('/upload', [BudgetcodeController::class, 'upload'])->name('upload');
    });
    
    Route::group(['prefix' => 'budgets', 'as' => 'budget.'], function () {
        Route::get('/', [BudgetController::class, 'index'])->name('index');
        Route::get('/create', [BudgetController::class, 'create'])->name('create');
        Route::get('/show/{id}', [BudgetController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [BudgetController::class, 'edit'])->name('edit');
        Route::post('/', [BudgetController::class, 'store'])->name('store');
        Route::put('/update/{id}', [BudgetController::class, 'update'])->name('update');
        Route::post('/{id}', [BudgetController::class, 'destroy'])->name('delete');
    });
    Route::group(['prefix' => 'allotment-letter','as' => 'allotmentLetter.'], function () {
        Route::post('/', [AllotmentLetterController::class, 'store'])->name('store');
    });
});