<?php

use Illuminate\Support\Facades\Route;
use Pondit\Baf\Budget\MasterData\Http\Controllers\Api\BudgetApiController;
use Pondit\Baf\Budget\MasterData\Http\Controllers\Api\BudgetcodeApiController;
use Pondit\Baf\Budget\MasterData\Http\Controllers\Api\BudgetDetailsController;


Route::group(['prefix' => 'masterdata/api', 'middleware' => ['web']], function () {
    
    Route::group(['prefix' => 'budgetcodes', 'as' => 'budgetcode-api.'], function () {

        Route::get('/budget', [BudgetcodeApiController::class, 'budgetcode'])->name('index');

        Route::get('/', [BudgetcodeApiController::class, 'index']);
        Route::post('/store', [BudgetcodeApiController::class, 'store']);
        Route::get('/show/{id}', [BudgetcodeApiController::class, 'show']);
        Route::put('/update/{id}', [BudgetcodeApiController::class, 'update']);
        Route::post('/delete/{id}', [BudgetcodeApiController::class, 'destroy']);
    });

    Route::group(['prefix' => 'budgets', 'as' => 'budget-api.'], function () {

        Route::get('/budget', [BudgetApiController::class, 'budget'])->name('index');

        Route::get('/', [BudgetApiController::class, 'index']);
        Route::post('/store', [BudgetApiController::class, 'store']);
        Route::get('/show/{id}', [BudgetApiController::class, 'show']);
        Route::put('/update/{id}', [BudgetApiController::class, 'update']);
        Route::post('/delete/{id}', [BudgetApiController::class, 'destroy']);
    });

    Route::group(['prefix' => 'budgets-details', 'as' => 'budget-details-api.'], function () {

        Route::get('/budget-details', [BudgetDetailsController::class, 'budgetDetails'])->name('index');

        Route::get('/', [BudgetDetailsController::class, 'index']);
        Route::post('/store', [BudgetDetailsController::class, 'store']);
        Route::get('/show/{id}', [BudgetDetailsController::class, 'show']);
        Route::put('/update/{id}', [BudgetDetailsController::class, 'update']);
        Route::post('/delete/{id}', [BudgetDetailsController::class, 'destroy']);
    });



});