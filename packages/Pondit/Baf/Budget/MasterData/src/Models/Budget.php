<?php

namespace Pondit\Baf\Budget\MasterData\Models;

use App\Traits\RecordSequenceable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Budget extends Model
{
    use  RecordSequenceable;
    

    protected $table    = 'masterdata_budgets';

    protected $fillable = ['fin_year'
                            ,'total_amount'
                            ,'no_of_supplimentary_budget'
                            ,'created_by'
                            ,'updated_by'
                        ];

    public function BudgetDetails(){
        return $this->hasMany(BudgetDetails::class);
    }                        
}
