<?php

namespace Pondit\Baf\Budget\MasterData\Models;

use App\Traits\RecordSequenceable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BudgetDetails extends Model
{
    use  RecordSequenceable;
    

    protected $table    = 'masterdata_budget_details';

    protected $fillable = ['budget_id'
                            ,'budgetcode_id'
                            ,'amount'
                            ,'is_initial'
                            ,'supplimentary_budget_no'
                            ,'created_by'
                            ,'updated_by'
                        ];

    public function budget(){
        return $this->belongsTo(Budget::class);
    }

    public function budgetCode(){
        return $this->belongsTo(Budgetcode::class);
    }

}
