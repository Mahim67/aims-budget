<?php

namespace Pondit\Baf\Budget\MasterData\Models;

use App\Traits\RecordSequenceable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AllotmentLetter extends Model
{
    use SoftDeletes, RecordSequenceable;
    
    protected $dates    = ['deleted_at'];

    protected $table    = 'allotment_letters';

    protected $fillable = ['id'
                            ,'sequence_number'
                            ,'introduced_fin_year'
                            ,'alloted_letter'
                            ,'alloted_file'
                        ];
}
