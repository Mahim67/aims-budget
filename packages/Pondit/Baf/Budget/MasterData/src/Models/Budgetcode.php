<?php

namespace Pondit\Baf\Budget\MasterData\Models;

use App\Traits\RecordSequenceable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Pondit\Baf\Range\Models\Range;

class Budgetcode extends Model
{
    use SoftDeletes, RecordSequenceable;
    
    protected $dates    = ['deleted_at'];

    protected $table    = 'masterdata_budgetcodes';

    protected $fillable = ['id'
                            ,'new_code'
                            ,'old_code'
                            ,'sequence_number'
                            ,'budget_head'
                            ,'activity_type'
                            ,'introduced_fin_years'
                            ,'description'
                        ];

    public function range(){
        return $this->belongsToMany(Range::class,'range_budgetcode','budgetcode_id','range_id');
    }
}
