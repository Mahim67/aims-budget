<?php

namespace Pondit\Baf\Budget\MasterData\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class BudgetcodeImport implements ToModel,WithHeadingRow
{
    public function model(array $row)
    {
        $budgetcode = new Budgetcode([
            "new_code"  =>  $row["new_code"],
            "old_code"  =>  $row["old_code"],
            "description"  =>  $row["description"],
        ]);
        // dd($budgetcode);
        return $budgetcode;
    }
}