@extends('pondit-limitless::layouts.master')
@section('content')
<x-pondit-card title="Budget Codes">
    {{-- {{ csrf_token() }} --}}
    <x-pondit-form action="{{ route('budgetcode.update', $data->new_code) }}">
        @method('PUT')
        <x-pondit-fin-year name="introduced_fin_year" label="Financial Year" />

        <div class='form-group row'>
            <label name="sequence_number" class="col-sm-2 col-form-label">Sequence Number</label>
            <div class='col-sm-10'>
                <input type="number" name="sequence_number" value="{{old('sequence_number') ?? $data->sequence_number }}" class="form-control" required/>
            </div>
        </div>
        <div class='form-group row'>
            <label name="new_code" class="col-sm-2 col-form-label">New Code</label>
            <div class='col-sm-10'>
            <input type="text" name="new_code" value="{{old('new_code') ?? $data->new_code }}" class="form-control"/>
            </div>
        </div>
        <div class='form-group row'>
            <label name="old_code" class="col-sm-2 col-form-label">Old Code</label>
            <div class='col-sm-10'>
                <input type="text" name="old_code" value="{{old('old_code') ?? $data->old_code   }}" class="form-control"/>
            </div>
        </div>
        <div class='form-group row'>
            <label for="activity_type" class="col-sm-2 col-form-label">Activity Type</label>
            <div class='col-sm-10'>
                <select id="activity_type" name="activity_type" class="form-control select">
                    <option value="general" {{ $data->activity_type == 'general'? 'selected': '' }}>General</option>
                    <option value="special" {{ $data->activity_type == 'special'? 'selected': '' }}>Special</option>
                </select>
            </div>
        </div>
        <div class='form-group row'>
            <label for="budget_head" class="col-sm-2 col-form-label">Budget Head</label>
            <div class='col-sm-10'>
                <input type="text" name="budget_head" value="{{old('budget_head') ?? $data->budget_head   }}" class="form-control"/>
            </div>
        </div>
        <div class='form-group row'>
            <label class="col-sm-2 col-form-label">Description</label>
            <div class='col-sm-10'>
            <input type="text" name="description" value="{{old('description') ?? $data->description}}" class="form-control">
            </div>
        </div>
        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>

    <x-slot name="cardFooter">
        <div></div>
        <div class="d-flex">
            <x-pondit-act-i url="{{route('budgetcode.index')}}" tooltip="{{__('list')}}"/>
            <x-pondit-act-c url="{{route('budgetcode.create')}}" tooltip="{{__('create')}}"/>
            <x-pondit-act-v url="{{route('budgetcode.show', $data->new_code)}}" tooltip="{{__('show')}}"/>
            <x-pondit-act-d url="{{route('budgetcode.show', $data->new_code)}}" tooltip="{{__('remove')}}"/>
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection