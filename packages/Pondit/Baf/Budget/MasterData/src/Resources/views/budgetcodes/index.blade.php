@extends('pondit-limitless::layouts.master')

@section('content')
<x-pondit-card title="Budget Codes">
    {{-- {{ csrf_token() }} --}}
    <x-pondit-datatable>
        <x-slot name="thead">
            <tr>
                <th>Ser No</th>
                <th>New Code</th>
                <th>Financial Year</th>
                <th>Sequence Number</th>
                <th>Actions</th>
            </tr>
        </x-slot>
        @foreach ($data as $key=>$datam)
        <tr>
            <td>{{ ++$key }}</td>
            <td>{{ $datam->new_code }}</td>
            <td>{{ $datam->introduced_fin_year }}</td>
            <td>{{ $datam->sequence_number }}</td>
            <td class="d-flex">
                <x-pondit-act-link url="{{route('budgetcode.show', $datam->new_code)}}" icon="eye" bg="success" />
                <x-pondit-act-link url="{{route('budgetcode.edit', $datam->new_code)}}" icon="pen" bg="primary" />
                <x-pondit-act-link-d url="{{route('budgetcode.delete', $datam->new_code)}}" icon="trash" />
            </td>
        </tr>
        @endforeach
    </x-pondit-datatable>
    {{-- card footer --}}
    <x-slot name="cardFooter">
        <div></div>
        <div>
        <x-pondit-act-c url="{{route('budgetcode.create')}}"/>
        <x-pondit-act-u url="{{route('budgetcode.upload')}}" />
    </div>
    <div>
        <button type="button" class="btn btn-link " data-toggle="modal" data-target="#modal_default">
            <i class="fas fa-file-powerpoint fa-2x" style="color: #3a68bb"></i>
        </button>
    </div>
    </x-slot>
</x-pondit-card>

<div id="modal_default" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                {{-- <h5 class="modal-title">Basic modal</h5> --}}
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link text-danger" data-dismiss="modal">Close</button>
                {{-- <button type="button" class="btn btn-link">Save changes</button> --}}
            </div>
        </div>
    </div>
</div>
@endsection