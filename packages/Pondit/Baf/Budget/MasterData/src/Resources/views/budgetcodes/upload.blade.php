@extends('pondit-limitless::layouts.master')
@section('content')
{{ csrf_token() }}
<x-pondit-card title="Upload">
    <x-pondit-form action="{{route('allotmentLetter.store')}}" >
        <x-pondit-fin-year name="introduced_fin_year"/>
        <div class='form-group row'>
            <label class="col-sm-2 col-form-label">Allotment Letter</label>
            <div class='col-sm-10'>
                <input type="file" name="alloted_letter" class="form-control" />
            </div>
        </div>
        <div class='form-group row'>
            <label class="col-sm-2 col-form-label">Allotment Budget Code</label>
            <div class='col-sm-10'>
                <input type="file" name="alloted_file" class="form-control" required/>
            </div>
        </div>
        <div class="form-group text-center">
            <x-pondit-btn title="{{ __('upload') }}" icon="file-upload" float="center"/>
        </div>
    </x-pondit-form>
    <x-slot name="cardFooter">
        <div></div>
        <div>
            <x-pondit-act-i url="{{route('budgetcode.index')}}"/>
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection