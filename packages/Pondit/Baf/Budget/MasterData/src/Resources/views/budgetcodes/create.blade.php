@extends('pondit-limitless::layouts.master')
@section('content')
{{-- {{ csrf_token() }} --}}
<x-pondit-card title="Budget Codes">
<x-pondit-form action="{{route('budgetcode.store')}}">

{{-- <x-pondit-fin-year name="introduced_fin_year" /> --}}

        <div class='form-group row'>
            <label name="sequence_number" class="col-sm-2 col-form-label">Sequence Number</label>
            <div class='col-sm-10'>
                <input type="number" name="sequence_number" class="form-control" />
            </div>
        </div>
        <div class='form-group row'>
            <label name="new_code" class="col-sm-2 col-form-label">New Code</label>
            <div class='col-sm-10'>
                <input type="text" name="new_code" required class="form-control" />
            </div>
        </div>
        <div class='form-group row'>
            <label name="old_code" class="col-sm-2 col-form-label">Old Code</label>
            <div class='col-sm-10'>
                <input type="text" name="old_code" class="form-control" />
            </div>
        </div>
        <div class='form-group row'>
            <label for="activity_type" class="col-sm-2 col-form-label">Activity Type</label>
            <div class='col-sm-10'>
                <select id="activity_type" name="activity_type" class="form-control select">
                    <option value="general">General</option>
                    <option value="special">Special</option>
                </select>
            </div>
        </div>
        <div class='form-group row'>
            <label for="budget_head" class="col-sm-2 col-form-label">Budget Head</label>
            <div class='col-sm-10'>
                <input type="text" name="budget_head" class="form-control" />
            </div>
        </div>
        <div class='form-group row'>
            <label class="col-sm-2 col-form-label">Description</label>
            <div class='col-sm-10'>
                <input type="text" name="description" class="form-control">
            </div>
        </div>
        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>
    <x-slot name="cardFooter">
        <div></div>
        <div>
        <x-pondit-act-i url="{{route('budgetcode.index')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection