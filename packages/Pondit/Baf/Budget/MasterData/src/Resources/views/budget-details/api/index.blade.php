@extends('pondit-limitless::layouts.master')
@push('css')
<link rel="stylesheet" href="{{ asset('') }}vendor/pondit/assets/jqgrid/css/ui.jqgrid-bootstrap4.css">
<link rel="stylesheet" href="{{ asset('') }}vendor/pondit/assets/jqgrid/css/fontawesome/css/fontawesome-all.min.css">
<link rel="stylesheet" href="{{ asset('') }}vendor/pondit/assets/jqgrid/css/main.css">
@endpush
@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<x-pondit-card title="Budgets">
    {{-- <table id="jqGrid"></table>
    <div id="jqGridPager"></div> --}}

    <div class="row" style="position: relative;">
        <div class="col-12 mt-1">
            <div class="p-2">
                <div class="row">
                    <table id="jqGrid"  style="position: relative;"></table>
                    <div id="jqGridPager"></div>
                </div>
            </div>
        </div>
    </div>

    
    <span class="oi oi-person"></span>
    <div id="userForm" class="modal fade" data-backdrop="static" data-keyboard="false" aria-modal="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
            <div class="modal-content">

                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white">Budget Details</h5>
                    <span aria-hidden="true" data-dismiss="modal"><i class="fa fa-times text-light"
                            style="cursor: pointer;"></i></span>
                </div>

                <div class="modal-body">
                    <form id="form">
                        <meta name="csrf-token" content="{{ csrf_token() }}" />
                        <input type="hidden" name="id" id="id" value="4">                        

                        <div class="form-group">
                            <label for="budget_id">Budget</label>
                            <select name="budget_id" id="budget_id" class="form-control">
                                @foreach ($budgets as $id => $budget)
                                     <option value="{{ $id }}">{{ $budget }}</option>       
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="budgetcode_id">Budget Code</label>
                            <select name="budgetcode_id" id="budgetcode_id" class="form-control">
                                @foreach ($budget_codes as $id => $budget_code)
                                     <option value="{{ $id }}">{{ $budget_code }}</option>       
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="amount">Amount</label>
                            <input name="amount" type="number" placeholder=" Amount"
                                id="amount" class="form-control" required="">
                        </div>

                        <div class="form-group">
                            <input name="is_initial" type="checkbox" placeholder=" is_initial"
                            id="is_initial" >&nbsp;
                            <label for="is_initial">Is Initial</label>
                        </div>

                        <div class="form-group">
                            <button type="button" id="form-reset" class="btn btn-info btn-sm float-left"><i
                                    class="fa fa-sync"></i> CLEAR</button>
                            <button type="button" id="form-submit" class="btn btn-success btn-sm float-right"><i
                                    class="fa fa-save"></i> SAVE</button>
                        </div>

                    </form>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>

    <div id="userDetails" class="modal fade show" data-backdrop="static" data-keyboard="false" aria-modal="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
            <div class="modal-content">

                <div class="modal-header bg-success">
                    <h5 class="modal-title text-white">Budget Codes</h5>
                    <span aria-hidden="true" data-dismiss="modal"><i class="fa fa-times text-light"
                            style="cursor: pointer;"></i></span>
                </div>

                <div class="modal-body">
                    <table class="table table-striped table-bordered" id="user-details-table"></table>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
</x-pondit-card>
@endsection

@push('js')

<script src="{{ asset('') }}vendor/pondit/assets/jqgrid/js/trirand/i18n/grid.locale-en.js"></script>
<script src="{{ asset('') }}vendor/pondit/assets/jqgrid/js/context-menu.js"></script>
<script src="{{ asset('') }}vendor/pondit/assets/jqgrid/js/trirand/jquery.jqGrid.min.js"></script>
<script src="{{ asset('') }}vendor/pondit/assets/jqgrid/js/budget-details.js"></script>

@endpush
