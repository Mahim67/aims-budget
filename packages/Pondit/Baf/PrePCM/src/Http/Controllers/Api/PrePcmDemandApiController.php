<?php

namespace Pondit\Baf\PrePCM\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Pondit\Baf\PrePCM\Models\PrePcmDemand;

class PrePcmDemandApiController extends Controller
{
    
    public function pcmDemand()
    {
        return view('pre-pcm::pcm-demands.pcm-demands');
    }
    public function index()
    {
        $data   =   PrePcmDemand::orderBy('id', 'desc')->get();
        return response()->json([
            'status' => 'ok',
            'data'   => $data
        ]);
    }
    public function store(Request $request)
    {
        try {
         
        $data   =   new PrePcmDemand();

        $data->fin_year                         =  $request->fin_year;
        $data->office_id                        =  $request->office_id;
        $data->office_name_hq                   =  $request->office_name_hq;
        $data->office_name_base                 =  $request->office_name_base;
        $data->office_name_unit                 =  $request->office_name_unit;
        $data->total_demanded_amount_bdt        =  $request->total_demanded_amount_bdt;
        $data->total_recom_amount_bdt           =  $request->total_recom_amount_bdt;
        $data->present_holding_chk_bdnumber     =  $request->present_holding_chk_bdnumber;
        $data->present_holding_chk_sign         =  $request->present_holding_chk_sign;
        $data->present_holding_chk_date         =  $request->present_holding_chk_date;
        $data->present_holding_chk_apmt         =  $request->present_holding_chk_apmt;
        $data->counter_signed_by_bdnumber       =  $request->counter_signed_by_bdnumber;
        $data->counter_signed_by_signature      =  $request->counter_signed_by_signature;
        $data->counter_signed_by_date           =  $request->counter_signed_by_date;
        $data->counter_signed_by_apmt           =  $request->counter_signed_by_apmt;
        $data->approved_by_bdnumber             =  $request->approved_by_bdnumber;
        $data->approved_by_signature            =  $request->approved_by_signature;
        $data->approved_by_date                 =  $request->approved_by_date;
        $data->approved_by_apmt                 =  $request->approved_by_apmt;
        $data->security_grading                 =  $request->security_grading;
        $data->save();
        
        return response()->json([
            'status' => 'ok',
            'message' => 'Created Successfully',
            'data'   => $data
        ]);                 
        }
        catch (QueryException $th)
        {
            return response()->json([
                'status'  => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }
    public function show($id)
    {
        $data   =   PrePcmDemand::find($id);
        return view('pre-pcm::pcm-demands.show', compact('data'));
    }
    public function edit($id)
    {
        $data   =   PrePcmDemand::find($id);
        return response()->json([
            'status' => 'ok',
            'data'   => $data
        ]);
    }
    public function update(Request $request, $id)
    {
        // dd($request->all());
        try {
            $data   =   PrePcmDemand::find($id);

            $data->fin_year                      =  $request->fin_year;
            $data->office_id                     =  $request->office_id;
            $data->office_name_hq                =  $request->office_name_hq;
            $data->office_name_base              =  $request->office_name_base;
            $data->office_name_unit              =  $request->office_name_unit;
            $data->total_demanded_amount_bdt     =  $request->total_demanded_amount_bdt;
            $data->total_recom_amount_bdt        =  $request->total_recom_amount_bdt;
            $data->present_holding_chk_bdnumber  =  $request->present_holding_chk_bdnumber;
            $data->present_holding_chk_sign      =  $request->present_holding_chk_sign;
            $data->present_holding_chk_date      =  $request->present_holding_chk_date;
            $data->present_holding_chk_apmt      =  $request->present_holding_chk_apmt;
            $data->counter_signed_by_bdnumber    =  $request->counter_signed_by_bdnumber;
            $data->counter_signed_by_signature   =  $request->counter_signed_by_signature;
            $data->counter_signed_by_date        =  $request->counter_signed_by_date;
            $data->counter_signed_by_apmt        =  $request->counter_signed_by_apmt;
            $data->approved_by_bdnumber          =  $request->approved_by_bdnumber;
            $data->approved_by_signature         =  $request->approved_by_signature;
            $data->approved_by_date              =  $request->approved_by_date;
            $data->approved_by_apmt              =  $request->approved_by_apmt;
            $data->security_grading              =  $request->security_grading;
            $data->save();

            return response()->json([
                'status' => 'ok',
                'message' => 'Updated Successfully',
                'data'   => $data
            ]);                 
            }
            catch (QueryException $th)
            {
                return response()->json([
                    'status'  => 'error',
                    'message' => $e->getMessage()
                ]);
            }
    }
    public function destroy($id)
    {
        try {
            $data   =   PrePcmDemand::find($id);
            $data->delete();

            return response()->json([
                'status'  => 'ok',
                'message' => 'Deleted successfully!',
            ]);
        } catch (QueryException $e) {
            return response()->json([
                'status'  => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }
}
