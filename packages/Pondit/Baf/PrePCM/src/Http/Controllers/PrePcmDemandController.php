<?php

namespace Pondit\Baf\PrePCM\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Pondit\Baf\PrePCM\Models\PrePcmDemand;

class PrePcmDemandController extends Controller
{
    
    public function index()
    {
        $data   =   PrePcmDemand::all();
        // dd($data);
        return view('pre-pcm::pcm-demands.index', compact('data'));
    }
    public function create()
    {
        return view('pre-pcm::pcm-demands.create');
    }
    public function store(Request $request)
    {
        // dd($request->all());
        try {
         
        $data   =   new PrePcmDemand();

        $data->fin_year                         =  $request->fin_year;
        $data->office_id                        =  $request->office_id;
        $data->office_name_hq                   =  $request->office_name_hq;
        $data->office_name_base                 =  $request->office_name_base;
        $data->office_name_unit                 =  $request->office_name_unit;
        $data->total_demanded_amount_bdt        =  $request->total_demanded_amount_bdt;
        $data->total_recom_amount_bdt           =  $request->total_recom_amount_bdt;
        $data->present_holding_chk_bdnumber     =  $request->present_holding_chk_bdnumber;
        $data->present_holding_chk_sign         =  $request->present_holding_chk_sign;
        $data->present_holding_chk_date         =  $request->present_holding_chk_date;
        $data->present_holding_chk_apmt         =  $request->present_holding_chk_apmt;
        $data->counter_signed_by_bdnumber       =  $request->counter_signed_by_bdnumber;
        $data->counter_signed_by_signature      =  $request->counter_signed_by_signature;
        $data->counter_signed_by_date           =  $request->counter_signed_by_date;
        $data->counter_signed_by_apmt           =  $request->counter_signed_by_apmt;
        $data->approved_by_bdnumber             =  $request->approved_by_bdnumber;
        $data->approved_by_signature            =  $request->approved_by_signature;
        $data->approved_by_date                 =  $request->approved_by_date;
        $data->approved_by_apmt                 =  $request->approved_by_apmt;
        $data->security_grading                 =  $request->security_grading;
        $data->save();
        return redirect()->route('prepcm-demands.index')
                         ->withMessage('Entity Have Been Successfully Saved!');
                           
        }
        catch (QueryException $th)
        {
            return redirect()->route('prepcm-demands.create')
                             ->withErrors($th);
        }
    }
    public function show($id)
    {
        $data   =   PrePcmDemand::find($id);
        return view('pre-pcm::pcm-demands.show', compact('data'));
    }
    public function edit($id)
    {
        $data   =   PrePcmDemand::find($id);
        return view('pre-pcm::pcm-demands.edit', compact('data'));
    }
    public function update(Request $request, $id)
    {
        // dd($request->all());
        try {
            $data   =   PrePcmDemand::find($id);

            $data->fin_year                      =  $request->fin_year;
            $data->office_id                     =  $request->office_id;
            $data->office_name_hq                =  $request->office_name_hq;
            $data->office_name_base              =  $request->office_name_base;
            $data->office_name_unit              =  $request->office_name_unit;
            $data->total_demanded_amount_bdt     =  $request->total_demanded_amount_bdt;
            $data->total_recom_amount_bdt        =  $request->total_recom_amount_bdt;
            $data->present_holding_chk_bdnumber  =  $request->present_holding_chk_bdnumber;
            $data->present_holding_chk_sign      =  $request->present_holding_chk_sign;
            $data->present_holding_chk_date      =  $request->present_holding_chk_date;
            $data->present_holding_chk_apmt      =  $request->present_holding_chk_apmt;
            $data->counter_signed_by_bdnumber    =  $request->counter_signed_by_bdnumber;
            $data->counter_signed_by_signature   =  $request->counter_signed_by_signature;
            $data->counter_signed_by_date        =  $request->counter_signed_by_date;
            $data->counter_signed_by_apmt        =  $request->counter_signed_by_apmt;
            $data->approved_by_bdnumber          =  $request->approved_by_bdnumber;
            $data->approved_by_signature         =  $request->approved_by_signature;
            $data->approved_by_date              =  $request->approved_by_date;
            $data->approved_by_apmt              =  $request->approved_by_apmt;
            $data->security_grading              =  $request->security_grading;
            $data->save();
            return redirect()->route('prepcm-demands.index')
                            ->withMessage('Entity Have Been Successfully Updated!');
                           
        }
        catch (QueryException $th)
        {
            return redirect()->back()
                             ->withErrors($th);
        }
    }
    public function destroy($id)
    {
        try {
            $data   =   PrePcmDemand::find($id);
            $data->delete();
            return redirect()->route('prepcm-demands.index')
                            ->withMessage('Entity has been deleted successfully!');
        } 
        catch (QueryException $th) 
        {
            return redirect()->back()
                             ->withErrors($th);
        }
    }
}
