<?php

namespace Pondit\Baf\PrePCM\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Pondit\Baf\Range\Models\Range;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Pondit\Baf\PrePCM\Models\PrePcmDemandDetail;

class PrePcmDemandDetailController extends Controller
{
    public function index()
    {
        $data   =   PrePcmDemandDetail::all();
        return view('pre-pcm::pcm-demand-details.index', compact('data'));
    }
    public function create()
    {
        return view('pre-pcm::pcm-demand-details.create');
    }
    public function store(Request $request)
    {
        try {    
            $range = Range::where('id', $request->range)->first();
            
            $data   =  [
                'office_id'                    => '101',
                'base_id'                      => '201',
                'fin_year'                     => $request->fin_year,
                'fin_year'                     => $request->fin_year,
                'range_id'                     => $range->id??null,
                'range_name'                   => $range->title??null,
                'recom_by_opi_dte_name'        => $request->recom_by_dte_name??null,
                'recom_by_opi_dte_id'          => null,
                'item_id'                      => $request->item_name??null,
                'item_name'                    => $request->item_name??null,
                'estimated_unit_cost'          => $request->price_unit??null,
                'currency'                     => $request->currency??null,
                'qty'                          => $request->qty??null,
                'unit'                         => $request->qty_unit??null,
                'estimated_total_cost'         => $request->total_cost??null,
                'present_holding_base_unit'    => $request->present_holding_base_unit??null,
                'present_holding_sqn_flt_sec'  => $request->present_holding_sqn_flt_sec??null,
                'justification'                => $request->justification??null,
            ];

            $pcmDemand   =  PrePcmDemandDetail::create($data);
            if(!$pcmDemand)
                throw new Exception("Pre PCM Demand could not be saved!", 403);
                
            return redirect()->route('prepcm-demand-details.index')
                            ->withMessage('Entity Have Been Successfully Saved!');
                            
        }
        catch (QueryException $th)
        {
            return redirect()->route('prepcm-demand-details.index')
                             ->withErrors($th->getMessage());
        }
    }
    public function show($id)
    {
        $data  =  PrePcmDemandDetail::find($id);
        return view('pre-pcm::pcm-demand-details.show', compact('data'));
    }
    public function edit($id)
    {
        $data  =  PrePcmDemandDetail::find($id);
        return view('pre-pcm::pcm-demand-details.edit', compact('data'));
    }
    public function update(Request $request, $id)
    {
        // dd($request->all());
        try {
         
            $data   =   PrePcmDemandDetail::find($id);
    
            $data->prepcm_demand_id             =  $request->prepcm_demand_id;
            $data->range_id                     =  $request->range_id;
            $data->range_name                   =  $request->range_name;
            $data->ser_no_1                     =  $request->ser_no_1;
            $data->name_of_equipment_2          =  $request->name_of_equipment_2;
            $data->item_id                      =  $request->item_id;
            $data->item_code                    =  $request->item_code;
            $data->name_of_user_3               =  $request->name_of_user_3;
            $data->present_holding_bu_4         =  $request->present_holding_bu_4;
            $data->present_holding_sfs_5        =  $request->present_holding_sfs_5;
            $data->demanded_qty_6               =  $request->demanded_qty_6;
            $data->reccom_by_opi_dte_7          =  $request->reccom_by_opi_dte_7;
            $data->estimated_unit_price_bdt_8   =  $request->estimated_unit_price_bdt_8;
            $data->total_cost_bdt_9             =  $request->total_cost_bdt_9;
            $data->total_recom_cost_bdt         =  $request->total_recom_cost_bdt;
            $data->justification_10             =  $request->justification_10;
            $data->save();
            return redirect()->route('prepcm-demand-details.index')
                             ->withMessage('Entity Have Been Successfully Updated!');
                               
        }
        catch (QueryException $th)
        {
            return redirect()->back()
                                ->withErrors($th);
        }
    }
    public function destroy($id)
    {
        try {
            $data  =  PrePcmDemandDetail::find($id);
            $data->delete();
            return redirect()->route('prepcm-demand-details.index')
                             ->withMessage('Entity Have Been Successfully Deleted!');
        }
        catch (QueryException $th) {
            return redirect()->back()
                            ->withErrors($th);
        }
    }

    /*
    * Report Download mehod
    */
    public function downloadPDF($id)
    {

    }

    /*
    * Report method
    */

    public function rangeWiseDemand()
    {
        return view('pre-pcm::pcm-demand-reports.range-wise-demand-report');
    }


    /* 
    *BASE DEMAND
    */
    // Range wise demand 
    public function baseRangeWiseDemand(Request $request)
    {
        try {
            $fin_year = $request->fin_year;
            $range = $request->range;

            if (!empty($fin_year) && ($range == NULL)) {
                $data  =  PrePcmDemandDetail::where('fin_year', $fin_year)->get();
            } 
            elseif (!empty($range) && ($fin_year == NULL)) {
                $data  =  PrePcmDemandDetail::where('range_id', $range)->get();
            }
            elseif (!empty($fin_year) && !empty($range)) {
                $data  =  PrePcmDemandDetail::where('range_id', $range)
                                            ->where('fin_year', $fin_year)
                                            ->get();
            }
            else {
                $data  =  "No Data Found!";
            }
            if(!$data)
                throw new Exception("Please, Try again!", 403);
            $request->session()->flash('fin_year', $fin_year);
            $request->session()->flash('range', $range);
            $request->session()->flash('data', $data);

            return redirect()->back();
        }
        catch (QueryException $th) {
            return redirect()->back()
                        ->withErrors($th);
        }
    } 

}
