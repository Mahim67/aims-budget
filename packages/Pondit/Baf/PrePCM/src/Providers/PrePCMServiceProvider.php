<?php

namespace Pondit\Baf\PrePCM\Providers;

use Illuminate\Support\ServiceProvider;

class PrePCMServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->loadRoutesFrom(__DIR__ . '/../Routes/web.php');
        $this->loadRoutesFrom(__DIR__ . '/../Routes/api.php');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        $this->publishes([
            __DIR__ . '/../../publishable/assets' => public_path('vendor/pre-pcm/assets'),
        ], 'public');

        $this->loadViewsFrom(__DIR__ . '/../Resources/views', 'pre-pcm');
    }

    public function register(): void
    {
        // Register stuffs
    }
}
