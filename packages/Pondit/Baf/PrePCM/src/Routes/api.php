<?php

use Illuminate\Support\Facades\Route;
use Pondit\Baf\PrePCM\Http\Controllers\Api\PrePcmDemandApiController;
use Pondit\Baf\PrePCM\Http\Controllers\Api\PrePcmDemandDetailApiController;


Route::group(['prefix' => 'api', 'middleware' => ['web']], function () {
    Route::group(['prefix' => 'prepcm-demands'], function () {

        Route::get('/', [PrePcmDemandApiController::class, 'pcmDemand']);

        Route::get('/getData', [PrePcmDemandApiController::class, 'index']);
        Route::get('/show/{id}', [PrePcmDemandApiController::class, 'show']);     
        Route::post('/store', [PrePcmDemandApiController::class, 'store']);
        Route::put('/update/{id}', [PrePcmDemandApiController::class, 'update']);
        Route::post('/{id}', [PrePcmDemandApiController::class, 'destroy']);
    });
    Route::group(['prefix' => 'prepcm-demand-details'], function () {

        Route::get('/', [PrePcmDemandDetailApiController::class, 'pcmDemandDetail']);

        Route::get('/getData', [PrePcmDemandDetailApiController::class, 'index']);
        Route::get('/show/{id}', [PrePcmDemandDetailApiController::class, 'show']);     
        Route::post('/store', [PrePcmDemandDetailApiController::class, 'store']);
        Route::put('/update/{id}', [PrePcmDemandDetailApiController::class, 'update']);
        Route::post('/{id}', [PrePcmDemandDetailApiController::class, 'destroy']);

        Route::get('/base-range-wise-demand/{id}', [PrePcmDemandDetailApiController::class, 'baseRangeWiseDemand']);
    });
});