<?php

use Illuminate\Support\Facades\Route;
use Pondit\Baf\PrePCM\Http\Controllers\PrePcmDemandController;
use Pondit\Baf\PrePCM\Http\Controllers\PrePcmDemandDetailController;


Route::group(['middleware' => ['web']], function () {
    
    Route::group(['prefix' => 'prepcm-demands', 'as' => 'prepcm-demands.'], function () {
        Route::get('/', [PrePcmDemandController::class, 'index'])->name('index');
        Route::get('/create', [PrePcmDemandController::class, 'create'])->name('create');
        Route::get('/show/{id}', [PrePcmDemandController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [PrePcmDemandController::class, 'edit'])->name('edit');
        Route::post('/', [PrePcmDemandController::class, 'store'])->name('store');
        Route::put('/update/{id}', [PrePcmDemandController::class, 'update'])->name('update');
        Route::post('/{id}', [PrePcmDemandController::class, 'destroy'])->name('delete');
    });
    Route::group(['prefix' => 'prepcm-demand-details', 'as' => 'prepcm-demand-details.'], function () {
        Route::get('/import', [PrePcmDemandDetailController::class, 'import'])->name('import');
        Route::get('/preview', [PrePcmDemandDetailController::class, 'preview'])->name('preview');
        
        Route::get('/', [PrePcmDemandDetailController::class, 'index'])->name('index');
        Route::get('/create', [PrePcmDemandDetailController::class, 'create'])->name('create');
        Route::get('/show/{id}', [PrePcmDemandDetailController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [PrePcmDemandDetailController::class, 'edit'])->name('edit');
        Route::post('/', [PrePcmDemandDetailController::class, 'store'])->name('store');
        Route::put('/update/{id}', [PrePcmDemandDetailController::class, 'update'])->name('update');
        Route::post('/{id}', [PrePcmDemandDetailController::class, 'destroy'])->name('delete');
        
        Route::get('/download/{id}', [PrePcmDemandDetailController::class, 'downloadPDF'])->name('download');

        Route::get('/range-wise-demand', [PrePcmDemandDetailController::class, 'rangeWiseDemand'])->name('range-wise-demand');

        Route::get('/base-range-wise-demand', [PrePcmDemandDetailController::class, 'baseRangeWiseDemand'])->name('base-range-wise');
        

    });
});