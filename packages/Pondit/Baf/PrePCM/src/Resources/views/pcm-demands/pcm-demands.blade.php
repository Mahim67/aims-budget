@extends('pondit-limitless::layouts.master')
@push('css')
<link rel="stylesheet" href="{{ asset('') }}vendor/pondit/assets/jqgrid/css/ui.jqgrid-bootstrap4.css">
<link rel="stylesheet" href="{{ asset('') }}vendor/pondit/assets/jqgrid/css/fontawesome/css/fontawesome-all.min.css">
<link rel="stylesheet" href="{{ asset('') }}vendor/pondit/assets/jqgrid/css/main.css">
@endpush
@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<!-- Basic example -->
<x-pondit-card title="Pre PCM Demands">
    <table id="jqGrid"></table>
    <div id="jqGridPager"></div>
    <span class="oi oi-person"></span>
    <div id="userForm" class="modal fade" data-backdrop="static" data-keyboard="false" aria-modal="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
            <div class="modal-content">

                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white">Pre PCM Demands</h5>
                    <span aria-hidden="true" data-dismiss="modal"><i class="fa fa-times text-light"
                            style="cursor: pointer;"></i></span>
                </div>

                <div class="modal-body">
                    <form id="form">
                        <meta name="csrf-token" content="{{ csrf_token() }}" />

                        <input type="hidden" name="id" id="id">

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label class="col-form-label">Financial Years</label>
                                    <select name="fin_year" id="" class="form-control select">
                                        <option value="">-- Select Fin Year --</option>
                                        <option value="2019-2020">2019-2020</option>
                                        <option value="2020-2021">2020-2021</option>
                                        <option value="2021-2022">2021-2022</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label class="col-form-label">Office</label>
                                    <select name="office_id" id="" class="form-control select">
                                        <option value="">A</option>
                                        <option value="">B</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label class="col-form-label"> Service Number of Checker</label>
                                    <input type="text" name="present_holding_chk_bdnumber" class="form-control">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label class="col-form-label">Service Number of Counter Signatory</label>
                                    <input type="text" name="counter_signed_by_bdnumber" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label class="col-form-label">Service Number of Approver</label>
                                    <input type="text" name="approved_by_bdnumber" class="form-control">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label class="col-form-label">Security Grading</label>
                                    <input type="text" name="security_grading" class="form-control">
                                </div>
                            </div>
                        </div>
                        <x-pondit-excel-importer />

                        <div class="form-group">
                            <button type="button" id="form-reset" class="btn btn-info btn-sm float-left">
                                <i class="fa fa-sync"></i> CLEAR
                            </button>
                            <button type="button" id="form-submit" class="btn btn-success btn-sm float-right">
                                <i class="fa fa-save"></i> SAVE
                            </button>
                        </div>

                    </form>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>

    <div id="userDetails" class="modal fade show" data-backdrop="static" data-keyboard="false" aria-modal="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
            <div class="modal-content">

                <div class="modal-header bg-success">
                    <h5 class="modal-title text-white">Budget Codes</h5>
                    <span aria-hidden="true" data-dismiss="modal"><i class="fa fa-times text-light"
                            style="cursor: pointer;"></i></span>
                </div>

                <div class="modal-body">
                    <table class="table table-striped table-bordered" id="user-details-table"></table>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
</x-pondit-card>

<!-- /basic example -->
@endsection

@push('js')

<script src="{{ asset('') }}vendor/pondit/assets/jqgrid/js/trirand/i18n/grid.locale-en.js"></script>
<script src="{{ asset('') }}vendor/pondit/assets/jqgrid/js/context-menu.js"></script>
<script src="{{ asset('') }}vendor/pondit/assets/jqgrid/js/trirand/jquery.jqGrid.min.js"></script>

<script src="{{ asset('') }}vendor/pondit/assets/jqgrid/js/pre-pcm-demands.js"></script>
@endpush