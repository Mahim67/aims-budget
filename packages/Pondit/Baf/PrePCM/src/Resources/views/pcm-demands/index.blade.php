@extends('pondit-limitless::layouts.master')
@section('content')
<x-pondit-card title="Pre-PCM Demands">
    <x-pondit-datatable>
        <x-slot name="thead">
            <tr>
                <th>Ser No</th>
                <th>FY</th>
                <th>Office</th>
                <th>Estimated Total Demand</th>
                <th>Action</th>
            </tr>
        </x-slot>
        @forelse ($data as $key=>$datam)
        <tr>
            <td>{{ ++$key }}</td>
            <td>{{ $datam->fin_year }}</td>
            <td>{{ $datam->office_id }}</td>
            <td>{{ $datam->total_demanded_amount_bdt }}</td>
            <td class="d-flex">
                <x-pondit-act-u  />
                <x-pondit-act-link url="{{route('prepcm-demand-details.index', $datam->id)}}" icon="list" bg="indigo" />
                <x-pondit-act-link url="{{route('prepcm-demands.show', $datam->id)}}" icon="eye" bg="success" />
                <x-pondit-act-link url="{{route('prepcm-demands.edit', $datam->id)}}" icon="pen" bg="primary" />
                <x-pondit-act-link-d url="{{route('prepcm-demands.delete', $datam->id)}}" icon="trash" />
            </td>
        </tr>
        @empty
        <tr class="text-danger text-center">
            <td colspan="5">No Record Found. <a href="{{route('prepcm-demands.create')}}">Add Item</a> Or <a href="">Upload</a> From Excel</td>
        </tr>
        @endforelse
    </x-pondit-datatable>

    <x-slot name="cardFooter">
        <div></div>
        <div>
            <x-pondit-act-c url="{{route('prepcm-demands.create')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection