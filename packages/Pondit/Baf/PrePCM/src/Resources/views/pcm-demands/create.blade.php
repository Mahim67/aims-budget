@extends('pondit-limitless::layouts.master')
@section('content')

@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<x-pondit-card title="Pre-PCM Demands">
    <x-pondit-form action="{{route('prepcm-demands.store')}}">
        <div class="row">
            <div class="col-6">
                <div class="row form-group">
                    <label class="col-md-4 col-form-label">Financial Years</label>
                    <div class="col-md-8">
                        <select name="fin_year" id="" class="form-control">
                            <option value="">-- Select Fin Year --</option>
                            <option value="2019-2020">2019-2020</option>
                            <option value="2020-2021">2020-2021</option>
                            <option value="2021-2022">2021-2022</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="row form-group">
                    <label class="col-md-3 col-form-label">Office</label>
                    <div class="col-md-9">
                        {{-- <input type="text" name="office_id" class="form-control"> --}}
                        <select name="office_id" id="" class="form-control">
                            <option value="">A</option>
                            <option value="">B</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="row form-group">
                    <label class="col-md-6 col-form-label"> Service Number of Checker</label>
                    <div class="col-md-6">
                        <input type="text" name="present_holding_chk_bdnumber" class="form-control">
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="row form-group">
                    <label class="col-md-6 col-form-label">Service Number of Counter Signatory</label>
                    <div class="col-md-6">
                        <input type="text" name="counter_signed_by_bdnumber" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="row form-group">
                    <label class="col-md-6 col-form-label">Service Number of Approver</label>
                    <div class="col-md-6">
                        <input type="text" name="approved_by_bdnumber" class="form-control">
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="row form-group">
                    <label class="col-md-6 col-form-label">Security Grading</label>
                    <div class="col-md-6">
                        <input type="text" name="security_grading" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        <x-pondit-excel-importer />

        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>

    <x-slot name="cardFooter">
        <div></div>
        <div>
            <x-pondit-act-i url="{{route('prepcm-demands.index')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection