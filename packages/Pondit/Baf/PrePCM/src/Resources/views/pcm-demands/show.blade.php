@extends('pondit-limitless::layouts.master')

@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<!-- Basic example -->
<x-pondit-card title="Pre PCM Demands">

    <div class="single-show w-75 m-auto">
        <div class="card-body text-center card-img-top">
            <div class="card-img-actions d-inline-block">
                <img class="img-fluid "
                    src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/images/placeholders/placeholder.jpg"
                    width="200" height="150" alt="">
            </div>
        </div>
        <div class="card-body p-0">
            <ul class="nav nav-sidebar mb-2">
                <li class="nav-item-header mt-0">Details</li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Fin Year :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->fin_year}}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Office Name HQ :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->office_name_hq}}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Office Name Base :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->office_name_base}}</div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <x-slot name="cardFooter">
        <div></div>
        <div class="d-flex">
            <x-pondit-act-i url="{{route('prepcm-demands.index')}}" tooltip="{{__('list')}}"/>
            <x-pondit-act-c url="{{route('prepcm-demands.create')}}" tooltip="{{__('create')}}"/>
            <x-pondit-act-e url="{{route('prepcm-demands.edit', $data->id)}}" tooltip="{{__('edit')}}"/>
            <x-pondit-act-d url="{{route('prepcm-demands.delete', $data->id)}}" tooltip="{{__('remove')}}"/>
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>

<!-- /basic example -->

@endsection
