@extends('pondit-limitless::layouts.master')
@section('content')

@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<x-pondit-card title="Pre-PCM Demands">
    <x-pondit-form action="{{route('prepcm-demands.update',$data->id)}}">
        @method('PUT')
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Financial Years</label>
            <div class="col-md-10">
                <select name="fin_year" id="" class="form-control">
                    <option value="">-- Select Fin Year --</option>
                    <option value="2019-2020">2019-2020</option>
                    <option value="2020-2021">2020-2021</option>
                    <option value="2021-2022">2021-2022</option>
                </select>
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Office ID</label>
            <div class="col-md-10">
                <input type="text" name="office_id" class="form-control" value="{{$data->office_id}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Office Name HQ</label>
            <div class="col-md-10">
                <input type="text" name="office_name_hq" class="form-control"  value="{{$data->office_name_hq}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Office Name Base</label>
            <div class="col-md-10">
                <input type="text" name="office_name_base" class="form-control" value="{{$data->office_name_base}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Office Name Unit</label>
            <div class="col-md-10">
                <input type="text" name="office_name_unit" class="form-control" value="{{$data->office_name_unit}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Total Demanded Amount bdt</label>
            <div class="col-md-10">
                <input type="number" name="total_demanded_amount_bdt" class="form-control" value="{{$data->total_demanded_amount_bdt}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Total Recom Amount bdt</label>
            <div class="col-md-10">
                <input type="number" name="total_recom_amount_bdt" class="form-control" value="{{$data->total_recom_amount_bdt}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Present Holding Chk bdnumber</label>
            <div class="col-md-10">
                <input type="text" name="present_holding_chk_bdnumber" class="form-control" value="{{$data->present_holding_chk_bdnumber}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Present Holding Chk Sign</label>
            <div class="col-md-10">
                <input type="text" name="present_holding_chk_sign" class="form-control" value="{{$data->present_holding_chk_sign}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Present Holding Chk Date</label>
            <div class="col-md-10">
                <input type="date" name="present_holding_chk_date" class="form-control" value="{{$data->present_holding_chk_date}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Present Holding Chk Appointment</label>
            <div class="col-md-10">
                <input type="text" name="present_holding_chk_apmt" class="form-control" value="{{$data->present_holding_chk_apmt}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Counter Signed By BDnumber</label>
            <div class="col-md-10">
                <input type="text" name="counter_signed_by_bdnumber" class="form-control" value="{{$data->counter_signed_by_bdnumber}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Counter Signed By Signature</label>
            <div class="col-md-10">
                <input type="text" name="counter_signed_by_signature" class="form-control" value="{{$data->counter_signed_by_signature}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Counter Signed By Date</label>
            <div class="col-md-10">
                <input type="date" name="counter_signed_by_date" class="form-control" value="{{$data->counter_signed_by_date}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Counter Signed By Appointment</label>
            <div class="col-md-10">
                <input type="text" name="counter_signed_by_apmt" class="form-control" value="{{$data->counter_signed_by_apmt}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Approved By BDnumber</label>
            <div class="col-md-10">
                <input type="text" name="approved_by_bdnumber" class="form-control" value="{{$data->approved_by_bdnumber}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Approved By Signature</label>
            <div class="col-md-10">
                <input type="text" name="approved_by_signature" class="form-control" value="{{$data->approved_by_signature}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Approved By Date</label>
            <div class="col-md-10">
                <input type="date" name="approved_by_date" class="form-control" value="{{$data->approved_by_date}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Approved By Appointment</label>
            <div class="col-md-10">
                <input type="text" name="approved_by_apmt" class="form-control" value="{{$data->approved_by_apmt}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Security Grading</label>
            <div class="col-md-10">
                <input type="text" name="security_grading" class="form-control" value="{{$data->security_grading}}">
            </div>
        </div>
        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>
    <x-slot name="cardFooter">
        <div></div>
        <div class="d-flex">
            <x-pondit-act-i url="{{route('prepcm-demands.index')}}" />
            <x-pondit-act-c url="{{route('prepcm-demands.create')}}" />
            <x-pondit-act-d url="{{route('prepcm-demands.delete', $data->id)}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection