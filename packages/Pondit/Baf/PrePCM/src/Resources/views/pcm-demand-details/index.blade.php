@extends('pondit-limitless::layouts.master')
@section('content')
<x-pondit-card title="Pre-PCM Demand Details">
    <x-pondit-datatable>
        <x-slot name="thead">
            <tr>
                <th scope="col" rowspan="2">Ser No</th>
                <th scope="col" rowspan="2">Name of Equipment</th>
                <th scope="col" colspan="4" class="text-center">Demand</th>
                <th scope="col" rowspan="2">Recom by OPI Dte</th>
                <th scope="col" rowspan="2">Estimated Unit Price (TK)</th>
                <th scope="col" rowspan="2">Total Cost (TK)</th>
                <th scope="col" rowspan="2">Justification</th>
                <th scope="col" rowspan="2">Action</th>
            </tr>
            <tr>
                <th scope="col">Name of User</th>
                <th scope="col" class="text-center">Present <br> holding in <br> Base/Unit</th>
                <th scope="col" class="text-center">Present <br> holding in <br> Sqn/Flt/Sec</th>
                <th scope="col">Qty Dmd</th>
            </tr>
            <tr class="text-center">
                <th scope="col">(1)</th>
                <th scope="col">(2)</th>
                <th scope="col">(3)</th>
                <th scope="col">(4)</th>
                <th scope="col">(5)</th>
                <th scope="col">(6)</th>
                <th scope="col">(7)</th>
                <th scope="col">(8)</th>
                <th scope="col">(9)</th>
                <th scope="col">(10)</th>
                <th scope="col"></th>
            </tr>
        </x-slot>
        @foreach ($data as $datum)
        <tr>
            <td>{{ $datum->ser_no }}</td>
            <td>{{ $datum->item_name }}</td>
            <td>{{ $datum->user_id }}</td>
            <td>{{ $datum->present_holding_base_unit }}</td>
            <td>{{ $datum->present_holding_sqn_flt_sec }}</td>
            <td>{{ $datum->qty }}</td>
            <td>{{ $datum->recom_by_opi_dte_name }}</td>
            <td class="text-right">{{ $datum->estimated_unit_cost }}</td>
            <td class="text-right">{{ $datum->estimated_total_cost }}</td>
            <td>{{ $datum->justification }}</td>
            <td class="d-flex">

                <x-pondit-act-link url="{{route('prepcm-demand-details.show', $datum->id)}}" icon="eye" bg="success" />
                <x-pondit-act-link url="{{route('prepcm-demand-details.edit', $datum->id)}}" icon="pen" bg="primary" />
                <x-pondit-act-link-d url="{{route('prepcm-demand-details.delete', $datum->id)}}" icon="trash" />
            </td>
        </tr>
        @endforeach
    </x-pondit-datatable>
    {{-- card footer --}}

    <x-slot name="cardFooter">
        <div></div>
        <div>
            <a href="" id="" class="btn btn-outline bg-brown btn-icon text-brown btn-sm border-brown border-2 rounded-round legitRipple mr-1"   data-popup="tooltip" title="Create" data-original-title="Create" data-toggle="modal"  data-target="#exampleModal">
                <i class="fas fa-plus"></i>
            </a>
            <x-pondit-act-u />
        </div>
        <div>
            <x-pondit-act-word />
            <x-pondit-act-excel />
            <x-pondit-act-pdf />
            <x-pondit-act-preview url="{{route('prepcm-demand-details.preview')}}" />
        </div>
    </x-slot>
</x-pondit-card>

<!-- Button trigger modal -->
{{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
    Launch demo modal
</button> --}}

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pre PCM Demand</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="" action="{{route('prepcm-demand-details.store')}}" method="POST">
                @csrf
                <div class="modal-body">
                    <meta name="cs  rf-token" content="{{ csrf_token() }}" />

                    <input type="hidden" name="id" id="id">

                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label class="col-form-label">Financial Years</label>
                                <select name="fin_year" id="" class="form-control select">
                                    <option value="">-- Select Fin Year --</option>
                                    <option value="2019-2020">2019-2020</option>
                                    <option value="2020-2021">2020-2021</option>
                                    <option value="2021-2022">2021-2022</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <x-pondit-baf-ranges name="range"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <x-pondit-baf-itemlist label="Name of Equipment" name="item_name"/>
                        </div>
                        <div class="col-6">
                            <label for="">Recom by Dte Name</label>
                            <input type="text" class="form-control" name="recom_by_dte_name">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <label>Present holding in Base/Unit</label>
                            <input type="text" name="present_holding_base_unit" class="form-control">
                        </div>
                        <div class="col-6">
                            <label>Present holding in Sqn/Flt/Sec</label>
                            <input type="text" name="present_holding_sqn_flt_sec" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <x-pondit-total-cost name="total_cost">
                            <x-slot name="estimatedPrice">
                                <x-pondit-unit-price label="Estimated Unit Price" name="price_unit" currencyName="currency"/>
                            </x-slot>
                            <x-slot name="qty">
                                <x-pondit-qty name="qty" qtyUnitName="qty_unit"/>
                            </x-slot>
                        </x-pondit-total-cost>
                    </div>
                    <div class="row">
                        <label class="form-label">Justification</label>
                            <textarea name="justification" class="form-control" rows="2"></textarea>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm bg-danger float- ml-1" data-dismiss="modal">
                        <i class="fas fa-times"></i> close
                    </button>
                    <button type="submit" class="btn btn-sm bg-success float- ml-1">
                        <i class="fas fa-check"></i> save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    $(document).ready(function(){
        $('#pcm_demand_details_form').on('submit', function(event){
            event.preventDefault();
            $.ajax({
                url:"{{ url('api/prepcm-demand-details/store') }}",
                method:"POST",
                data:new FormData(this),
                dataType:'JSON',
                contentType: false,
                cache: false,
                processData: false,
                success:function(data)
                {
                    console.log(data)

                }
            })
        });
    });

</script>
@endpush