@extends('pondit-limitless::layouts.master')
@section('content')

@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<x-pondit-card title="Pre-PCM Demand Details">
    <x-pondit-form action="{{route('prepcm-demand-details.update',$data->id)}}">
        @method('PUT')
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Pre PCM Demand ID</label>
            <div class="col-md-10">
                <input type="text" name="prepcm_demand_id" class="form-control" value="{{$data->prepcm_demand_id}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Range ID</label>
            <div class="col-md-10">
                <input type="text" name="range_id" class="form-control" value="{{$data->range_id}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Range Name</label>
            <div class="col-md-10">
                <input type="text" name="range_name" class="form-control" value="{{$data->range_name}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Ser No 1</label>
            <div class="col-md-10">
                <input type="text" name="ser_no_1" class="form-control" value="{{$data->ser_no_1}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Name of Equipment 2</label>
            <div class="col-md-10">
                <input type="text" name="name_of_equipment_2" class="form-control" value="{{$data->name_of_equipment_2}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Item ID</label>
            <div class="col-md-10">
                <input type="text" name="item_id" class="form-control" value="{{$data->item_id}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Item Code</label>
            <div class="col-md-10">
                <input type="text" name="item_code" class="form-control" value="{{$data->item_code}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Name of User 3</label>
            <div class="col-md-10">
                <input type="text" name="name_of_user_3" class="form-control" value="{{$data->name_of_user_3}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Present Holding bu 4</label>
            <div class="col-md-10">
                <input type="text" name="present_holding_bu_4" class="form-control" value="{{$data->present_holding_bu_4}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Present Holding Sfs 5</label>
            <div class="col-md-10">
                <input type="text" name="present_holding_sfs_5" class="form-control" value="{{$data->present_holding_sfs_5}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Demanded Qty 6</label>
            <div class="col-md-10">
                <input type="text" name="demanded_qty_6" class="form-control" value="{{$data->demanded_qty_6}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Reccom by Opi Dte 7</label>
            <div class="col-md-10">
                <input type="text" name="reccom_by_opi_dte_7" class="form-control" value="{{$data->reccom_by_opi_dte_7}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Estimated Unit Price Bdt 8</label>
            <div class="col-md-10">
                <input type="number" name="estimated_unit_price_bdt_8" class="form-control" value="{{$data->estimated_unit_price_bdt_8}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Total Cost Bdt 9</label>
            <div class="col-md-10">
                <input type="number" name="total_cost_bdt_9" class="form-control" value="{{$data->total_cost_bdt_9}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Total Recom Cost Bdt</label>
            <div class="col-md-10">
                <input type="number" name="total_recom_cost_bdt" class="form-control" value="{{$data->total_recom_cost_bdt}}">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Justification 10</label>
            <div class="col-md-10">
                <input type="text" name="justification_10" class="form-control" value="{{$data->justification_10}}">
            </div>
        </div>

        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>
    <x-slot name="cardFooter">
        <div></div>
        <div class="d-flex">
            <x-pondit-act-i url="{{route('prepcm-demand-details.index')}}" />
            <x-pondit-act-c url="{{route('prepcm-demand-details.create')}}" />
            <x-pondit-act-v url="{{route('prepcm-demand-details.show', $data->id)}}" />
            <x-pondit-act-d url="{{route('prepcm-demand-details.delete', $data->id)}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection