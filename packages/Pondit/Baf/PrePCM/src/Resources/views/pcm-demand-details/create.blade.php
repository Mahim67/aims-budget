@extends('pondit-limitless::layouts.master')
@section('content')

@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<x-pondit-card title="Pre-PCM Demand Details">
    <x-pondit-form action="{{route('prepcm-demand-details.store')}}">
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Pre PCM Demand Form</label>
            <div class="col-md-10">
                <input type="text" name="prepcm_demand_id" class="form-control">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Range</label>
            <div class="col-md-10">
                <select name="range_name" id="" class="form-control">
                    <option value="">Engg Range</option>
                    <option value="">C&E Range</option>
                    <option value="">ARM Range</option>
                </select>
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Ser No</label>
            <div class="col-md-10">
                <input type="text" name="ser_no_1" class="form-control">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Name of Equipment </label>
            <div class="col-md-10">
                <input type="text" name="name_of_equipment_2" class="form-control">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-2 col-form-label">Name of User</label>
            <div class="col-md-10">
                <input type="text" name="name_of_user_3" class="form-control">
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="row form-group">
                    <label class="col-md-6 col-form-label">Present Holding in Base/Unit</label>
                    <div class="col-md-6">
                        <input type="text" name="present_holding_bu_4" class="form-control">
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="row form-group">
                    <label class="col-md-6 col-form-label">Present Holding in Sqn/Flt/Sec</label>
                    <div class="col-md-6">
                        <input type="text" name="present_holding_sfs_5" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-6">
                <div class="row form-group">
                    <label class="col-md-3 col-form-label">Demanded Qty</label>
                    <div class="col-md-9">
                        <input type="text" name="demanded_qty_6" class="form-control">
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="row form-group">
                    <label class="col-md-3 col-form-label">Recom by OPI Dte</label>
                    <div class="col-md-9">
                        <input type="text" name="reccom_by_opi_dte_7" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        
        
        <div class="row form-group">
            <label class="col-md-3 col-form-label">Estimated Unit Price (BDT)</label>
            <div class="col-md-9">
                <input type="number" name="estimated_unit_price_bdt_8" class="form-control">
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="row form-group">
                    <label class="col-md-4 col-form-label">Total Cost (BDT)</label>
                    <div class="col-md-8">
                        <input type="number" name="total_cost_bdt_9" class="form-control">
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="row form-group">
                    <label class="col-md-4 col-form-label">Total Recom Cost (BDT)</label>
                    <div class="col-md-8">
                        <input type="number" name="total_recom_cost_bdt" class="form-control">
                    </div>
                </div>
            </div>
        </div>

        <div class="row form-group">
            <label class="col-md-2 col-form-label">Justification</label>
            <div class="col-md-10">
                <textarea name="justification_10" id="" class="form-control" cols="4" rows="4"></textarea>
            </div>
        </div>


        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>
    <x-slot name="cardFooter">
        <div></div>
        <div>
            <x-pondit-act-i url="{{route('prepcm-demand-details.index')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection