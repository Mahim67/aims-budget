
    <style>

        .A4  { 
            width: 210mm; 
            height: 296mm; 
            padding: 10mm;
            margin:auto 
        }

        .landscape{
            width: 276mm;
            height: 190mm;
            margin:auto;
        }

        .portrait {
            width: 190mm;
            height: 276mm;
            margin:auto;
        }
            
        table {
            width: 100%;
            border:  0px;
            border-spacing:0px;
            border-collapse:0px;
        }
        
        .second_parent_td{
            padding-left: 4px;
            padding-right: 4px;
            width: 80%;
            border-right: 1px solid;
            border-left:  1px solid;
            border-bottom: 0px dotted;
            border-top: 0px dotted;
            border-color: black;

        }
        .confidential{
            text-align:center;
            padding:0px 0px 30px 0px;
        }
        .annex{
            padding:0px 0px 10px 0px;
        }
        #notesheet_title{
            padding:15px 0px 0px 0px;
            font-weight:bold;
            display:inline;
            border-bottom:1px solid black;
            
        }
        #notesheet-body{
            padding:15px 4px 52px 4px;

        }
        .notesheet-signature-block{
            padding-left: 500px!important;
            border:1px solid balck!important
        }
        #signing-info{
            padding-left: 50%!important;
            
        }
        #initiating-appointment{
            padding-top: 3%!important;
            line-height: 30px;
        }
        .notesheet-reference{
            padding:15px 0px 5px 0px;

        }
        .number{
            padding:0px 0px 10px 0px;
            font-weight:bold;
            text-align:center;
        }
        span#notesheet_number{
            padding:0px 0px 5px 0px;
            border-bottom:1px solid black;
        }
        .notesheet-security-type {
            font-size: 12px;
            text-transform: uppercase;
            text-align: center;
        }
        .line-space-70{
            line-height: 70%;
        }
        .line-space-50px{
            line-height: 50px;
        }
            #notesheet-prepared-by{
            line-height: 50px;
        }
        .padding-left-20px{
            padding-left:20px
        }

        .text-center{
            text-align: center;
        }

        .border-bottom{
            border-bottom: 2px solid #000;
            font-size: 14px;
            font-weight: bold;
        }

        .remove-row-margin {
            border: none;
            padding: 10px 0px;
        }

    </style>
<div class="landscape">
    <table border="1">
        <thead>

            <tr>
                <th class="notesheet-security-type" colspan="10" style="border:none; padding: 10px 0px;">
                    <span id="notesheet_security_type">|:|PLACEHOLDER_CONFIDENTIAL|:|</span>
                </th>
            </tr>

            <tr>
                <th rowspan="2">Serial No</th>
                <th rowspan="2">Name of Description</th>
                <th class="text-center" colspan="4">Demand</th>
                <th rowspan="2">Recom by OPI Dte</th>
                <th rowspan="2">Estimated Unit Price (TK) </th>
                <th rowspan="2">Total Cost (TK) </th>
                <th rowspan="2">Justification</th>

            </tr>

            <tr>
                <th>Name of User</th>
                <th>Present holding in Base/Unit</th>
                <th>Present holding in Sqn/Fit/Sec</th>
                <th>Qty Dmd</th>
            </tr>

        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
                <td>5</td>
                <td>6</td>
                <td>7</td>
                <td>8</td>
                <td>9</td>
                <td>10</td>
            </tr>
            <tr>
                <td>1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
                <td>5</td>
                <td>6</td>
                <td>7</td>
                <td>8</td>
                <td>9</td>
                <td>10</td>
            </tr>
            <tr>
                <td>1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
                <td>5</td>
                <td>6</td>
                <td>7</td>
                <td>8</td>
                <td>9</td>
                <td>10</td>
            </tr>
        </tbody>

        <tfoot>

            <tr height="100">
                <td colspan="4" class="remove-row-margin">
                    <span class="border-bottom">COLUM 4 & 5 CHECKED AND FOUND CORRECT</span>
                </td>

                <td colspan="3" class="remove-row-margin">
                    <span class="border-bottom">COUNTER SIGNED BY</span>
                </td>

                <td colspan="3" class="remove-row-margin">
                    <span class="border-bottom">APPROVED BY</span>
                </td>
            </tr>

            <tr height="70">
                <td colspan="4" class="remove-row-margin margin-bottom">
                    <span>|:|SIGNITURE|:|</span><br/>
                    <span>|:|PLACEHOLDER_PRESENT_HOLDING_APPOINTMENT|:|</span>
                </td>

                <td colspan="3" class="remove-row-margin">
                    <span>|:|SIGNITURE|:|</span><br/>
                    <span>|:|PLACEHOLDER_COUNTER_BY_APPOINTMENT|:|</span>
                </td>

                <td colspan="3" class="remove-row-margin">
                    <span>|:|SIGNITURE|:|</span><br/>
                    <span>|:|PLACEHOLDER_APPROVED_BY_APPOINTMENT|:|</span>
                </td>
            </tr>

            <tr>
                <th class="notesheet-security-type" colspan="10" style="border:none; padding: 10px 0px;">
                    <span id="notesheet_security_type">|:|PLACEHOLDER_CONFIDENTIAL|:|</span>
                </th>
            </tr>

        </tfoot>

    </table>
</div>