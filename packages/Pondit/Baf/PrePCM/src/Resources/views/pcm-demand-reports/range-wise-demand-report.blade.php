@extends('pondit-limitless::layouts.master')
@section('content')
<x-pondit-card title="Pre-PCM Demand Details">
    {{-- @dd($data??'') --}}
    {{-- {{Session::get('data')??''}} --}}
    <form id="" action="{{route('prepcm-demand-details.base-range-wise')}}" method="GET">
        @csrf
        <div class="row w-50 m-auto">
            <div class="form-group col-sm-4">
                <label class="">Financial Years</label>
                <select name="fin_year" id="fin_year" class="form-control select-search">
                    <option value="">-- Select Fin Year --</option>
                    <option value="2019-2020" {{Session::get('fin_year')=="2019-2020"?"selected":""}}>2019-2020</option>
                    <option value="2020-2021" {{Session::get('fin_year')=="2020-2021"?"selected":""}}>2020-2021</option>
                    <option value="2021-2022" {{Session::get('fin_year')=="2021-2022"?"selected":""}}>2021-2022</option>
                </select>
            </div>
            <div class="form-group col-sm-4">
                <label class="">Base</label>
                <select name="base" id="base" class="form-control select-search">
                    <option value="">-- Select Base --</option>
                    <option value="BSR">BSR</option>
                    <option value="ZSR">ZSR</option>
                </select>
            </div>
            <div class="col-sm-4">
                <x-pondit-baf-ranges id="range" name="range" selected="{{Session::get('range')??''}}" />
            </div>
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-sm bg-success text-center">Search</button>
        </div>
    </form>


    <x-pondit-datatable tbodyClass="tbody">
        <x-slot name="thead">
            <tr>
                <th scope="col" rowspan="2">Ser No</th>
                <th scope="col" rowspan="2">Name of Equipment</th>
                <th scope="col" colspan="4" class="text-center">Demand</th>
                <th scope="col" rowspan="2">Recom by OPI Dte</th>
                <th scope="col" rowspan="2">Estimated Unit Price (TK)</th>
                <th scope="col" rowspan="2">Total Cost (TK)</th>
                <th scope="col" rowspan="2">Justification</th>
                {{-- <th scope="col" rowspan="2">Action</th> --}}
            </tr>
            <tr>
                <th scope="col">Name of User</th>
                <th scope="col" class="text-center">Present <br> holding in <br> Base/Unit</th>
                <th scope="col" class="text-center">Present <br> holding in <br> Sqn/Flt/Sec</th>
                <th scope="col">Qty Dmd</th>
            </tr>
            <tr class="text-center">
                <th scope="col">(1)</th>
                <th scope="col">(2)</th>
                <th scope="col">(3)</th>
                <th scope="col">(4)</th>
                <th scope="col">(5)</th>
                <th scope="col">(6)</th>
                <th scope="col">(7)</th>
                <th scope="col">(8)</th>
                <th scope="col">(9)</th>
                <th scope="col">(10)</th>
                {{-- <th scope="col"></th> --}}
            </tr>
        </x-slot>
        @if (!empty(Session::get('data')) && Session::get('data') !== null)
        @foreach (Session::get('data') as $datum)
        <tr>
            <td>{{ $datum->ser_no }}</td>
            <td>{{ $datum->item_name }}</td>
            <td>{{ $datum->user_id }}</td>
            <td>{{ $datum->present_holding_base_unit }}</td>
            <td>{{ $datum->present_holding_sqn_flt_sec }}</td>
            <td>{{ $datum->qty }}</td>
            <td>{{ $datum->recom_by_opi_dte_name }}</td>
            <td class="text-right">{{ $datum->estimated_unit_cost }}</td>
            <td class="text-right">{{ $datum->estimated_total_cost }}</td>
            <td>{{ $datum->justification }}</td>
        </tr>
        @endforeach
        @endif
    </x-pondit-datatable>
    {{-- card footer --}}
    <x-slot name="cardFooter">
        <div></div>
        <div>
        </div>
        <div>
            <x-pondit-act-word />
            <x-pondit-act-excel />
            {{-- <x-pondit-act-pdf url="{{route('prepcm-demand-details.download', [Session::get('fin_year', 'range')])}}" /> --}}
            <x-pondit-act-preview />
        </div>
    </x-slot>
</x-pondit-card>
@endsection

@push('js')

{{-- <script>
     $(document).on('change', '#fin_year', function()
    {
        let id   =   $(this).val()
        console.log(id)
            // currenEl    =   $(this).parent().parent().find('.add2division');
        $.ajax
        ({
            type: "GET",
            url: 'http://127.0.0.1:8000/api/prepcm-demand-details/base-range-wise-demand/' + id,
            data:id,
            cache: false,
            success: function(data)
            {
                let demands = data.data;
                console.log(demands)
                let htmlData
                demands.forEach(data => {
                    htmlData += `
                    <tr>
                        <td>${data.ser_no??''}</td>
                        <td>${data.item_name??''}</td>
                        <td>${data.user_id??''}</td>
                        <td>${data.present_holding_base_unit??''}</td>
                        <td>${data.present_holding_sqn_flt_sec??''}</td>
                        <td>${data.qty??''}</td>
                        <td>${data.recom_by_opi_dte_name??''}</td>
                        <td class="text-right">${data.estimated_unit_cost??''}</td>
                        <td class="text-right">${data.estimated_total_cost??''}</td>
                        <td>${data.justification??''}</td>
                    </tr>
                    
                    `;
                });
                let tbody = $('.tbody')
                
                tbody.append(htmlData)
            }
        });
    })
</script> --}}


@endpush