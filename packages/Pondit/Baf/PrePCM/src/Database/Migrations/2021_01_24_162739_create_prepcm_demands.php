<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrepcmDemands extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prepcm_demands', function (Blueprint $table) {
            $table->id();
            $table->string('fin_year')->nullable();
            $table->unsignedInteger('office_id')->nullable();
            $table->string('office_name_hq')->nullable();
            $table->string('office_name_base')->nullable();
            $table->string('office_name_unit')->nullable();
            $table->decimal('total_demanded_amount_bdt',10,2)->nullable();
            $table->decimal('total_recom_amount_bdt',10,2)->nullable();
            $table->string('present_holding_chk_bdnumber')->nullable();
            $table->string('present_holding_chk_sign')->nullable();
            $table->date('present_holding_chk_date')->nullable();
            $table->string('present_holding_chk_apmt')->nullable();
            $table->string('counter_signed_by_bdnumber')->nullable();
            $table->string('counter_signed_by_signature')->nullable();
            $table->date('counter_signed_by_date')->nullable();
            $table->string('counter_signed_by_apmt')->nullable();
            $table->string('approved_by_bdnumber')->nullable();
            $table->string('approved_by_signature')->nullable();
            $table->date('approved_by_date')->nullable();
            $table->string('approved_by_apmt')->nullable();
            $table->string('security_grading')->nullable();

            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prepcm_demands');
    }
}
