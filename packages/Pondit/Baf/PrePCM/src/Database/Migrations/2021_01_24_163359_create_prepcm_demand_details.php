<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrepcmDemandDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prepcm_demand_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('prepcm_demand_id')->nullable();
            $table->string('fin_year')->nullable();
            $table->unsignedInteger('range_id')->nullable();
            $table->string('range_name')->nullable();

            $table->unsignedInteger('office_id')->nullable();
            $table->string('office_name')->nullable();

            $table->unsignedInteger('base_id')->nullable();
            $table->string('base_name')->nullable();

            $table->unsignedInteger('unit_id')->nullable();
            $table->string('unit_name')->nullable();

            $table->unsignedInteger('user_id')->nullable();
            $table->string('bdnumber')->nullable();

            $table->unsignedInteger('budgetcode_id')->nullable();
            $table->string('budgetcode_old')->nullable();
            $table->string('budgetcode_new')->nullable();

            $table->unsignedInteger('category_id')->nullable();
            $table->string('category_name')->nullable();

            $table->unsignedInteger('item_id')->nullable();
            $table->string('item_code')->nullable();
            $table->string('item_name')->nullable();
            
            $table->string('ser_no')->nullable();

            $table->unsignedInteger('recom_by_opi_dte_id')->nullable();
            $table->string('recom_by_opi_dte_name')->nullable();

            $table->string('present_holding_base_unit')->nullable();
            $table->string('present_holding_sqn_flt_sec')->nullable();

            $table->integer('qty')->nullable();
            $table->string('unit')->nullable();
            $table->string('currency')->nullable();
            $table->string('is_recom_by_opi_dte')->nullable();
            $table->string('recom_qty')->nullable();
            $table->decimal('estimated_unit_cost',10,2)->nullable();
            $table->decimal('estimated_total_cost',10,2)->nullable();
            $table->decimal('recom_total_cost',10,2)->nullable();
            $table->text('justification')->nullable();

            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prepcm_demand_details');
    }
}
