<?php

namespace Pondit\Baf\PrePCM\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PrePcmDemandDetail extends Model
{
    use HasFactory ;

    protected $table    = 'prepcm_demand_details';

    protected $fillable     =      [
            'prepcm_demand_id'
            ,'fin_year'
            ,'range_id'
            ,'range_name'
            ,'office_id'
            ,'office_name'
            ,'base_id'
            ,'base_name'
            ,'unit_id'
            ,'unit_name'
            ,'user_id'
            ,'bdnumber'
            ,'budgetcode_id'
            ,'budgetcode_old'
            ,'budgetcode_new'
            ,'category_id'
            ,'category_name'
            ,'item_id'
            ,'item_code'
            ,'item_name'
            ,'ser_no'
            ,'recom_by_opi_dte_id'
            ,'recom_by_opi_dte_name'
            ,'present_holding_base_unit'
            ,'present_holding_sqn_flt_sec'
            ,'qty'
            ,'unit'
            ,'currency'
            ,'is_recom_by_opi_dte'
            ,'recom_qty'
            ,'estimated_unit_cost'
            ,'estimated_total_cost'
            ,'recom_total_cost'
            ,'justification'

            ,'created_by'
            ,'updated_by'
            ,'deleted_by'
    ];
}
