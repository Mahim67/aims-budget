<?php

namespace Pondit\Baf\PrePCM\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PrePcmDemand extends Model
{
    use HasFactory, SoftDeletes;
    
    protected $dates    = ['deleted_at'];

    protected $table    = 'prepcm_demands';

    protected $fillable   =   [
            'fin_year'
            ,'office_id'
            ,'office_name_hq'
            ,'office_name_base'
            ,'office_name_unit'
            ,'total_demanded_amount_bdt'
            ,'total_recom_amount_bdt'
            ,'present_holding_chk_bdnumber'
            ,'present_holding_chk_sign'
            ,'present_holding_chk_date'
            ,'present_holding_chk_apmt'
            ,'counter_signed_by_bdnumber'
            ,'counter_signed_by_signature'
            ,'counter_signed_by_date'
            ,'counter_signed_by_apmt'
            ,'approved_by_bdnumber'
            ,'approved_by_signature'
            ,'approved_by_date'
            ,'approved_by_apmt'
            ,'security_grading'
    ];
}
