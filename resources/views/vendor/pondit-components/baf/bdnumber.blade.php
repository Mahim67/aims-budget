<div class="form-group row">
    <label for="{{$id}}" class="col-md-2">@lang($label)</label>
    <div class="col-md-10">
    <input type="number" name="{{$name}}" id="{{$id}}" class="form-control {{$class}}" placeholder="{{$placeholder}}" {{$otherAttr}} />
    </div>
</div>
