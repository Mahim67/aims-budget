<div class="form-group row">
    <label for="{{$id}}" class="col-md-2">@lang($label)</label>
    <div class="col-md-10">
        <select name="{{$name}}" id="{{$id}}" class="form-control select-search {{$class}}" {{$otherAttr}}>
            
            @foreach ($ranges as $key=>$item)
            @if ($key != $selected)
            <option value="{{$key}}" >{{$item}}</option>
            @else 
            <option value="{{$key}}" selected='selected'>{{$item}}</option>
            @endif
            @endforeach
        </select>
    </div>
</div>

@push('js')

<script src="{{ asset("") }}vendor/pondit-component/assets/limitless/financial-years/select2.min.js"></script>
<script src="{{ asset("") }}vendor/pondit-component/assets/limitless/financial-years/form_select2.js"></script>

@endpush