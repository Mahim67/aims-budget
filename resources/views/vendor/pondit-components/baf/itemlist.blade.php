<div class="form-group row">
    <label for="{{$id}}" class="col-md-2">@lang($label)</label>
    <div class="col-md-10">
        <select name="{{$name}}" id="{{$id}}" class="form-control select-search {{$class}}" {{$otherAttr}}>
            <option value="">-- Select Item List --</option>
            <option value="a">A</option>
            <option value="b">B</option>
            <option value="c">C</option>
        </select>
    </div>
</div>

@push('js')

<script src="{{ asset("") }}vendor/pondit-component/assets/limitless/financial-years/select2.min.js"></script>
<script src="{{ asset("") }}vendor/pondit-component/assets/limitless/financial-years/form_select2.js"></script>

@endpush