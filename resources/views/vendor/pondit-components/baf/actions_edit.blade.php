<a href="{{ $url }}" id="{{ $id }}" class="{{ $class }} btn btn-outline bg-primary btn-icon text-primary btn-sm border-primary border-2 rounded-round legitRipple mr-1"  data-popup="tooltip" title="{{ $tooltip }}" data-original-title="{{ $tooltip }}">
    <i class="fas fa-{{ $icon }}"></i>
    {{ $title }}
</a>
