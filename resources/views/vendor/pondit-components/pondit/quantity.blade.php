<div class="form-group row ">
    <label for="{{$id}}" class="col-sm-2 col-form-label"> @lang($label)</label>
    <div class="col-sm-10 d-flex">
        <input type="number" class="form-control {{$class}}" name="{{$name}}" id="{{$id}}" {{$otherAttr}}>
        <x-pondit-qty-unit class="w-50 ml-2" />
    </div>
</div>
