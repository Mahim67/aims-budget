<div class="row">
    <label for="{{$id}}" class="col-md-4 col-form-label">{{$label}}</label>
    <div class="col-md-8 d-flex">
        <input type="number" name="{{$name}}" id="{{$id}}" class="form-control {{$class}}" {{$otherAttr}}>
        <x-pondit-currency class="w-50 ml-3"/>
    </div>
</div>