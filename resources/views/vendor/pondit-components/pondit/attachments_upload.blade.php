<div class='form-group row'>
    <label for="file" class="col-sm-2 col-form-label"> @lang($label)</label>
    <div class='col-sm-10'>
        <input type="file" class="form-control {{$class}}" name="{{$name}}" id="{{$id}}"  {{$otherAttr}}>
        <div id="download"></div>
    </div>
</div>

@push('js')
<script>
    $(document).ready(function(){
        // var id = document.forms[0]['id'];
        // console.log(id)
     $('#upload_attachments_form').on('submit', function(event){
      event.preventDefault();
      $.ajax({
       url:"{{ url('api/attachments/store') }}",
       method:"POST",
       data:new FormData(this),
       dataType:'JSON',
       contentType: false,
       cache: false,
       processData: false,
       success:function(data)
       {
            console.log(data.data.extension)
            // if (data.data.extension = 'docx') {
            //     let dwnlod = `<button class="btn btn-circle-sm btn-success">
            //         <i class="fas fa-download"></i>
            //         </button>`
            //     $('#download').html(dwnlod);
            // }
       }
      })
     });
    
    });
</script>
@endpush