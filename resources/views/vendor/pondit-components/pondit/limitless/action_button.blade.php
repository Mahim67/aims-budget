<span>
    <a href="{{ $href }}" id="{{ $id }}"
        class="{{ $class }} btn btn-outline bg-{{$bg}} btn-icon text-{{$bg}} border-{{$bg}} border-2 rounded-round legitRipple mr-1"
        data-popup="tooltip" title="{{ $tooltip }}" data-original-title="Top tooltip">
        <i class="fas fa-{{ $icon }}"></i>
        {{ $title }}
    </a>
</span>