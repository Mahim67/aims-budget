<table class="table {{ $tableClass }}">
    <thead class="{{$theadClass}}">
        {{ $thead }}
    </thead>
    <tbody class="{{ $tbodyClass }}">
        {{ $slot }}
    </tbody>
    <tfoot class="{{ $tfootClass }}">
        {{ $tfooter }}
    </tfoot>
</table>
@push('js')
<script src="{{ asset("") }}vendor/pondit-component/assets/limitless/datatable/datatables.min.js"></script>
<script src="{{ asset("") }}vendor/pondit-component/assets/limitless/datatable/buttons.min.js"></script>
<script src="{{ asset("") }}vendor/pondit-component/assets/limitless/datatable/datatables_extension_colvis.js"></script>
<script src="{{ asset("") }}vendor/pondit-component/assets/limitless/select2/select2.min.js"></script>

@endpush