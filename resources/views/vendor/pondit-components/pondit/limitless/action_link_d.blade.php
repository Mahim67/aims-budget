
<span>
    <form action="{{$url}}" method="post">
        @csrf
        <button id="{{ $id }}" class="{{ $class }} btn btn-circle btn-circle-sm bg-danger mr-1" data-popup="tooltip" onclick="return confirm('Are You Sure!')"
            title="{{ $tooltip }}" data-original-title="{{ $tooltip }}">
            <i class="fas fa-{{ $icon }}"></i>
            {{ $title }}
        </button>
    </form>
</span>