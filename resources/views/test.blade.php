@extends('pondit-limitless::layouts.master')
@section('content')
<?php 
$data = [
    0 => "2010",
    1 => "2011",
    2 => "2012",
    3 => "2013",
];
?>
{{-- @dd($data) --}}
<x-pondit-card>
    <x-pondit-act-c />

    <x-pondit-fin-year />

    <x-slot name="cardFooter">
        <x-pondit-act-c />
    </x-slot>
</x-pondit-card>
@endsection