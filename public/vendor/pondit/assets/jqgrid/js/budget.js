(function($) {
    $(document).ready(function() {

        let aa = $('meta[name="csrf-token"]').attr('content')
        console.log(aa)
            // load data 
        load_data();


        // eventlistener
        $(document).on('click', '.fa-pen', editData);

        $(document).on('click', '#form-submit', updateExistingItem);

        $(document).on('click', '.add-item', showAddItemForm);

        $(document).on('click', '#form-submit-add', sendNewItemToDatabase);

        $(document).on('click', '.fa-trash', deleteData);

        $(document).on('hidden.bs.modal', function() { resetForm(); });

        $(document).on('click', '.fa-eye', singleItemDetails);


    });


})(jQuery)




function editData() {
    $('#form-submit-add').attr('id', 'form-submit');

    let
        rowId = $(this).parent().parent().attr('id'),
        data = $("#jqGrid").jqGrid("getRowData", rowId);
    console.log(data)

    $('#fin_year').val(data.fin_year);
    $('#total_amount').val(data.total_amount);
    $('#no_of_supplimentary_budget').val(data.no_of_supplimentary_budget);
    $('#id').val(data.id);

    $('#userForm').modal('show');
}




function updateExistingItem() {
    $.ajax({
        url: `http://aims-budget.test/masterdata/api/budgets/update/${getFormData().id}`,
        datatype: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: getFormData(),
        type: 'PUT',
        success: function(res) {
            console.log(res)
            $('#jqGrid').trigger('reloadGrid');
            $('#userForm').modal('hide');
            $(document).find(`#${getFormData().id}`).replaceWith(renderSingleRow(getFormData()));
        },
        error: function(xhr, status, error) {
            $('#jqGrid').restoreRow(getFormData().id);
            $('#jqGrid').trigger('reloadGrid');
            console.log(xhr.responseText);
        }
    });
}



function deleteData() {
    let
        rowId = $(this).parent().parent().attr('id');
    data = $("#jqGrid").jqGrid("getRowData", rowId);
    console.log(rowId)

    if (confirm('Are You want To delete It ?')) {
        $.ajax({
            url: `http://aims-budget.test/masterdata/api/budgets/delete/${rowId}`,
            datatype: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: { 'id': rowId },
            type: 'POST',
            success: function(res) {
                console.log(res)
                $('#jqGrid').jqGrid('delRowData', rowId);
                $('#jqGrid').trigger('reloadGrid');
            },
            error: function(xhr, status, error) {
                $('#jqGrid').restoreRow(rowId);
                $('#jqGrid').trigger('reloadGrid');
                console.log(xhr.responseText);
            }
        });
    }
}


function showAddItemForm() {
    $('#userForm').modal('show');
    $('#form-submit').attr('id', 'form-submit-add');
}



function sendNewItemToDatabase() {
    $.ajax({
        url: `http://aims-budget.test/masterdata/api/budgets/store`,
        datatype: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: getFormData(),
        type: 'POST',
        success: function(res) {
            console.log(res)
            location.reload();
            $('#userForm').modal('hide');

        },
        error: function(xhr, status, error) {
            $('#jqGrid').restoreRow(getFormData().id);
            $('#jqGrid').trigger('reloadGrid');
            console.log(xhr.responseText);
        }
    });

}


function singleItemDetails() {
    let rowId = $(this).parent().parent().attr('id');
    $.ajax({
        url: `http://aims-budget.test/masterdata/api/budgets/show/${rowId}`,
        datatype: 'json',
        data: { "id": rowId },
        type: 'GET',
        success: function(res) {
            console.log(res)

            var row = `
                <tr>
                    <th>Fin Year : </th>
                    <td>${(res.fin_year == null)?'':res.fin_year}</td>
                </tr>
                <tr>
                    <th>Total Amount : </th>
                    <td>${(res.total_amount == null)?'':res.total_amount}</td>
                </tr>
                <tr>
                    <th>No Of Supplimentary Budget : </th>
                    <td>${(res.no_of_supplimentary_budget == null)?'':res.no_of_supplimentary_budget}</td>
                </tr>
                `;

            $('#user-details-table').html(row);
            $('#userDetails').modal('show');

        },
        error: function(xhr, status, error) {
            $('#jqGrid').restoreRow(getFormData().id);
            $('#jqGrid').trigger('reloadGrid');
            console.log(xhr.responseText);
        }
    });
}





function resetForm() {
    $('#form')[0].reset();
}




function renderSingleRow(v = null) {
    let single_row = `<tr id="${v.id}" class="jqgrow ui-row-ltr" role="row" tabindex="-1">
                        <td role="gridcell">${v.fin_year}</td>
                        <td role="gridcell">${v.total_amount}</td>
                        <td role="gridcell">0</td>
                        <td role="gridcell" >
                        <i class="fa fa-eye  btn bg-success btn-circle btn-circle-sm pt-2" style="cursor: pointer"></i> <i class="fa fa-pen btn bg-primary btn-circle btn-circle-sm pt-2" style="cursor: pointer"></i> <i class="fa fa-trash btn bg-danger btn-circle btn-circle-sm pt-2" style="cursor: pointer"></i>
                        </td>
                    </tr>`;

    return single_row;
}


function getFormData() {
    let obj = {};
    obj.id = $('#id').val();
    obj._token = $('meta[name="csrf-token"]').attr('content');
    obj.fin_year = $('#fin_year').val();
    obj.total_amount = $('#total_amount').val();
    obj.no_of_supplimentary_budget = $('#no_of_supplimentary_budget').val();
    // console.log(obj)
    return obj;
}



function load_data() {
    $.ajax({
        url: `http://aims-budget.test/masterdata/api/budgets`,
        method: 'get',
        dataType: 'json',
        beforeSend: function(data) {
            // console.log('loading ...');
        },
        success: function(res) {
            // console.log(res)
            render_data(res);
        },
        error: function(error) {
            console.log("ERROR :" + error);
        },
        complete: function() {

            $("#jqGrid").jqGrid('filterToolbar', {
                stringResult: true,
                searchOnEnter: false,
                defaultSearch: "cn"
            });

        }
    });
}




function render_data(res) {
    $('#jqGrid').jqGrid({
        datatype: "local",
        data: res,
        editurl: 'clientArray',
        colModel: [
            { label: 'Fin Year', name: 'fin_year', align: 'left', firstsortorder: 'asc', formatter: 'string', editable: false, editrules: { required: true } },
            { label: 'Total Amount', name: 'total_amount', align: 'left', firstsortorder: 'asc', formatter: 'string', editable: true, editrules: { required: true } },
            { label: 'No Of Supplimentary Budget', name: 'no_of_supplimentary_budget', formatter: 'string', firstsortorder: 'asc', editable: true },
            { label: 'Id', name: 'id', key: true, hidden: true, formatter: 'string', align: 'left', firstsortorder: 'asc', editable: true },
            { label: 'Action', name: 'action', class: "text-center", firstsortorder: 'asc' },
        ],
        rowNum: 5000,
        rownumbers: true,
        pager: '#jqGridPager',
        pgbuttons: true,
        toppager: false,
        // caption 			: `Budget Codes`,
        height: '500',
        width: '1020',
        headertitles: false,
        gridview: true,
        footerrow: true,
        userDataOnFooter: true,
        viewrecords: true,
        sortname: 'sequence_number',
        subGrid: false,
        subGridModel: [],
        responsive: true,
        multiselect: false,
        grouping: true,
        gridComplete: function() {

            // total Users count
            var td, users, countUsers,
                $grid = $('#jqGrid'),
                actions = $('td[aria-describedby="jqGrid_action"]');

            for (var i = 0; i < actions.length; i++) {
                td = actions[i];
                $(td).html(`<i class="fa fa-eye  btn bg-success btn-circle btn-circle-sm pt-2" style="cursor: pointer"></i> <i class="fa fa-pen btn bg-primary btn-circle btn-circle-sm pt-2" style="cursor: pointer"></i> <i class="fa fa-trash btn bg-danger btn-circle btn-circle-sm pt-2" style="cursor: pointer"></i>`);
                $(td).attr('class', 'text-center');
            }

            $grid.jqGrid('footerData', 'set', { 'action': `<span class="btn btn-sm btn-light add-item"> + ADD</span>`, });

            users = $grid.jqGrid('getCol', 'id'),

                total_users = [];

            for (var i = 0; i < users.length; i++) {
                total_users[users[i]] = (users[i]);
            }

            countUsers = Object.keys(users).length;
            $grid.jqGrid('footerData', 'set', {
                'id': `Users : ${countUsers}`,
            });


            $('.ui-jqgrid').css('overflow-x', 'auto');


        },
        colMenu: true,
        styleUI: 'Bootstrap4',
        iconSet: "fontAwesome",
        beforeRequest: function() {
            // console.log('sending...')
        },
        onSelectRow: function(id) {
            if (id != null) {
                if (id == typeof Number) console.log(id);

            }
        },

    });

}