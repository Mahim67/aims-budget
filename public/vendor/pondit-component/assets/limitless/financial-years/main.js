// const URL_BASE = 'https://btcn-aims-api2.pondit.com/api/masterdata/financial-years'
const URL_BASE = 'http://127.0.0.1:8001/api/masterdata/financial-years'

var fin_years = document.getElementById("fin_years")

let date = new Date().getFullYear();
if (date > 6) {
    var year = date + "-" + (date + 1);
}
else {
    var year = (date - 1) + "-" + date;
}

fetch(URL_BASE)
    .then((resp) => resp.json())
    .then(function (data) {

        data.forEach(element => {
            // console.log(element.year_range)
            if (element.year_range == null) {
                fin_years.innerHTML = `<option>Not Found</option>`
            } else {
                html = `<option value="${element.year_range}" ${element.year_range == year ?'selected':''} >${element.year_range}</option>`
                // console.log(html)   
                fin_years.innerHTML += html
            }

        });
    })
    .catch(function (error) {
        console.log(error);
    });